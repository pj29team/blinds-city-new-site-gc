<?php
/*
Plugin Name:       Blinds City Pricing Import
Description:       Allow the upload of a excel spreadsheet and send to a gravity form as conditional
                   pricing rules.
Version:           0.2
Author:            Joel Sutton
Author URI:        http://innerteapot.com/
*/

// If this file is called directly, abort.
if (!defined('WPINC')) {
        die;
}

function bcpi_process_worksheet($objWorksheet, $form_id, $markup, $debug=True) {
    global $wpdb;
    $table_name = $wpdb->prefix . 'bc_pricing_import';

    // check for headers
    //
    $check_errors = 0;

    $check_1 = $objWorksheet->getCellByColumnAndRow(0, 4)->getValue();
    if ($check_1 != "Width mm") 
    {
        $check_errors++;
    }

    $check_2 = $objWorksheet->getCellByColumnAndRow(1, 4)->getValue();
    if ($check_2 != "Pin") 
    {
        $check_errors++;
    }

    $check_3 = $objWorksheet->getCellByColumnAndRow(0, 5)->getValue();
    if ($check_3 != "Dropmm") 
    {
        $check_errors++;
    }

    $check_4 = $objWorksheet->getCellByColumnAndRow(1, 5)->getValue();
    if ($check_4 != "Measure") 
    {
        $check_errors++;
    }

    if ($check_errors > 0)
    {
        if ($debug == True) {
            echo $check_1 . "<br />";
            echo $check_2 . "<br />";
            echo $check_3 . "<br />";
            echo $check_4 . "<br />";
        }
        return False;
    }

    // get sheet name
    // 
    $group = $objWorksheet->getTitle(); 

    // loop through pricing, put data into an array, and save data to mysql
    //
    $pricing_data = array();

    $start_x = 2;
    $start_y = 6;

    $y = $start_y;
    while($y != False)
    {
        $x = $start_x;
        while($x != False)
        {
           $price = $objWorksheet->getCellByColumnAndRow($x, $y)->getValue();
           if (empty($price))
           {
               if ($x == $start_x)
               {
                   $y = False;
               }
               $x = False;
           }
           else
           {
               $drop = $objWorksheet->getCellByColumnAndRow(0, $y)->getValue();
               $width = $objWorksheet->getCellByColumnAndRow($x, 4)->getValue();
               if ($markup != 0)
               {
                   $sale_price = (float)$price + ($price * ($markup / 100)) ;
               }
               else
               {
                   $sale_price = $price ;
               }

               if ($debug == True) {
                   echo "Col: " . $x . "<br/>" ;
                   echo "Row: " . $y . "<br/>" ;
                   echo "Form ID: " . $form_id . "<br/>" ;
                   echo "Price: " . $price . "<br/>" ;
                   echo "Markup: " . $markup . "<br/>" ;
                   printf("Sale Price: %.2f <br/>", $sale_price) ;
                   echo "Dropmm: " . $drop . "<br/>" ;
                   echo "Widthmm: " . $width . "<br/>" ;
                   echo "Group: " . $group . "<br/>" ;
                   echo "<hr />" ;
               }

               $values = array( 
                   'grouping' => $group,
                   'width_mm' => $width,
                   'drop_mm' => $drop,
                   'price' => $price,
                   'markup' => $markup,
                   'gravity_form_id' => $form_id,
               ); 

               $wpdb->insert( 
                   $table_name, 
                   $values,
                   array( 
                       '%s', 
                       '%d', 
                       '%d', 
                       '%f', 
                       '%d', 
                       '%d', 
                   ) 
               );

               $values["sale_price"] = sprintf("%.2f", $sale_price);
               $pricing_data[] = $values;

               $x++;
           }
        }
        $y++;
    }

    if ($debug==True) {
        print "<h2>Pricing Data</h2>";
        print "<pre>";
        var_dump($pricing_data);
        print "</pre>";
        print "<hr />";
    }

    return $pricing_data;
}


function bcpi_make_form($form_id, $pricing_data, $debug=True) {
    $form = GFAPI::get_form($form_id);

    if ($debug == True) {
        echo "<h2>Original Form</h2>";
        echo "<pre>";
        var_dump($form);
        echo "</pre>";
        echo "<hr/>";
    }

    $form["title"] .= " (copy " . date("YmdHi") . ")"; 

    // find height and width fields
    //
    $height_field_id = "";
    $width_field_id  = "";
    $group_field_id  = "";

    foreach ($form["fields"] as $field_object) {
        if ($field_object->label == "Width") {
            $width_field_id = sprintf("%d", $field_object->id);
        }

        if ($field_object->label == "Height") {
            $height_field_id = sprintf("%d", $field_object->id);
        }

        if ($field_object->label == "Fabric Group") {
            $group_field_id = sprintf("%d", $field_object->id);
        }
    }

    if ($debug == True) {
        echo "Height ID: $height_field_id <br/>";
        echo "Width ID: $width_field_id <br/>";
        echo "Group ID: $group_field_id <br/>";
        echo "<hr/>";
    }

    if ($height_field_id == "") {
        return "Could not find Height field in Gravity Form";
    }

    if ($width_field_id == "") {
        return "Could not find Width field in Gravity Form";
    }

    if ($group_field_id == "") {
        return "Could not find Fabric Group field in Gravity Form";
    }

    $new_pricing_logic = array();
    $new_pricing_logic[1] = array();

    foreach ($pricing_data as $p) {
        // build array with beginning boundaries
        //
        $new_pricing_logic[1][] = array(
            "price" => $p["sale_price"],
            "position" => "",
            "conditionalLogic" =>
                array(
                    "actionType" => "show",
                    "logicType" => "all",
                    "rules" => array(
                        array(
                            "fieldId" => $group_field_id,
                            "operator" => "is",
                            "value" => $p["grouping"],
                        ),
                        array(
                            "fieldId" => $width_field_id,
                            "operator" => ">",
                            "value" => (int)$p["width_mm"],
                        ),
                        array(
                            "fieldId" => $width_field_id,
                            "operator" => "<",
                            "value" => 0,
                        ),
                        array(
                            "fieldId" => $height_field_id,
                            "operator" => ">",
                            "value" => (int)$p["drop_mm"],
                        ),
                        array(
                            "fieldId" => $height_field_id,
                            "operator" => "<",
                            "value" => 0,
                        ),
                    ),
                ),
        );
    }

    // add end boundaries
    //
    foreach ($new_pricing_logic[1] as $logic_key => $logic) {
        // get the width from the next record
        //
        $width_start = $new_pricing_logic[1][$logic_key]["conditionalLogic"]["rules"][1]["value"] ; 
        $width_end   = $new_pricing_logic[1][$logic_key+1]["conditionalLogic"]["rules"][1]["value"] ; 

        // delete rule if we've hit the next column
        //
        if ($width_end < $width_start) {
            unset($new_pricing_logic[1][$logic_key]["conditionalLogic"]["rules"][2]) ;
        } else {
            // convert to a string for api call
            // 
            $width_end = sprintf("%d", $width_end);
            $new_pricing_logic[1][$logic_key]["conditionalLogic"]["rules"][2]["value"] = $width_end; 
        }

        // correct for no greater than or equal to operator
        //
        $width_start--;

        // convert to a string for api call
        //
        $width_start = sprintf("%d", $width_start);
        $new_pricing_logic[1][$logic_key]["conditionalLogic"]["rules"][1]["value"] = $width_start;

        // loop through heights to find the next value
        //
        $height_start = $new_pricing_logic[1][$logic_key]["conditionalLogic"]["rules"][3]["value"] ; 
        $height_key = $logic_key;
        while($height_key <= count($new_pricing_logic[1])){
            $height_end = $new_pricing_logic[1][$height_key]["conditionalLogic"]["rules"][3]["value"] ; 

            if ($height_end > $height_start) {
                // found the next value
                //
                break;
            }

            $height_key++;
        }

        // delete rule if we've hit the last row
        if ($height_end == 0) {
            unset($new_pricing_logic[1][$logic_key]["conditionalLogic"]["rules"][4]);
        } else {
            // convert to a string for api call
            // 
            $height_end = sprintf("%d", $height_end);
            $new_pricing_logic[1][$logic_key]["conditionalLogic"]["rules"][4]["value"] = $height_end; 
        }

        // correct for no greater than or equal to operator
        //
        $height_start--;

        // convert to a string for api call
        // 
        $height_start = sprintf("%d", $height_start);
        $new_pricing_logic[1][$logic_key]["conditionalLogic"]["rules"][3]["value"] = $height_start;

        // close any gaps in the rule numbers (which cause unexpected behaviour)
        //
        $new_pricing_logic[1][$logic_key]["conditionalLogic"]["rules"] =
            array_values($new_pricing_logic[1][$logic_key]["conditionalLogic"]["rules"]);
    }

    $form["gw_pricing_logic"] = $new_pricing_logic;
    $result = GFAPI::add_form($form);

    if ($debug == True) {
        echo "<h2>Updated Form</h2>";
        echo "<pre>";
        var_dump($form);
        echo "</pre>";
        echo "<hr/>";

        echo "<h2>New Form ID</h2>";
        echo "<pre>";
        var_dump($height_start);
        echo "</pre>";
        echo "<hr/>";
    }

    return $result;
}

function bcpi_install() {
    global $wpdb;

    $table_name = $wpdb->prefix . 'bc_pricing_import';
    
    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name (
        id int NOT NULL AUTO_INCREMENT,
        grouping varchar(255),
        width_mm int,
        drop_mm int,
        price decimal(6,2),
        markup int, 
        gravity_form_id int,
        UNIQUE KEY id (id)
    ) $charset_collate;";

    $wpdb->query($sql);
}
register_activation_hook(__FILE__, 'bcpi_install');

function bcpi_menu_form()
{

    //echo 'Get forms';
    //$form_id = '93';
    // $forms = GFAPI::get_form(92);
    $forms = GFAPI::get_forms();

    //print_r ($forms);

    ?>
    <p>For a successful import download a copy of this 
    <a href="<?php echo site_url("/wp-content/plugins/blindscity-pricing-import/pricing-template-v2.xlsx");?>">
    Pricing Template</a>, update the figures, and import using this form. The Gravity Form you select will
    be copied and that copy will contain pricing rules matching the newly import figures. Specifying
    a markup (as a percentage) is optional. 
    </p>
    <hr />
    <form name="bcpi_form" method="post" action="" enctype="multipart/form-data">
    <?php wp_nonce_field('name_of_my_action', 'wpnf_ft'); ?>
    <br />
    Gravity Form: <select name="form_id">

   
    <?php foreach ($forms as $f):?>

      <option value="<?php echo $f['id']; ?>"><?php echo $f['title']; ?></option>
    <?php endforeach;?>

   <!--  <option value="92"><?php //echo $forms['title']; ?></option>
    <option value="89">DIY Custom Venetian Blinds</option>
    <option value="91">Blinds City Express Roller Blinds</option>
     <option value="88">DIY Custom Roller Blinds</option>
     <option value="98">Motorised Roller Blinds</option> -->
    </select>
    <br />
    Excel Spread Sheet: <input type="file" name="excel" />
    <br />
    Markup (%):
    <input type="text" name="markup" />
    <br />
    <br />
    <hr/>
    <input type="submit" class="button-primary" name="Submit" value="Import" />
    </form>
    <?php
}

function bcpi_menu_post($debug=True)
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'bc_pricing_import';

    if ($debug == True) {
        echo "<pre>" ;
        var_dump($_POST);
        var_dump($_FILES['excel']);
        echo "<hr/>";
    }

    $markup = $_POST['markup'];

    if (is_numeric($markup))
    {
        $markup = (int) $markup;
    }
    else
    {
        $markup = 0;
    }

    $form_id = $_POST['form_id'];

    $wpdb->delete($table_name, array('gravity_form_id' => $form_id), array('%d'));

    if ($debug == True) {
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
    }

    /** PHPExcel_IOFactory */
    require_once dirname(__FILE__) . '/PHPExcel-1.8.1/Classes/PHPExcel/IOFactory.php';
    try
    {
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel = $objReader->load($_FILES['excel']['tmp_name']);
    }
    catch (PHPExcel_Exception $e)
    {
        echo "Invalid spread sheet. Did you upload an xlsx file? Please try again.<br />";
        return;
    }

    $pricing_data = array();

    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {

        $worksheet_pricing_data = bcpi_process_worksheet($worksheet, $form_id, $markup);
        if ($worksheet_pricing_data == False) {
            echo "Invalid spread sheet. Are you using the correct template? Please try again.<br />";
            return;
        }

        if ($debug == True) {
            print "<h2>Worksheet Pricing Data</h2>";
            print "<pre>";
            var_dump($worksheet_pricing_data);
            print "</pre>";
            print "<hr />";
        }

        $pricing_data = array_merge($pricing_data, $worksheet_pricing_data);

    }

    if ($debug == True) {
        print "<h2>Combined Pricing Data</h2>";
        print "<pre>";
        var_dump($pricing_data);
        print "</pre>";
        print "<hr />";
    }

    $result = bcpi_make_form($form_id, $pricing_data);

    if ($debug == True) {
        echo "</pre>" ;
    }

    if (is_numeric($result)) {
        echo "<p>A new <a href=\"admin.php?page=gf_edit_forms&view=settings&subview=conditional_pricing_page&id=$result \">Gravity Form</a> has been created and the import has been saved to the database.</p>" ;
    } else {
        echo "<p>An error occured: $result</p>";
    }
}

function bcpi_menu()
{
    echo "<h1>Blinds City Pricing Import</h1>";

    if (!empty($_POST) && 
        isset($_POST['Submit']) && 
        check_admin_referer('name_of_my_action', 'wpnf_ft')) 
    {
        bcpi_menu_post();
    }
    else
    {
        bcpi_menu_form();
    }
}

function bcpi_admin_actions()
{
    add_menu_page(
        "Blinds City Pricing Import", 
        "Blinds City Pricing Import", 
        1, 
        "blindscity-pricing-import", 
        "bcpi_menu"
    );
}
add_action('admin_menu', 'bcpi_admin_actions');

/* 
vim: syntax=php nohlsearch tabstop=4 softtabstop=4 shiftwidth=4 expandtab number smartindent 
*/?>
