<?php
use \zipMoney\Model\ShopperStatistics;
use \zipMoney\Model\OrderItem;
use \zipMoney\Model\Address;


class WC_Zipmoney_Payment_Gateway_API_Abstract {
    protected $WC_Zipmoney_Payment_Gateway;

    public function __construct(WC_Zipmoney_Payment_Gateway $WC_Zipmoney_Payment_Gateway)
    {
        $this->WC_Zipmoney_Payment_Gateway = $WC_Zipmoney_Payment_Gateway;

        //set the environment
       // $is_sandbox = $this->WC_Zipmoney_Payment_Gateway->WC_Zipmoney_Payment_Gateway_Config->get_bool_config_by_key(WC_Zipmoney_Payment_Gateway_Config::CONFIG_SANDBOX);
        $is_sandbox = $this->WC_Zipmoney_Payment_Gateway->get_environment_status();

        if($is_sandbox == true){
            zipMoney\Configuration::getDefaultConfiguration()->setEnvironment('sandbox');
        } else {
            zipMoney\Configuration::getDefaultConfiguration()->setEnvironment('production');
        }

        //set the platform string
        zipMoney\Configuration::getDefaultConfiguration()->setPlatform(
            WC_Zipmoney_Payment_Gateway_Util::get_platform_string(
                $this->WC_Zipmoney_Payment_Gateway
            )
        );

    }

    /**
     * Set the api key
     *
     * @param $api_key
     */
    protected function set_api_key($api_key)
    {
        zipMoney\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'Bearer ' . $api_key);
    }

    /**
     * Get the login user statistics
     *
     * @return \zipMoney\Model\ShopperStatistics
     */
    protected function _get_shopper_statistics()
    {
        if(is_user_logged_in() == false){
            //we won't return anything if the user is not login
            return null;
        }

        $current_user = wp_get_current_user();
        $customer_orders = get_posts(array(
            'numberposts' => -1,
            'meta_key' => '_customer_user',
            'meta_value' => get_current_user_id(),
            'post_type' => wc_get_order_types(),
            'post_status' => array('wc-completed', 'wc-refunded'),
        ));

        $account_created = DateTime::createFromFormat('Y-m-d H:i:s', $current_user->get('user_registered'));
        $sales_total_count = 0;
        $sales_total_amount = 0;
        $sales_avg_amount = 0;
        $sales_max_amount = 0;
        $refunds_total_amount = 0;
        $currency = get_woocommerce_currency();
        $last_login = DateTime::createFromFormat('Y-m-d H:i:s', $current_user->get('user_login'));

        if(!empty($customer_orders)){
            foreach ($customer_orders as $post) {
                $order = new WC_Order($post->ID);

                if($order->get_status() == 'completed'){
                    $sales_total_count++;
                    $sales_total_amount += $order->get_total();

                    if($sales_max_amount < $order->get_total()){
                        $sales_max_amount = $order->get_total();
                    }
                } else if($order->get_status() == 'refunded'){
                    $refunds_total_amount += $order->get_total();
                }
            }
        }

        if($sales_total_count > 0){
            $sales_avg_amount = (float)round($sales_total_count / $sales_total_count, 2);
        }

        $data = array(
            'sales_total_count' => $sales_total_count,
            'sales_total_amount' => $sales_total_amount,
            'sales_avg_amount' => $sales_avg_amount,
            'sales_max_amount' => $sales_max_amount,
            'refunds_total_amount' => $refunds_total_amount,
            'currency' => $currency
        );

        if(!empty($account_created)){
            $data['account_created'] = $account_created;
        }

        if(!empty($last_login)){
            $data['last_login'] = $last_login;
        }

        return new ShopperStatistics($data);
    }

    protected function _get_order_item_data($item)
    {
        
        if(WC_Zipmoney_Payment_Gateway_Util::is_wc_3()){
           $product_id = $item->get_product_id();
           $item_quantity = intval($item->get_quantity());
           $item_subtotal = $item->get_subtotal();
           $item_subtotal_tax = $item->get_subtotal_tax();
        }
        else{
           $product_id = $item['item_meta']['_product_id'][0];
           $item_quantity = intval($item['item_meta']['_qty'][0]);
           $item_subtotal = $item['item_meta']['_line_subtotal'][0];
           $item_subtotal_tax = $item['item_meta']['_line_subtotal_tax'][0];
        }


        $product = new WC_Product($product_id);

        $description = WC_Zipmoney_Payment_Gateway_Util::get_product_description($product);

        $order_item_data = array(
            'name' => $product->get_title(),
            'amount' => round((floatval($item_subtotal) + floatval($item_subtotal_tax)) / $item_quantity,2),
            'reference' => strval($product_id),
            'description' => $description,
            'quantity' => $item_quantity,
            'type' => 'sku',
            'item_uri' => $product->get_permalink(),
            'product_code' => $product->get_sku()
        );

        $attachment_ids = WC_Zipmoney_Payment_Gateway_Util::get_product_images_ids($product);
        if (!empty($attachment_ids)) {
            $image_uri = wp_get_attachment_url($attachment_ids[0]);
            $order_item_data['image_uri'] = $image_uri ? $image_uri : null;
        }

        return $order_item_data;
    }

    /**
     * Create the order items
     *
     * @param WC_Session $WC_Session
     * @return array
     */
    protected function _get_order_items(WC_Order $order = null)
    {   
        $order_items = array();

        foreach($order->get_items() as $product_item){
        	
        	$order_item_data = self::_get_order_item_data($product_item);

            $order_items[] = new OrderItem($order_item_data);
        }


        //get the shipping cost

        if (WC_Zipmoney_Payment_Gateway_Util::is_wc_3()){
            $shipping_amount = $order->get_shipping_total() + $order->get_shipping_tax();
        }
        else{
            $shipping_amount = $order->get_total_shipping() + $order->get_shipping_tax();
        }
        
        if ($shipping_amount > 0) {
            $order_items[] = new OrderItem(
                array(
                    'name' => 'Shipping cost',
                    'amount' => round(floatval($shipping_amount), 2),
                    'quantity' => 1,
                    'type' => 'shipping'
                )
            );
        }

        //get the discount

        if(WC_Zipmoney_Payment_Gateway_Util::is_wc_3()){
            $discount_amount = $order->get_discount_total() + $order->get_discount_tax();
        }
        else{
            $discount_amount = $order->get_total_discount(false) ;
        }

        if ($discount_amount > 0) {
            $order_items[] = new OrderItem(
                array(
                    'name' => 'Discount',
                    'amount' => floatval($discount_amount) * -1,
                    'quantity' => 1,
                    'type' => 'discount'
                )
            );
        }

        
        foreach ($order->get_fees() as $fee) {

            if(WC_Zipmoney_Payment_Gateway_Util::is_wc_3()){
                $fee_amount = $fee->get_amount() + $fee->get_total_tax();
                $fee_name = $fee->get_name();
            }
            else{
                $fee_amount = $fee['item_meta']['_line_total'][0] + $fee['item_meta']['_line_total_tax'][0];                
                $fee_name = $fee['name'];
            }

            # code...
            if ($fee_amount > 0 ){
                $order_items[] = new OrderItem(
                        array(
                            'name' => $fee_name,
                            'amount' => $fee_amount,
                            'quantity' => 1,
                            'type' => 'sku'
                            )
                    );
            }
        }

        return $order_items;
    }
    
    /**
     * @param $order
     *
     * check for virtual type products in order list.
    */
    public function has_virtual_product(){
       global $woocommerce;

       $products = $woocommerce->cart->get_cart();

    
       foreach($products as $product_item){
           
           if($product_item['data']->virtual == 'no' || $product_item['data']->downloadable == 'no')
           {
               return false;
           }

        }
       return true;
   }

    /**
     * Create the billing address
     *
     * @param array $billing_array => array(
     *      'zip_billing_address_1' => '',
     *      'zip_billing_address_2' =>,
     *      'zip_billing_city' =>,
     *      'zip_billing_state' =>,
     *      'zip_billing_postcode' =>,
     *      'zip_billing_country' =>,
     *      'zip_billing_first_name' =>,
     *      'zip_billing_last_name' =>
     * )
     * @return \zipMoney\Model\Address
     */
    protected function _create_billing_address(WC_Order $order)
    {
        
        if (WC_Zipmoney_Payment_Gateway_Util::is_wc_3()){
            return new Address(
                array(
                    'line1' => $order->get_billing_address_1() ? $order->get_billing_address_1() : null ,
                    'line2' => $order->get_billing_address_2() ? $order->get_billing_address_2() : null,
                    'city' => $order->get_billing_city() ? $order->get_billing_city() :  null,
                    'state' => $order->get_billing_state() ? $order->get_billing_state() : null,
                    'postal_code' => $order->get_billing_postcode() ? $order->get_billing_postcode() : null,
                    'country' => $order->get_billing_country() ? $order->get_billing_country() : null,
                    'first_name' => $order->get_billing_first_name() ? $order->get_billing_first_name() : null,
                    'last_name' => $order->get_billing_last_name() ? $order->get_billing_last_name() : null
                )
            );
        }
        else{
            $billing_address = $order->get_address();

            return new Address(
                array(
                        'line1' => $billing_address['address_1'] ? $billing_address['address_1'] : null,
                        'line2' => $billing_address['address_2'] ? $billing_address['address_2'] : null,
                        'city' => $billing_address['city'] ? $billing_address['city'] : null,
                        'state' => $billing_address['state'] ? $billing_address['state'] : null ,
                        'postal_code' => $billing_address['postcode'] ? $billing_address['postcode'] : null ,
                        'country' => $billing_address['country'] ? $billing_address['country'] : null,
                        'first_name' => $billing_address['first_name'] ? $billing_address['first_name'] : null,
                        'last_name' => $billing_address['last_name'] ?  $billing_address['last_name'] : null              
                    )
                );

        }
    }

    /**
     * Create the shipping address
     *
     * @param array $shipping_array
     * @return \zipMoney\Model\Address
     */
    protected function _create_shipping_address(WC_Order $order)
    {

        if(WC_Zipmoney_Payment_Gateway_Util::is_wc_3()){
                return new Address(
                array(
                    'line1' => $order->get_shipping_address_1() ? $order->get_shipping_address_1() : null,
                    'line2' => $order->get_shipping_address_2() ? $order->get_shipping_address_2() : null,
                    'city' => $order->get_shipping_city() ? $order->get_shipping_city() : null , 
                    'state' => $order->get_shipping_state() ? $order->get_shipping_state() : null,
                    'postal_code' => $order->get_shipping_postcode() ? $order->get_shipping_postcode() : null ,
                    'country' => $order->get_shipping_country() ? $order->get_shipping_country() : null,
                    'first_name' => $order->get_shipping_first_name() ? $order->get_shipping_first_name() : null,
                    'last_name' => $order->get_shipping_last_name() ? $order->get_shipping_last_name() : null
                )
            );
        }
        else{
            $shipping_address = $order->get_address('shipping');
            return new Address(
                array(
                        'line1' => $shipping_address['address_1'] ? $shipping_address['address_1']  : null,
                        'line2' => $shipping_address['address_2'] ? $$shipping_address['address_2'] : null,
                        'city' => $shipping_address['city'] ? $shipping_address['city'] : null,
                        'state' => $shipping_address['state'] ? $shipping_address['state'] : null,
                        'postal_code' => $shipping_address['postcode'] ? $shipping_address['postcode'] : null,
                        'country' => $shipping_address['country'] ? $shipping_address['country'] : null,
                        'first_name' => $shipping_address['first_name'] ? $shipping_address['first_name'] : null,
                        'last_name' => $shipping_address['last_name'] ? $shipping_address['last_name'] : null              
                    )
            );
        }
        
    }

    /**
     * Retrieve cart total amount regardless of woocommerce version
     */
    protected function _get_cart_total(WC_Order $order = null){
        return $order->get_total();
    }

    /**
     * Retrieve cart fees
     */
    
}