<?php

use \zipMoney\Api\CheckoutsApi;

class WC_Zip_Controller_Checkout_Controller extends WC_Zip_Controller_Abstract_Controller
{

    /**
     * Convert the current checkout session to some static data
     *
     * @param $post_data
     * @return array
     */ 
    public function create_checkout($post_data, WC_Order $order)
    {

        
        //update the customer details
        WC_Zipmoney_Payment_Gateway_Util::update_customer_details($post_data);

        WC_Zipmoney_Payment_Gateway_Util::log('Checkout session started');

        $WC_Zipmoney_Payment_Gateway_API_Request_Checkout = new WC_Zipmoney_Payment_Gateway_API_Request_Checkout(
            $this->WC_Zipmoney_Payment_Gateway,
            new CheckoutsApi()
        );

        $checkout_response = $WC_Zipmoney_Payment_Gateway_API_Request_Checkout->create_checkout(
            WC()->session,
            WC_Zipmoney_Payment_Gateway_Util::get_complete_endpoint_url(),
            $this->WC_Zipmoney_Payment_Gateway_Config->get_merchant_private_key(),
            $order
        );
        
        //save checkout Id into post meta.
        $order_id = WC()->session->get('_post_id');
        update_post_meta($order_id, WC_Zipmoney_Payment_Gateway_Config::META_CHECKOUT_ID, $checkout_response->getId());
       

        return array(
            'redirect_uri' => $checkout_response->getUri(),
            'message' => 'Redirecting to zipMoney.',
            'result' => 'success',
            'checkout_id' => $checkout_response->getId()
        );

    }
}