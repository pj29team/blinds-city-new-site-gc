<?php

/**
 * Plugin Name:       Zip - WooCommerce
 * Plugin URI:        https://wordpress.org/plugins/zipmoney-woocommerce-plugin/
 * Description:       Sell more online & in-store with Zip.
Give your customers the power to pay later, interest free and watch your sales grow.
Take advantage of our fast-growing customer base, proven revenue uplift, fast and simple integration. 
 * Version:           2.0.4
 * Author:            Zip
 * Author URI:        https://www.zip.co/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Github URI:        https://github.com/zipMoney/woocommerce/
 *
 *
 * @version  2.0.4
 * @package  Zip
 * @author   Zip
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}


/**
 * Add zipMoney gateway class to hook
 *
 * @param $methods
 * @return array
 */
function add_zipmoney_gateway_class($methods)
{
    $methods[] = 'WC_Zipmoney_Payment_Gateway';
    return $methods;
}

/**
 * Instantiates the Zipmoney Payment Gateway class and then
 * calls its run method officially starting up the plugin.
 */
function run_zipmoney_payment_gateway()
{
    //Include the vendor repositories
    require_once plugin_dir_path(__FILE__) . 'vendor/autoload.php';

    /**
     * Include the core class responsible for loading all necessary components of the plugin.
     */
   

    if (!class_exists('WC_Payment_Gateway')) {
        //if the woocommerce payment gateway is not defined, then we won't activate the zipmoney payment gateway
        return;
    }

    require_once plugin_dir_path(__FILE__) . 'includes/class-wc-zipmoney-payment-gateway.php';

    $wc_zipmoney_payment_gateway = new WC_Zipmoney_Payment_Gateway();
    $wc_zipmoney_payment_gateway->run();

    //After the class is initialized, we put the class to wc payment options
    add_filter('woocommerce_payment_gateways', 'add_zipmoney_gateway_class');

}

// Call the above function to begin execution of the plugin.
add_action('plugins_loaded', 'run_zipmoney_payment_gateway');
