<?php

get_header();
$sidebar_configs = greenmart_tbay_get_woocommerce_layout_configs();

$page_title = '';
if( is_shop()){
	$page_title .= esc_html__('Shop', 'greenmart');
}else if( is_singular( 'product' ) ) {
	$page_title .= get_the_title();
} else {
	$page_title .= woocommerce_page_title(false);
}

if ( isset($sidebar_configs['left']) && !isset($sidebar_configs['right']) ) {
	$sidebar_configs['main']['class'] .= ' pull-right';
}

?>

<?php do_action( 'greenmart_woo_template_main_before' ); ?>


<section id="main-container" class="main-content <?php echo apply_filters('greenmart_tbay_woocommerce_content_class', 'container');?>">
	<div class="row">
		
		<?php if ( !is_singular( 'product' ) ) : ?>

		<?php if ( isset($sidebar_configs['left']) && isset($sidebar_configs['right']) ) : ?>
			<div class="<?php echo esc_attr($sidebar_configs['left']['class']) ;?>">
			  	<aside class="sidebar sidebar-left" itemscope="itemscope" itemtype="http://schema.org/WPSideBar">
			   		<?php dynamic_sidebar( $sidebar_configs['left']['sidebar'] ); ?>
			  	</aside>
			</div>
		<?php endif; ?>

		<?php endif; ?>

		<div id="main-content" class="<?php  echo ( !is_singular( 'product' ) ) ? 'archive-shop' : 'singular-shop'; ?> col-xs-12 <?php echo ( !is_singular( 'product' ) ) ? esc_attr($sidebar_configs['main']['class']) : ''; ?>">
			<div class="top-archive">

				<?php if ( !is_singular( 'product' ) ) : ?>
					<?php if(is_active_sidebar('top-archive-product')) : ?>
							<?php dynamic_sidebar('top-archive-product'); ?>
						<!-- End Top Archive Product Widget -->
					<?php endif;?>
				<?php endif;?>

			</div>
			<div id="primary" class="content-area">
				<div id="content" class="site-content" role="main">

					 <?php  woocommerce_content(); ?>

				</div><!-- #content -->
			</div><!-- #primary -->
		</div><!-- #main-content -->
		
		<?php if ( !is_singular( 'product' ) ) : ?>
			<?php if ( isset($sidebar_configs['left']) && !isset($sidebar_configs['right']) ) : ?>
				<div class="<?php echo esc_attr($sidebar_configs['left']['class']) ;?>">
				  	<aside class="sidebar sidebar-left" itemscope="itemscope" itemtype="http://schema.org/WPSideBar">
				   		<?php dynamic_sidebar( $sidebar_configs['left']['sidebar'] ); ?>
				  	</aside>
				</div>
			<?php endif; ?>
			
			<?php if ( isset($sidebar_configs['right']) ) : ?>
				<div class="<?php echo esc_attr($sidebar_configs['right']['class']) ;?>">
				  	<aside class="sidebar sidebar-right" itemscope="itemscope" itemtype="http://schema.org/WPSideBar">
				   		<?php dynamic_sidebar( $sidebar_configs['right']['sidebar'] ); ?>
				  	</aside>
				</div>
			<?php endif; ?>
		<?php endif; ?>

	</div>
</section>

<?php if ( is_singular( 'product' ) ) : ?>

	<div class="woo-after-single-product-summary">
		<div class="container">
			<div class="row">

				<?php if ( isset($sidebar_configs['left']) && isset($sidebar_configs['right']) ) : ?>
					<div class="<?php echo esc_attr($sidebar_configs['left']['class']) ;?>">
					  	<aside class="sidebar sidebar-left" itemscope="itemscope" itemtype="http://schema.org/WPSideBar">
					   		<?php dynamic_sidebar( $sidebar_configs['left']['sidebar'] ); ?>
					  	</aside>
					</div>
				<?php endif; ?>
	 

				<div class="woo-after-single-content col-xs-12 <?php echo esc_attr($sidebar_configs['main']['class']); ?>">
					
					<?php 
						/**
						 * woocommerce_after_single_product_summary hook
						 *
						 * @hooked woocommerce_output_product_data_tabs - 10
						 * @hooked woocommerce_upsell_display - 15
						 * @hooked woocommerce_output_related_products - 20
						 */
						do_action( 'woocommerce_after_single_product_summary' ); 
					?>

				</div>

				<?php if ( isset($sidebar_configs['left']) && !isset($sidebar_configs['right']) ) : ?>
					<div class="<?php echo esc_attr($sidebar_configs['left']['class']) ;?>">
					  	<aside class="sidebar sidebar-left" itemscope="itemscope" itemtype="http://schema.org/WPSideBar">
					   		<?php dynamic_sidebar( $sidebar_configs['left']['sidebar'] ); ?>
					  	</aside>
					</div>
				<?php endif; ?>
				
				<?php if ( isset($sidebar_configs['right']) ) : ?>
					<div class="<?php echo esc_attr($sidebar_configs['right']['class']) ;?>">
					  	<aside class="sidebar sidebar-right" itemscope="itemscope" itemtype="http://schema.org/WPSideBar">
					   		<?php dynamic_sidebar( $sidebar_configs['right']['sidebar'] ); ?>
					  	</aside>
					</div>
				<?php endif; ?>


			</div>
		</div>
	</div>

<?php endif; ?>

<?php

get_footer();
