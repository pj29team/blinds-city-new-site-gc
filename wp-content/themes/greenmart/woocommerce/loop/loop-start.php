<?php
/**
 * Product Loop Start
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
$woo_mode = greenmart_tbay_woocommerce_get_display_mode();
?>
<div class="products products-<?php if ( $woo_mode == 'list' ) { ?>list products-grid<?php }else{ ?>grid<?php } ?>"><div class="row">