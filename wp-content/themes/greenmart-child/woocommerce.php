<?php
global $product, $post;
get_header();
$sidebar_configs = greenmart_tbay_get_woocommerce_layout_configs();

$page_title = '';
if( is_shop()){
	$page_title .= esc_html__('Shop', 'greenmart');
}else if( is_singular( 'product' ) ) {
	$page_title .= get_the_title();
} else {
	$page_title .= woocommerce_page_title(false);
}

if ( isset($sidebar_configs['left']) && !isset($sidebar_configs['right']) ) {
	$sidebar_configs['main']['class'] .= ' pull-right';
}

?>

<?php do_action( 'greenmart_woo_template_main_before' ); ?>


<section id="main-container" class="main-content <?php echo apply_filters('greenmart_tbay_woocommerce_content_class', 'container');?> <?php  echo ( !is_singular( 'product' ) ) ? 'archive-shop-container' : 'singular-shop-container'; ?>">
	<div class="row">

		<?php if ( is_shop() ) echo do_shortcode(get_post_field('post_content', get_option( 'woocommerce_shop_page_id' ))); ?>
		
		<?php if ( !is_singular( 'product' ) ) : ?>

		<?php if ( isset($sidebar_configs['left']) && isset($sidebar_configs['right']) ) : ?>
			<div class="<?php echo esc_attr($sidebar_configs['left']['class']) ;?>">
			  	<aside class="sidebar sidebar-left" itemscope="itemscope" itemtype="http://schema.org/WPSideBar">
			   		<?php dynamic_sidebar( $sidebar_configs['left']['sidebar'] ); ?>
			  	</aside>
			</div>
		<?php endif; ?>

		<?php endif; ?>

		<div id="main-content" class="<?php  echo ( !is_singular( 'product' ) ) ? 'archive-shop' : 'singular-shop'; ?> col-xs-12 <?php echo ( !is_singular( 'product' ) ) ? esc_attr($sidebar_configs['main']['class']) : ''; ?>">
			<div class="top-archive">

				<?php if ( !is_singular( 'product' ) ) : ?>
					<?php if(is_active_sidebar('top-archive-product')) : ?>
							<?php dynamic_sidebar('top-archive-product'); ?>
						<!-- End Top Archive Product Widget -->
					<?php endif;?>
				<?php endif;?>

			</div>
			<div id="primary" class="content-area">
				<div id="content" class="site-content" role="main">

					 <?php  woocommerce_content(); ?>

				</div><!-- #content -->
			</div><!-- #primary -->
		</div><!-- #main-content -->
		
		<?php if ( !is_singular( 'product' ) ) : ?>
			<?php if ( isset($sidebar_configs['left']) && !isset($sidebar_configs['right']) ) : ?>
				<div class="<?php echo esc_attr($sidebar_configs['left']['class']) ;?>">
				  	<aside class="sidebar sidebar-left" itemscope="itemscope" itemtype="http://schema.org/WPSideBar">
				   		<?php dynamic_sidebar( $sidebar_configs['left']['sidebar'] ); ?>
				  	</aside>
				</div>
			<?php endif; ?>
			
			<?php if ( isset($sidebar_configs['right']) ) : ?>
				<div class="<?php echo esc_attr($sidebar_configs['right']['class']) ;?>">
				  	<aside class="sidebar sidebar-right" itemscope="itemscope" itemtype="http://schema.org/WPSideBar">
				   		<?php dynamic_sidebar( $sidebar_configs['right']['sidebar'] ); ?>
				  	</aside>
				</div>
			<?php endif; ?>
		<?php endif; ?>

	</div>
</section>

<?php if ( is_singular( 'product' ) ) : ?>


	<div class="woo-after-single-product-summary <?php if( $product->is_type( 'simple' ) ) echo 'simple'; ?>">
		<div class="container">
			<div class="row">
				<?php if( $product->is_type( 'variable' ) ) : ?>
				<?php 
					$manweek = rwmb_meta( 'bcmeta_manufacture_weeks' );
					$del = rwmb_meta( 'bcmeta_delivery' );
					$manwar = rwmb_meta( 'bcmeta_manufacture_warranty' );
				?>

				<div id="prod-info" class="product-more-info">
					<div class="get-samples">
						<i class="bc-icon sample-icon"></i>
						<span>Want to see this fabric before you buy it? Get your <a href="<?php get_bloginfo('url')?>/samples" class="button">FREE SAMPLE</a></span>
					</div>
					<div class="more-info">
						<ul>
							<li>
								<i class="bc-icon factory-icon"></i>
								<p>Manufacture</p>
								<p><?php echo (empty($manweek)) ? '2-3 weeks' : $manweek;?></p>
							</li>
							<li class="info-mid">
								<i class="bc-icon manufacture-icon"></i>
								<p>Leaves warehouse in</p>
								<p>Delivery: <?php echo (empty($del)) ? '3 weeks' : $del;?></p>
							</li>
							<li>
								<i class="bc-icon warranty-icon"></i>
								<p><?php echo (empty($manvar)) ? '5 years' : $manwar;?> Manufacturer's</p>
								<p>Warranty</p>
							</li>
						</ul>
					</div>
				</div>
			<?php endif; ?>
				<div class="woo-after-single-content col-xs-12 <?php echo esc_attr($sidebar_configs['main']['class']); ?>">
					<?php if( $product->is_type( 'variable' ) ) : ?>
					<h3>Customise Your Order</h3>
					<h5>Begin by selecting your options</h5>
					<?php endif; ?>
					<?php 
						/**
						 * woocommerce_after_single_product_summary hook
						 *
						 * @hooked woocommerce_output_product_data_tabs - 10
						 * @hooked woocommerce_upsell_display - 15
						 * @hooked woocommerce_output_related_products - 20
						 */
						do_action( 'woocommerce_after_single_product_summary' ); 
					?>

				</div>



			</div>
		</div>
	</div>

<?php endif; ?>

<?php

get_footer();
