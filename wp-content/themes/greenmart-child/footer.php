<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage greenmart
 * @since greenmart 1.0
 */
global $product;
$footer = apply_filters( 'greenmart_tbay_get_footer_layout', 'default' );
$edit_cart_item = (isset($_POST['edit_cart_item']) && $_POST['edit_cart_item'])?$_POST['edit_cart_item']:0;
?>

	</div><!-- .site-content -->

	<footer id="tbay-footer" class="tbay-footer <?php if( is_product() && $product->is_type( 'simple' ) ) echo 'simple'; if ( $edit_cart_item ) echo 'edit-cart'?>" role="contentinfo">

		<?php if ( $edit_cart_item ): ?>
            <div id="product-add-to-cart">
                <div class="container">
                    <div class="pull-left zippay">
                        <i class="bc-icon zip-icon"></i>
                        <div class="zippay-desc">
                            <p>Up to 12 Months Interest Free</p>
                            <a href="#">Choose ZipPay at the checkout</a>
                        </div>
                    </div>
                    <div class="pull-right">
                        <div class="total-price"><span class="formattedTotalPrice ginput_total">$0</span></div>
                        <div class="add-btn"><a href="#" class="button button-add_to_cart add_to_cart_button ajax_add_to_cart" disabled="disabled">Update to cart</a></div>
<!--                        <div class="add-btn"><input name="update_cart"  class="button update-button" value="Update Cart"></div>-->
                    </div>
                </div>
            </div>
        <?php endif; ?>
		<?php if ( !empty($footer) ): ?>
			<?php if ( is_product() ):?>
				<?php if( $product->is_type( 'variable' ) ) : ?>
				<div id="product-add-to-cart">
					<div class="container">
						<div class="pull-left zippay">
							<i class="bc-icon zip-icon"></i>
							<div class="zippay-desc">
								<p>Up to 12 Months Interest Free</p>
								<a href="#">Choose ZipPay at the checkout</a>
							</div>
						</div>
						<div class="pull-right">
							<div class="total-price"><span class="formattedTotalPrice ginput_total">$0</span></div>
							<div class="add-btn"><a href="#" class="button button-add_to_cart add_to_cart_button ajax_add_to_cart" disabled="disabled">Add to cart</a></div>
						</div>
					</div>
				</div>
				<?php endif; ?>
			<?php else: ?>
			<div class="footer-desktop hidden-xs">
				<?php greenmart_tbay_display_footer_builder($footer); ?>
			</div>
			<div class="footer-mobile hidden-lg hidden-md hidden-sm">
				<?php greenmart_tbay_display_footer_builder('footer-mobile'); ?>
			</div>
			<?php endif;?>
		<?php else: ?>
			<?php if ( is_active_sidebar( 'footer' ) ) : ?>
				<div class="footer">
					<div class="container">
						<div class="row">
							<?php dynamic_sidebar( 'footer' ); ?>
						</div>
					</div>
				</div>
			<?php endif; ?>
			<?php 
			// $footer_type = greenmart_tbay_get_config('footer_type', '');
			// bc_footer_type_style($footer_type);
	 	//  	greenmart_tbay_display_footer_builder($footer_type);
	 	 	?>
	 	 	<div class="footer-desktop hidden-xs">
				<?php greenmart_tbay_display_footer_builder($footer); ?>
			</div>
			<div class="footer-mobile hidden-lg hidden-md hidden-sm">
				<?php greenmart_tbay_display_footer_builder('footer-mobile'); ?>
			</div>
		<?php endif; ?>			
	</footer><!-- .site-footer -->

	<?php $tbay_header = apply_filters( 'greenmart_tbay_get_header_layout', greenmart_tbay_get_config('header_type') );
		if ( empty($tbay_header) ) {
			$tbay_header = 'v1';
		}
	?>
	
	<?php 

	$_id = greenmart_tbay_random_key();

	?>

	<?php
	if ( greenmart_tbay_get_config('back_to_top') ) { ?>
		<div class="tbay-to-top <?php echo esc_attr($tbay_header); ?>">
			
			<?php if( class_exists( 'YITH_WCWL' ) ) { ?>
			<a class="text-skin wishlist-icon" href="<?php $wishlist_url = YITH_WCWL()->get_wishlist_url(); echo esc_url($wishlist_url); ?>"><i class="icofont icofont-heart-alt" aria-hidden="true"></i><span class="count_wishlist"><?php $wishlist_count = YITH_WCWL()->count_products(); echo esc_attr($wishlist_count); ?></span></a>
			<?php } ?>
			
			
			<?php if ( !(defined('GREENMART_WOOCOMMERCE_CATALOG_MODE_ACTIVED') && GREENMART_WOOCOMMERCE_CATALOG_MODE_ACTIVED) && defined('GREENMART_WOOCOMMERCE_ACTIVED') && GREENMART_WOOCOMMERCE_ACTIVED ): ?>
			<!-- Setting -->
			<div class="tbay-cart top-cart hidden-xs">
				<a href="<?php echo esc_url( wc_get_cart_url() ); ?>" class="mini-cart">
					<i class="icofont icofont-shopping-cart"></i>
					<span class="mini-cart-items-fixed">
					   <?php echo sprintf( '%d', WC()->cart->cart_contents_count );?>
					</span>
				</a>
			</div>
			<?php endif; ?>
			
			<a href="#" id="back-to-top">
				<p><?php esc_html_e('TOP', 'greenmart'); ?></p>
			</a>
		</div>
		
		
	<?php
	}
	?>
	
	

</div><!-- .site -->

<?php wp_footer(); ?>

</body>
</html>

