<?php if ( has_nav_menu( 'nav-account' ) ): ?>
    <?php $current_user = wp_get_current_user(); ?>
    <div class="top-menu">
        <div class="dropdown menu">
            <span data-toggle="dropdown" class="dropdown-toggle"><span class="user"><?php echo esc_html( $current_user->display_name); ?> <i class="bc-icon user-icon"></i><i class="bc-icon angle-down-icon"></i></span></span>
            <div class="dropdown-menu dropdown-menu-right">
                <nav class="tbay-nav-account" role="navigation">
                    <?php
                        $args = array(
                            'theme_location'  => 'nav-account',
                            'menu_class'      => 'tbay-menu-top list-inline',
                            'fallback_cb'     => '',
                            'menu_id'         => 'nav-account'
                        );
                        wp_nav_menu($args);
                    ?>
                </nav>
            </div>
        </div>
    </div>
<?php endif; ?>