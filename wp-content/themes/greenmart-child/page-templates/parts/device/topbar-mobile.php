<?php   global $woocommerce; ?>
<div class="topbar-top hidden-lg hidden-md clearfix">
	<div class="col-md-6">
	    <div class="pull-left call-number">
			<a href="tel:1300055888">
				<i class="bc-icon phone-icon"></i>
				<span class="num">&nbsp;&nbsp;1300&nbsp;055&nbsp;888</span>
			</a>
		</div>
	</div>
	<div class="col-md-6">
	    <div class="pull-right help-centre">
			<a href="#">
				HELP CENTER
			</a>
		</div>
	</div>
</div>
<div class="topbar-device-mobile hidden-lg hidden-md clearfix" style="display: flex;position: relative;border: 0;box-shadow: none;padding: 20px 30px;">
	<?php
		$mobilelogo = greenmart_tbay_get_config('mobile-logo');
	?>
		<div class="active-mobile">
			<button data-toggle="offcanvas" data-target="mobile-menu" data-action="open" class="slide-menu-control btn btn-sm btn-danger btn-offcanvas btn-toggle-canvas offcanvas" type="button">
			<i class="fa fa-bars"></i>
			</button>
		</div>
		<div class="mobile-logo" style="width: 100%; text-align: center;">
			<?php if( isset($mobilelogo['url']) && !empty($mobilelogo['url']) ): ?>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
					 <i class="bc-icon bc-logo"></i>
				</a>
			<?php else: ?>
				<div class="logo-theme">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
						 <i class="bc-icon bc-logo"></i>
					</a>
				</div>
			<?php endif; ?>
		</div>
		<?php if ( !(defined('GREENMART_WOOCOMMERCE_CATALOG_MODE_ACTIVED') && GREENMART_WOOCOMMERCE_CATALOG_MODE_ACTIVED) && defined('GREENMART_WOOCOMMERCE_ACTIVED') && GREENMART_WOOCOMMERCE_ACTIVED ): ?>
			<div class="device-cart">
				<a class="mobil-view-cart" href="<?php echo esc_url( wc_get_cart_url() ); ?>" >
					<i class="bc-icon cart-icon"></i>
					<span class="mini-cart-items"><?php echo sprintf( '%d', $woocommerce->cart->get_cart_contents_count() );?></span>
				</a>   
			</div>
		<?php endif; ?>

</div>
