<?php
    $logo = greenmart_tbay_get_config('media-logo');
?>

<?php if( isset($logo['url']) && !empty($logo['url']) ): ?>
    <div class="logo">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
            <i class="bc-icon bc-logo"></i>
        </a>
    </div>
<?php else: ?>
    <div class="logo logo-theme">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
           <i class="bc-icon bc-logo"></i>
        </a>
    </div>
<?php endif; ?>