<?php if ( defined('GREENMART_WOOCOMMERCE_ACTIVED') && GREENMART_WOOCOMMERCE_ACTIVED ): ?>
	<ul class="list-inline acount style1">
		<?php if( !is_user_logged_in() ){ ?>
			<li> <a href="<?php echo esc_url( get_permalink( get_option('woocommerce_myaccount_page_id') ) ); ?>" class="login-join" title="<?php esc_html_e('Login','greenmart'); ?>"> <span><?php esc_html_e('Login | Join', 'greenmart'); ?></span> <i class="bc-icon user-icon"></i></a></li>
		<?php }else{ ?>
			<?php $current_user = wp_get_current_user(); ?>
			<li>
			<ul>
				<li><?php get_template_part( 'page-templates/parts/nav-account' ); ?></li>
			</ul>
			</li>
			<li>
				<a class="login" href="<?php echo wp_logout_url(home_url()); ?>"><?php esc_html_e('Logout ','greenmart'); ?></a>
			</li>
		<?php } ?>
	</ul>
<?php endif; ?>