<?php $tbay_header = apply_filters( 'greenmart_tbay_get_header_layout', greenmart_tbay_get_config('header_type') );
	if ( empty($tbay_header) ) {
		$tbay_header = 'v1';
	}
    $location = 'mobile-menu';
    $tbay_location  = '';
    if ( has_nav_menu( $location ) ) {
        $tbay_location = $location;
    }
?> 
<?php $current_user = wp_get_current_user(); ?>
<div id="tbay-mobile-menu" class="testings tbay-offcanvas hidden-lg hidden-md <?php echo esc_attr($tbay_header);?>"> 
         <button type="button" class="btn btn-toggle-canvas btn-danger" data-toggle="offcanvas">
                <i class="fa fa-close"></i> 
            </button>
        
            <div class="tbay-offcanvas-body">
        <div class="offcanvas-head bg-primary">
           <div class="login-join">
               <i class="icofont icofont-user login"></i>
               <?php if( !is_user_logged_in() ){ ?>
               <a href="<?php echo esc_url( get_permalink( get_option('woocommerce_myaccount_page_id') ) ); ?>" class="button" title="<?php esc_html_e('Login','greenmart'); ?>">LOGIN</a>
               <a href="<?php echo esc_url( get_permalink( get_option('woocommerce_myaccount_page_id') ) ); ?>/?action=register" class="button join" title="<?php esc_html_e('Join','greenmart'); ?>">REGISTER</a>
            <?php } else { ?>
                <span class="user"><?php echo esc_html( $current_user->display_name); ?></span>
                <a class="login button" href="<?php echo wp_logout_url(home_url()); ?>"><?php esc_html_e('Logout ','greenmart'); ?></a>
            <?php } ?>
           </div>
        </div>
            <p class="shop-by">SHOP BY</p>
        <nav id="menu-mobile" class="slide-menu navbar snavbar-offcanvas snavbar-static" role="navigation">
            <?php
                $args = array(
                    'theme_location' => $tbay_location,
                    // 'container_class' => 'navbar-collapse navbar-offcanvas-collapse',
                    // 'menu_class' => 'treeview nav navbar-nav',
                    // 'menu_class' => 'navbar-nav',
                    'fallback_cb' => '',
                    // 'menu_id' => 'main-mobile-menu',
                    // 'link_before' => '<div>',
                    // 'link_after' => '</div>',
                    // 'walker' => new Menu_With_Description()
                );
                wp_nav_menu($args);
            ?>
        </nav>

    </div>
</div>
<!-- <script type="text/javascript">
    jQuery(document).ready(function($){ 
        $('.sub-menu').prepend('<p class="shop-by">SELECT A BLIND</p>');
    });
</script> -->