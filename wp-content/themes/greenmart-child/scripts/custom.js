// devlog
var DEVLOG_ON = 1;

// expands steps
var DEBUG_EXPAND_STEPS = 0;

function devlog() {
  if ((typeof DEVLOG_ON === 'undefined') || !DEVLOG_ON) {
    return;
  }

  if (
    (typeof console === 'object') && (typeof console.log === 'function')
  ) {
    console.log.apply(console, arguments);
  }
}

function separatedValue(val) {
  if (typeof val === 'undefined') {
    return '';
  }

  if (val){

  parts = val.split('|');

  return parts[0];
  } else {
    return '';
  }
}

jQuery(document).ready(function($) {



  // if (jQuery(window).width() <) {
  //  jQuery(".dropdown").hover(function() {
  //      jQuery('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
  //      jQuery(this).toggleClass('open');
  //  }, function() {
  //      jQuery('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
  //      jQuery(this).toggleClass('open');
  //  });
  // }

  /* Showing qty fields back, since we cannot edit the gravity form properly, im removing the off screen class from qty fields */

  $('[class*="gfield_quantity"]').removeClass('gf-off-screen');

  /* end showing qty function */

  $('.fancybox').fancybox();

  var NEXT_LI_SELECTOR = 'li' + ':not(.gf-off-screen)' + ':not(.gfield_hidden_product)' + ':not(.gsection-buttons)';

  var $w = $(window);

  ////////////////////////////////////////////////////////////////////////////

  var $product_form = $('.variations_form');
  var $product_totals = $('.product_totals');
  var $gform_variation = $('.gform_variation_wrapper');
  //var $total_price = $product_totals.find('.formattedTotalPrice');
  var $total_price = $('#product-add-to-cart').find('.formattedTotalPrice');
  var product_id = parseInt($product_form.data('product_id'), 10);
  var product_variations = $product_form.data('product_variations');
  var $variation_id = $product_form.find('input[name="variation_id"]');
  var variation_id = getVariationId();
  var variation_map = [];
  var variation_ids = [];
  var variation_set = {};
  var variation_id2object = {};

  $product_form.submit(function() {
    var var_id = getVariationId();

    if (var_id === -1) {
      return false;
    }

    $variation_id.val(var_id);

    return true;
  });

  var $add_to_cart_button = $('.single_add_to_cart_button');
  $add_to_cart_button.attr('disabled', 'disabled');
  $add_to_cart_button.click(function() {
    return $add_to_cart_button.hasAttr('disabled');
  });

  if (product_variations) {
    for (var i = 0, l = product_variations.length; i < l; i++) {
      var variation = product_variations[i];

      if (!(variation.variation_is_active) || !(variation.variation_is_visible)) {
        continue;
      }

      var attrs = [];

      for (var attr in variation.attributes) {
        if (variation.attributes.hasOwnProperty(attr)) {
          attrs.push(variation.attributes[attr]);
        }
      }

      variation_map.push(attrs.join('&'));
      variation_ids.push(variation.variation_id);
      variation_id2object[variation.variation_id] = variation;
    }
  }

  ////////////////////////////////////////////////////////////////////////////

  if ($gform_variation.length) {
    function updateProductTotalsPositions(debug) {
      if (typeof debug === 'undefined') {
        debug = true;
      }

      //if (debug) {
      //  devlog('updateProductTotalsPositions');
      //}

      var pt_height = $product_totals.height();

      var w_scrolltop = $w.scrollTop();
      var w_height = $w.height();
      var w_boundary = w_scrolltop + w_height;

      var gfv_position = $gform_variation.offset();
      var gfv_height = $gform_variation.height();
      var gfv_boundary = gfv_position.top + gfv_height + pt_height;

      if (w_boundary < gfv_boundary) {
        var different = gfv_boundary - w_boundary;

        // Nam's change, don't change bottom
        // $product_totals.css('bottom', different);

        /* if (typeof $zopim.livechat !== 'undefined') {
          $zopim.livechat.button.setOffsetVertical(pt_height);
        } */
      } else {
        $product_totals.css('bottom', 0);

        /* if (typeof $zopim.livechat !== 'undefined') {
          $zopim.livechat.button.setOffsetVertical(0);
        } */
      }
    }

    $w.on('scroll resize orientationchange', function() {
      updateProductTotalsPositions(false);
    });

    updateProductTotalsPositions();
  }

  ////////////////////////////////////////////////////////////////////////////

  // Total price:
  if (typeof gform !== 'undefined') {
    var $product_price = $('.product_price');

    window.update_dynamic_price = function(gform_total) {
      var f1_price;
      if ( $('#db-blind-form input[name="dbl_filter_1_price"]').length  && $('input[name="dbl_filter_1_price"]').val() !== '') {
        f1_price = $('input[name="dbl_filter_1_price"]').val(); // get price of 1st filter if field exists
        // console.log(f1_price);
      } else {
        f1_price = 0; 
      }
      if (product_id || variation_id) {
        var the_id = 0;

        if (variation_id) {
          the_id = variation_id;
        } else {
          the_id = product_id;
        }

        var base_price = wc_gravityforms_params.prices[the_id];

        if (product_id == 258) { // Plantation Shutters
          //var wim = parseInt(bc_width,  10);
          //var him = parseInt(bc_height, 10);
          //
          //var gf_total_start = gform_total;
          //
          //devlog('wim = ' + wim + ', him = ' + him);
          //
          //if (!isNaN(wim) && !isNaN(him)) {
          //  wim /= 1000;
          //  him /= 1000;
          //
          //  gform_total *= wim * him;
          //}
          //
          //devlog('gform_total (start) = ' + gf_total_start
          //+ ', gform_total (end) = ' + gform_total);
        }

        var price;

        if (product_id == 258 || product_id == 1952 || product_id == 1932 || product_id == 2351 || product_id == 21721 || product_id == 21740 || product_id == 21771 || product_id == 21790 || product_id == 21809 || product_id == 21828) { /* Plantation Shutters, sydney shutters and express shutters */
          var price_text = $product_price.text();
          var product_qty = $('.gfield_price .ginput_container input').val();
          price_text = price_text.replace(not_digits_regexp, '');

          price = (parseFloat(price_text) + parseFloat(gform_total)) * parseFloat(product_qty);
          $('.quantity .input-text.qty.text').val(product_qty);
        } else {
          if ( $('.bc-fg-field-cur').length != 0 ) {
            var $bc_height_field = $('#room-height-1');
            var h = parseFloat($bc_height_field.val());
            var p = parseFloat(base_price) + parseFloat(gform_total);
            if ( h > 2800.00 ) {// drop greater than 2800 add 15% (custom curtains)
              price = p * 1.15;
              price *= 1.1;
            } else {
              price = p;
              price *= 1.1; // Tax?
            }
          } else {
            price = parseFloat(base_price) + parseFloat(gform_total);
            //price *= 1.1; // Tax?
          }
        }
        var price_r = Math.round(price);
        var price_f;
        f1_price = parseFloat(f1_price);
        if (price_r == f1_price){ // double blinds step 3 add total price and filter 1 price, regardless if f1_price is 0 or not
          if ( $('#db-blind-form #pa_roller-blinds-colour').val() ) {
            price_f = price + f1_price;
          } else {
            price_f = price;
          }
        } else {
          price_f = price + f1_price;
        }
        $total_price.html(
          accounting.formatMoney(
            Math.round(price_f), {
              'symbol': wc_gravityforms_params.currency_format_symbol,
              'decimal': wc_gravityforms_params.currency_format_decimal_sep,
              'thousand': wc_gravityforms_params.currency_format_thousand_sep,
              'precision': wc_gravityforms_params.currency_format_num_decimals,
              'format': wc_gravityforms_params.currency_format
            }
          ) + wc_gravityforms_params.price_suffix
        );
      }

      if ( $('input[name="dbl_filter_2_price"]').length ) $('input[name="dbl_filter_2_price"]').val(price_r);

      devlog('[update_dynamic_price] gform_total = ' + gform_total);
      return gform_total;
    }

    gform.addHook('filter', 'gpcp_price', function(price, gpcp_info) {
      devlog('[gpcp_price] price = ' + price);

      return price;
    });
  }

  ////////////////////////////////////////////////////////////////////////////

  $('#pa_filter, #pa_material').change(function(event) {
    var $filter = $(this);
    var filter_val = $filter.val();
    var $filter_next = $filter.closest('.swatch-filter-next');

    var $li = $filter.closest('li');
    var $next_lis = $li.nextAll(NEXT_LI_SELECTOR);
    var $next_li = $next_lis.first();

    $li.nextAll(NEXT_LI_SELECTOR).first()
      .addClass('gfield-hidden')
      .find('.selected')
      .removeClass('selected')
      .end()
      .find('.swatch-wrapper')
      .addClass('swatch-hidden')
      .end()
      .find('.swatches-load-more').hide();

    variation_set[$filter.attr('id')] = filter_val;

    var filter_next = $filter_next.data('filter_next');
    var filter_next_elem = $filter_next.data('filter_next_elem');
    var filter_next_look = $filter_next.data('filter_next_look');

    var $filters = $next_li.find('.swatch-filters');

    if ($filters.length) {
      var filter_for = $filters.data('filter_for');
      var $picker = $('#picker_' + filter_for);
      var $filter_elems = $filters.find('.swatch-filter');

      $filter_elems.removeClass('selected');
      $filter_elems.removeClass('next-elem-shown');

      $filter_elems.each(function() {
        var $elem = $(this);
        var elem_val = $elem.data('value');

        if ($.inArray(elem_val, filter_next[filter_val]) === -1) {
          return;
        }

        $elem.addClass('next-elem-shown');
      });
    } else {
      var $next_elems = $next_li.find(filter_next_elem);
      $next_elems.addClass('swatch-hidden');

      $next_elems.each(function() {
        var $elem = $(this);
        var elem_val = $elem.attr(filter_next_look);

        if ($.inArray(elem_val, filter_next[filter_val]) === -1) {
          return;
        }

        $elem.removeClass('swatch-hidden');
      });
    }

    updateProductTotalsPositions();
  });

  function showSwatches($picker, filter_expr, offset, limit) {
    $picker.data('last_expr', filter_expr);
    $picker.data('last_offset', offset);
    $picker.data('last_limit', limit);

    var matches_counter = 0;
    var visible_counter = 0;
    var max_offset;
    var items_skipped = 0;

    if (limit) {
      max_offset = offset + limit;
    }

    var variation_prefix = '';

    if (typeof variation_set['pa_filter'] !== 'undefined') {
      variation_prefix += variation_set['pa_filter'] + '&';
    }

    if (typeof variation_set['pa_fabric'] !== 'undefined') {
      variation_prefix += variation_set['pa_fabric'] + '&';
    }

    if (typeof variation_set['pa_material'] !== 'undefined') {
      variation_prefix += variation_set['pa_material'] + '&';
    }

    if (typeof variation_set['pa_size'] !== 'undefined') {
      variation_prefix += variation_set['pa_size'] + '&';
    }

    $picker.find('.swatch-wrapper').each(function() {
      var $swatch = $(this);
      var value = $swatch.data('value');

      if (
        ($.inArray(variation_prefix + value, variation_map) >= 0) && value.match(filter_expr)
      ) {
        if (++matches_counter >= offset) {
          if (max_offset) {
            if (matches_counter <= max_offset) {
              $swatch.removeClass('swatch-hidden');
            } else {
              $swatch.addClass('swatch-hidden');

              items_skipped++;
            }
          } else {
            $swatch.removeClass('swatch-hidden');
          }
        }
      } else {
        $swatch.addClass('swatch-hidden');
      }
    });

    var $load_more = $picker.next('.swatches-load-more');

    if (items_skipped) {
      $load_more.show();
    } else {
      $load_more.hide();
    }

    updateProductTotalsPositions();
  }

  var $swatch_filters = $('.swatch-filter');

  $swatch_filters.find('a').click(function(event) {
    var $filter_a = $(this);
    var $filter = $filter_a.closest('.swatch-filter');
    var $filters = $filter.closest('.swatch-filters');

    $swatch_filters.removeClass('selected');
    $filter.addClass('selected');

    var filter_for = $filters.data('filter_for');
    var filter_name = $filters.data('filter_name');
    var filter_val = $filter.data('value');
    var filter = $filter.data('filter');
    var filter_desc = $filter.data('desc');
    var filter_expr = new RegExp(filter, 'i');

    var $filter_desc = $filters.next('.swatch-filters-desc');

    if ($filter_desc.length === 0) {
      $filter_desc = $('<div class="swatch-filters-desc">');
      $filter_desc.insertAfter($filters);
    }

    if (filter_desc) {
      $filter_desc.html(filter_desc.replace(/[\r\n]+/g, '<br>'));
      $filter_desc.show();
    } else {
      $filter_desc.empty();
      $filter_desc.hide();
    }

    $('#' + filter_name).val(filter_val).trigger('change');

    variation_set[filter_name] = filter_val;

    var $picker = $('#picker_' + filter_for);

    $('#' + filter_for).val('').trigger('change')
      .find('.selected').removeClass('selected');

    showSwatches($picker, filter_expr, 0, $picker.data('visible_max'));

    return false;
  });

  var $load_more = $('.swatches-load-more');

  $load_more.on('click', '.swatches-load-more-btn', function() {
    var $btn = $(this);

    var btn_for = $btn.data('for');

    var $picker = $('#picker_' + btn_for);

    var last_expr = $picker.data('last_expr');
    var last_offset = $picker.data('last_offset');
    var last_limit = $picker.data('last_limit');

    var limit = $picker.data('visible_max') + last_limit;

    showSwatches($picker, last_expr, 0, limit);

    return false;
  });

  ////////////////////////////////////////////////////////////////////////////

  var $add_sample_popup = $('#add_sample-popup');
  var $add_sample_popup_close = $add_sample_popup.find('.popup-close');
  var $add_sample_popup_title = $add_sample_popup.find('.popup-title');
  var $add_sample_popup_desc = $add_sample_popup.find('.popup-description');
  var $add_sample_popup_image = $add_sample_popup.find('.popup-image');
  var $add_sample_order_btn = $add_sample_popup.find('.popup-order-btn');
  var $add_sample_select_btn = $add_sample_popup.find('.popup-select-btn');
  var $add_sample_ajax_result = $add_sample_popup.find('.popup-ajax-result');

  var adding_sample = false;

  var show_colour_popup = true;

  function getVariationIdStr() {
    var variation_idstr = '';

    $product_form.find('.field-group-1 select').each(function() {
      if (variation_idstr !== '') {
        variation_idstr += '&';
      }

      variation_idstr += $(this).val();
    });

    return variation_idstr;
  }

  function getVariationId(idstr) {
    if (typeof idstr === 'undefined') {
      idstr = getVariationIdStr();
    }

    var id = $.inArray(idstr, variation_map);

    if (id >= 0) {
      return variation_ids[id];
    }

    return -1;
  }

  $add_sample_order_btn.click(function(event) {
    if (adding_sample) {
      return false;
    }

    adding_sample = true;

    var var_id = $add_sample_popup.data('variation_id');
    var swatch_attr = $add_sample_popup.data('swatch_attr');
    var swatch_val = $add_sample_popup.data('swatch_val');

    if (var_id === -1) {
      adding_sample = false;
      $add_sample_ajax_result
        .html('ERROR! Something is wrong.')
        .addClass('result_error')
        .show();

      return false;
    }

    $.ajax({
      'url': php_array.admin_ajax,
      'type': 'POST',
      'data': {
        'action': 'bc_add_sample',
        'prod_id': product_id,
        'var_id': var_id,
        'swatch_attr': swatch_attr,
        'swatch_val': swatch_val
      },
      'beforeSend': function(xhr, settings) {
        $add_sample_ajax_result
          .hide()
          .removeClass('result_error')
          .removeClass('result_success');
        $add_sample_popup.addClass('ajax_loading');
      },
      'complete': function(xhr, textstatus) {
        $add_sample_popup.removeClass('ajax_loading');
        adding_sample = false;
      },
      'success': function(data) {
        $add_sample_ajax_result
          .html('FREE Sample Added!')
          .addClass('result_success')
          .show();
        var seconds_left = 5;
        var interval_id = setInterval(function() {
          if (seconds_left <= 0) {
            clearInterval(interval_id);
            $add_sample_popup.hide();
            return;
          }

          $add_sample_ajax_result.html(
            'FREE Sample Added!' + ' Autoclose in ' + seconds_left + '...'
          );

          seconds_left--;
        }, 1000);
        // updateCart();
      var fragments = data.fragments;

      // Replace fragments
      if ( fragments ) {
          $.each( fragments, function( key, value ) {
              $( key ).replaceWith( value );
          });
      }
      $('html, body').animate({
          scrollTop: $("#wrapper-container").offset().top
      }, 1000);
      $('.tbay-topcart #cart').addClass('open');
      },
      'error': function() {
        $add_sample_ajax_result
          .html('ERROR! Try Again.')
          .addClass('result_error')
          .show();
      }
    });

    return false;
  });

  $add_sample_select_btn.click(function(event) {
    $add_sample_popup.hide();

    return false;
  });

  $add_sample_popup_close.click(function() {
    $add_sample_popup.hide();

    return false;
  });

  $('.field-group-1').find('select').change(function() {
    var $select = $(this);

    $add_sample_popup.hide();
    $add_sample_ajax_result.hide();

    variation_set[$select.attr('id')] = $select.val();

    $bc_width_field.trigger('keyup');
    $bc_height_field.trigger('keyup');
  });

  $('.field-group-1').last().find('select').change(function() {
    variation_id = getVariationId();

    if (variation_id === -1) {
      $product_form.trigger('reset_variation');

      return;
    }

    $variation_id.val(variation_id);

    var curr_var = variation_id2object[variation_id];

    if (typeof curr_var.sku === 'undefined') {
      curr_var.sku = null;
    }

    curr_var.image_src = [];

    $product_form.trigger('found_variation', [curr_var]);

    var $select = $(this);
    
    
    
    var $parent = $select.closest('.swatch-control');
    var $swatch = $parent.find('.swatch-wrapper.selected');
    var $swatch_anchor = $swatch.find('.swatch-anchor');
    var more_large_src = $swatch.find('img').data('more_large_src');
    var desc = $swatch_anchor.data('desc');
    
    var swatch_attr = $swatch.data('attribute');
    var swatch_val = $swatch.data('value');
    
    //devlog(swatch_attr, swatch_val);

    $add_sample_popup.data('variation_id', variation_id);
    $add_sample_popup.data('swatch_attr', swatch_attr);
    $add_sample_popup.data('swatch_val', swatch_val);
    
    $add_sample_popup_title.text($swatch_anchor.attr('title'));
    if (typeof desc !== 'undefined') {
        $add_sample_popup_desc.text(desc.replace(/[\r\n]+/g, '<br>'));
    }

    if (more_large_src) {
      $add_sample_popup_image.html(
        '<img src="' + more_large_src + '" alt="">'
      );
    } else {
      $add_sample_popup_image.empty();
    }

    $add_sample_popup.show();
  });

  ////////////////////////////////////////////////////////////////////////////

  var $bc_width = $('.bc-width-field');
  var $bc_height = $('.bc-height-field');
  var $bc_width_new = $('.rwidth');
  var $bc_height_new = $('.rheight');
  var $bc_width_field = $bc_width.find('input');
  var $bc_height_field = $bc_height.find('input');
  var $bc_width_field_new = $bc_width_new.find('input');
  var $bc_height_field_new = $bc_height_new.find('input');

  var not_digits_regexp = /[^\d]/g;

  var bc_width = $bc_width_field.val();
  var bc_height = $bc_height_field.val();

  if (bc_width) {
    bc_width = bc_width.replace(not_digits_regexp, '');
  } else {
    bc_width = '';
  }

  if (bc_height) {
    bc_height = bc_height.replace(not_digits_regexp, '');
  } else {
    bc_height = '';
  }

  $bc_width_field.val(bc_width);
  $bc_height_field.val(bc_height);

  $bc_width.add($bc_height).find('input').keyup(function(e) {
    if (
      // Backspace, delete, tab, esacpe, enter and '.'
      ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1) || (e.keyCode === 65 && e.ctrlKey === true) // Ctrl + A
      || (e.keyCode === 67 && e.ctrlKey === true) // Ctrl + X
      || (e.keyCode === 88 && e.ctrlKey === true) // Ctrl + X
      || (e.keyCode >= 35 && e.keyCode <= 39) // Home, End, Left, Right
    ) {
      return;
    }

    if (
      (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)
    ) {
      e.preventDefault();
    }
  });

  $bc_width_field_new.bind('change input', function() {
    var $this = $(this);
    var val = $this.val().replace(not_digits_regexp, '');

    $this.val(val);

    bc_width = parseInt(val, 10);
  });

  $bc_height_field_new.bind('change input', function() {
    var $this = $(this);
    var val = $this.val().replace(not_digits_regexp, '');

    $this.val(val);

    bc_height = parseInt(val, 10);
  });

  ////////////////////////////////////////////////////////////////////////////

  function checkFormFields() {
    //devlog('checkFormFields');

    $('.gfield input').each(function() {
      var $input = $(this);

      if ($input.attr('type') !== 'hidden') {
        if ($input.val() !== '') {
          $input.closest('li')
            .find('.gfield-status-icon')
            .addClass('status_ok')
            .end()
            .nextAll(NEXT_LI_SELECTOR).first()
            .removeClass('gfield-hidden');
          updateProductTotalsPositions();
        } else {
          $input.closest('li')
            .find('.gfield-status-icon')
            .removeClass('status_ok');
        }
      }
    });

    $('.gfield select').each(function() {
      var $select = $(this);
      var val = separatedValue($select.val());

      if ((val !== '') && (val !== '0')) {
        $select.closest('li')
          .find('.gfield-status-icon')
          .addClass('status_ok')
          .end()
          .nextAll(NEXT_LI_SELECTOR).first()
          .removeClass('gfield-hidden');
        updateProductTotalsPositions();
      } else {
        $select.closest('li')
          .find('.gfield-status-icon')
          .removeClass('status_ok');
      }
    });

    $product_form.find('.gf_inner_form').each(function(index) {
      checkStepFields($(this),index);
    });
  }

  if (typeof edit_cart_config !== 'undefined') {
    $.each(edit_cart_config.values, function(key, value) {
      $('#input_' + edit_cart_config.form_id + '_' + key).val(value);
    });

    bc_width = $bc_width_field.val();
    bc_height = $bc_height_field.val();

    setTimeout(function() {
      $.each(edit_cart_config.variation_data, function(key, value) {
        if (key.startsWith('attribute_')) {
          key = key.substr(10);
        }

        var $div = $product_form.find(
          'div[data-attribute="' + key + '"]' + '[data-value="' + value + '"]'
        );

        if ($div.length) {
          $div.find('a').click();
        }

        var $filters = $('.swatch-filters' + '[data-filter_name="' + key + '"]');

        if ($filters.length) {
          $filters.find('div[data-value="' + value + '"]')
            .find('a').click();
        }
      });
    }, 200);
  }

  init_edit_form = function() {
    console.log('init_edit_form ( ' + arguments.join(', ') + ' )');
  }

  ////////////////////////////////////////////////////////////////////////////

  var $gfield_price = $('.gfield_price .ginput_amount');

  var $bc_mp_field = $('.bc-mp-field');
  var $bc_tf_field = $('.bc-tf-field');

  var $pa_filter = $('#pa_filter');
  var $pa_fabric = $('#pa_fabric');
  var $pa_material = $('#pa_material');
  var $pa_size = $('#pa_size');
  var $pa_hb_colour = $('#pa_honeycomb-colour');

  var form_updating = false;

  function updateLogic() {
    //devlog('updateLogic');

    if ($bc_mp_field.find('select').val() === 'Inside') {
      $bc_tf_field.find('select').val('0');
      $bc_tf_field.find('.selected').removeClass('selected');
    }

    var set_width = parseInt(bc_width, 10);
    var set_height = parseInt(bc_height, 10);

    if (isNaN(set_width)) {
      set_width = 0;
    }

    if (isNaN(set_height)) {
      set_height = 0;
    }

    var bc_mp_value = $bc_mp_field.find('select').val();
    var bc_tf_value = $bc_tf_field.find('select').val();

    //devlog('bc_mp_value', bc_mp_value);
    //devlog('bc_tf_value', bc_tf_value);

    if (product_id === 131 || product_id === 1818 || product_id === 3382 || product_id === 20785 ) { // Roller Blinds
      if (bc_mp_value === 'Inside') {
        set_width -= 4;
      }
    } else if (product_id === 135) { // Vertical Blinds
      if (bc_mp_value === 'Inside') {
        $bc_tf_field.closest('li').addClass('gf-off-screen');

        set_width -= 5;
        set_height -= 10;
      } else if (bc_mp_value === 'Outside') {
        $bc_tf_field.closest('li').removeClass('gf-off-screen');

        if (bc_tf_value === 'Yes') {
          set_height -= 15;
        }
      }
    } else if (product_id === 256) { // Roman Blinds
      //if (bc_mp_value === 'Outside') { // As I know, it is no MP field
      if (bc_tf_value === 'Yes') {
        set_height -= 35;
      }
      //}
    } else if (product_id === 254 || product_id === 1877 || product_id === 21280) { // Venetian Blinds
      if (bc_mp_value === 'Inside') {
        $bc_tf_field.closest('li').addClass('gf-off-screen');

        set_width -= 8;
        set_height -= 5;
      } else if (bc_mp_value === 'Outside') {
        $bc_tf_field.closest('li').removeClass('gf-off-screen');

        if (bc_tf_value === 'Yes') {
          set_height -= 10;
        }
      }
    } else if (product_id === 260) { // Panel Blinds
      if (bc_mp_value === 'Inside') {
        $bc_tf_field.closest('li').addClass('gf-off-screen');

        set_width -= 5;
        set_height -= 15;
      } else if (bc_mp_value === 'Outside') {
        $bc_tf_field.closest('li').removeClass('gf-off-screen');

        if (bc_tf_value === 'Yes') {
          set_height -= 20;
        }
      }
    } else if (product_id === 262) { // Honeycomb Blinds
      if (bc_mp_value === 'Inside') {
        $bc_tf_field.closest('li').addClass('gf-off-screen');

        set_width -= 5;
        set_height -= 5;
      } else if (bc_mp_value === 'Outside') {
        $bc_tf_field.closest('li').removeClass('gf-off-screen');

        if (bc_tf_value === 'Yes') {
          set_height -= 10;
        }
      }
    } else if (product_id === 264) { // Twin Blinds
      if (bc_mp_value === 'Inside') {
        $bc_tf_field.closest('li').addClass('gf-off-screen');

        set_width -= 5;
      } else if (bc_mp_value === 'Outside') {
        $bc_tf_field.closest('li').removeClass('gf-off-screen');

        if (bc_tf_value === 'Yes') {
          set_height -= 10;
        }
      }
    }

    updateProductTotalsPositions();

    if (set_width <= 0) {
      set_width = '';
    }

    if (set_height <= 0) {
      set_height = '';
    }

    var bc_width_val = $bc_width_field.val(); 
    var bc_height_val = $bc_height_field.val();
    var w_filter1 = $('#db-blind-form input[name="dbl_filter_1_width"]');

    //devlog('set_width',  set_width);
    //devlog('set_height',  set_height);
    //devlog('bc_width_val',  bc_width_val);
    //devlog('bc_height_val', bc_height_val);

    if (bc_width_val != set_width) {
      //devlog('change bc_width to ' + set_width);

      $bc_width_field.val(set_width).trigger('keyup');
      $bc_width_field_new.val(set_width).trigger('keyup');
      if(w_filter1.length != 0) w_filter1.val(set_width);
    }

    if (bc_height_val != set_height) {
      //devlog('change set_height to ' + set_height);

      $bc_height_field.val(set_height).trigger('keyup');
      $bc_height_field_new.val(set_height).trigger('keyup');
    }
  }

  function updateData() {
    //devlog('updateData');

    var selected_filter = $pa_filter.val();
    var selected_fabric = $pa_fabric.val();
    var selected_material = $pa_material.val();
    var selected_size = $pa_size.val();
    var selected_hb_colour = $pa_hb_colour.val();

    // HONEYCOMB BLINDS
    // Setting the value on the hiddin fabric group field.
    if (selected_hb_colour != null) {
      if (selected_hb_colour.indexOf('rioja') != -1) {
        selected_hb_colour = 'Rioja';
      } else {
        selected_hb_colour = 'Palma';
      }
    }

    if (
      (selected_filter === 'blockout') && (selected_hb_colour === 'Rioja')
    ) {
      $('.bc-fg-field-hb').find('select').val('blackout_montreal_rioja');
    }

    if (
      (selected_filter === 'light-filter') && (selected_hb_colour === 'Rioja')
    ) {
      $('.bc-fg-field-hb').find('select').val('light_montreal_rioja');
    }

    // PANEL BLINDS
    // Setting the value on the hiddin fabric group field.
    if (selected_fabric === 'riviera') {
      $('.bc-fg-field-pb').find('select').val('group_4');
    }

    if (selected_fabric === 'balmoral') {
      $('.bc-fg-field-pb').find('select').val('group_5');
    }

    if (selected_fabric === 'barbados') {
      $('.bc-fg-field-pb').find('select').val('group_2');
    }

    if (selected_fabric === 'prestige') {
      $('.bc-fg-field-pb').find('select').val('group_1');
    }

    if ( (selected_filter === 'blockout') && (selected_fabric === 'jersey') ){
      $('.bc-fg-field-pb').find('select').val('group_4');
    }

    if ( (selected_filter === 'light-filter') && (selected_fabric === 'jersey') ){
      $('.bc-fg-field-pb').find('select').val('group_4');
    }

    if ( selected_fabric === 'focus' ){
      $('.bc-fg-field-pb').find('select').val('group_1');
    }

    if ( selected_fabric === 'tusk' ){
      $('.bc-fg-field-pb').find('select').val('group_4');
    }

    // ROLLER BLINDS OLD REMOVE ON LIVE
    // Setting the value on the hiddin fabric group field.
    if (selected_fabric === 'prestige' || selected_fabric === 'focus') {
      $('.bc-fg-field-rlb').find('select').val('group_1');
    }

    if (selected_fabric === 'one' || selected_fabric === 'sanctuary') {
      $('.bc-fg-field-rlb').find('select').val('group_3');
    }

    if (
      (selected_fabric === 'rome') || (selected_fabric === 'milan') || (selected_fabric === 'florence') || (selected_fabric === 'maui') || (selected_fabric === 'london') || (selected_fabric === 'amalfi') || (selected_fabric === 'como') || (selected_fabric === 'venice') || (selected_fabric === 'empire')
    ) {
      $('.bc-fg-field-rlb').find('select').val('group_3');
    }

    if (selected_fabric === 'barbados' || selected_fabric === 'riviera') {
      $('.bc-fg-field-rlb').find('select').val('group_4');
    }

    if (selected_fabric === 'tusk') {
      $('.bc-fg-field-rlb').find('select').val('group_4');
    }

    if (selected_fabric === 'dakota') {
      $('.bc-fg-field-rlb').find('select').val('group_4');
    }

    if (selected_fabric === 'balmoral') {
      $('.bc-fg-field-rlb').find('select').val('group_6');
    }

    if (selected_fabric === 'serengetti') {
      $('.bc-fg-field-rlb').find('select').val('group_8');
    }

    if (selected_fabric === 'positano') {
      $('.bc-fg-field-rlb').find('select').val('group_9');
    }

    if (selected_fabric === 'blinds-city-express' && selected_filter === 'blockout') {
      $('.bc-fg-field-rlb').find('select').val('group_1');
    }

    if (selected_fabric === 'blinds-city-express' && selected_filter === 'sunscreen') {
      $('.bc-fg-field-rlb').find('select').val('group_3');
    }

    if (selected_fabric === 'budget' && selected_filter === 'blockout') {
      $('.bc-fg-field-rlb').find('select').val('group_1b');
    }

    if (selected_fabric === 'budget' && selected_filter === 'sunscreen') {
      $('.bc-fg-field-rlb').find('select').val('group_3b');
    }

    if (selected_fabric === 'jersey' && selected_filter === 'blockout') {
      $('.bc-fg-field-rlb').find('select').val('group_5');
    }

    if (selected_fabric === 'jersey' && selected_filter === 'light-filter') {
      $('.bc-fg-field-rlb').find('select').val('group_4');
    }

    // NEW ROLLER BLINDS
    // Setting the value on the hiddin fabric group field.

    // if (selected_fabric === 'focus' || selected_fabric === 'solitaire') {
    //   $('.bc-fg-field-rlb').find('select').val('group_1');
    // }

    // if (selected_fabric === 'one') {
    //   $('.bc-fg-field-rlb').find('select').val('group_2');
    // }

    // if (selected_fabric === 'sanctuary' || selected_fabric === 'barbados' || selected_fabric === 'empire') {
    //   $('.bc-fg-field-rlb').find('select').val('group_3');
    // }

    // if (selected_fabric === 'tusk' || selected_fabric === 'metroshade') {
    //   $('.bc-fg-field-rlb').find('select').val('group_4');
    // }

    // if (selected_fabric === 'jersey' || selected_fabric === 'riviera') {
    //   $('.bc-fg-field-rlb').find('select').val('group_5');
    // }

    // if (selected_fabric === 'balmoral') {
    //   $('.bc-fg-field-rlb').find('select').val('group_6');
    // }

    // if (selected_fabric === 'budget' && selected_filter === 'blockout') {
    //   $('.bc-fg-field-rlb').find('select').val('group_1b');
    // }

    // if (selected_fabric === 'budget' && selected_filter === 'sunscreen') {
    //   $('.bc-fg-field-rlb').find('select').val('group_3b');
    // }

    // ROMAN BLINDS
    // Setting the value on the hiddin fabric group field.
    if (selected_fabric === 'sanctuary') {
      $('.bc-fg-field-rmb').find('select').val('group_1');
    }

    if (selected_fabric === 'barbados') {
      $('.bc-fg-field-rmb').find('select').val('group_1');
    }

    if (selected_fabric === 'riviera') {
      $('.bc-fg-field-rmb').find('select').val('group_3');
    }

    if (selected_fabric === 'jersey') {
      $('.bc-fg-field-rmb').find('select').val('group_4');
    }

    if (selected_fabric === 'balmoral') {
      $('.bc-fg-field-rmb').find('select').val('group_5');
    }

    // TWIN BLINDS
    // Setting the value on the hiddin fabric group field.
    if (selected_filter === 'blockout') {
      $('.bc-fg-field-tb').find('select').val('dim_out');
    } else {
      $('.bc-fg-field-tb').find('select').val('light_filter');
    }

    // VENETIAN BLINDS
    // Setting the value on the hiddin fabric group field. */
    if (selected_material === 'aluminiun') {
      $('.bc-fg-field-vnb').find('select').val('25_alu');
    }

    if (selected_material === 'pvc' && selected_size === '50mm') {
      $('.bc-fg-field-vnb').find('select').val('50_pvc');
    }

    if (selected_material === 'pvc' && selected_size === '63mm') {
      $('.bc-fg-field-vnb').find('select').val('63_pvc');
    }

    if (selected_material === 'pvc' && selected_size === 'visionwood-50mm') {
      $('.bc-fg-field-vnb').find('select').val('50_pvc');

    }

    if (selected_material === 'pvc' && selected_size === 'visionwood-63mm') {
      $('.bc-fg-field-vnb').find('select').val('63_pvc');

    }

    if (selected_material === 'pvc' && selected_size === 'budget-50mm') {
      $('.bc-fg-field-vnb').find('select').val('50_budget');

    }

    if (selected_material === 'pvc' && selected_size === 'budget-63mm') {
      $('.bc-fg-field-vnb').find('select').val('63_budget');

    }

    if ( (selected_material === 'timber' && selected_size === '50mm') || (selected_material === 'timber' && selected_size === 'purewood-pastel-50mm') || (selected_material === 'timber' && selected_size === 'purewood-shadow-50mm') || (selected_material === 'timber' && selected_size === 'purewood-urban-50mm') ) {
      $('.bc-fg-field-vnb').find('select').val('50_tim');
    }

    if (selected_material === 'timber' && selected_size === 'purewood-gloss-50mm') {
      $('.bc-fg-field-vnb').find('select').val('50_tim_pwg');
    }

    if (selected_material === 'timber' && selected_size === '63mm') {
      $('.bc-fg-field-vnb').find('select').val('63_tim');
    }

    // VERTICAL BLINDS
    // Setting the value on the hiddin fabric group field.
    if (selected_fabric === 'balmoral') {
      $('.bc-fg-field-vrb').find('select').val('group_c');
    }

    if (selected_fabric === 'prestige') {
      $('.bc-fg-field-vrb').find('select').val('group_a');
    }

    if (selected_fabric === 'focus') {
      $('.bc-fg-field-vrb').find('select').val('group_a');
    }

    // AWNING BLINDS
    // Setting the value on the hiddin fabric group field.
    if (selected_fabric === 'solar-view') {
      $('.bc-fg-field-aab').find('select').val('group_1');
      $('.bc-fg-field-sdab').find('select').val('group_1');
    }

    //Plantation Shutters
    if (selected_material === 'pvc') {
      $('.bc-fg-field-pnb').find('select').val('pvc');
    }
    if (selected_material === 'timber') {
      $('.bc-fg-field-pnb').find('select').val('timber');
    }

    // Custom Made Curtains - New product march 2018
    if ( selected_filter === 'blockout' && selected_fabric === 'luther' ){
      $('.bc-fg-field-cur').find('select').val('group_2');
    }
    if ( selected_filter === 'sheers' && selected_fabric === 'flo' ){
      $('.bc-fg-field-cur').find('select').val('group_1');
    }
    if ( selected_filter === 'blockout' && (selected_fabric === 'fitzgerald' || selected_fabric === 'harper' || selected_fabric === 'wilde') ){
      $('.bc-fg-field-cur').find('select').val('group_3');
    }
    if ( selected_filter === 'sheers' && selected_fabric === 'chios' ){
      $('.bc-fg-field-cur').find('select').val('group_4');
    }

    // Motorised Roller Shutters - New product may 2018

    if (selected_material === 'aluminiun') {
      $('.bc-fg-field-mrs').find('select').val('group_1');
    }

    // Custom Printed Roller Blinds - New product june 2018

    if ( selected_filter === 'light-filter' || selected_filter  === 'sunscreen') {
      $('.bc-fg-field-cprlb').find('select').val('group_1');
    }
  }

  var delayed_form_check_timer;

  function updateForm() {
    //checkFormFields();

    if (form_updating) {
      return;
    }

    form_updating = true;

    updateData();

    form_updating = false;

    checkFormFields();
  }

  var $width_needed = $('#width_needed');
  var $length_needed = $('#length_needed');

  $('.bc-width-field-pnb')
    .find('input').bind('change input', function() {
      $width_needed.val($(this).val()).trigger('keyup');
    });

  $('.bc-height-field-pnb')
    .find('input').bind('change input', function() {
      $length_needed.val($(this).val()).trigger('keyup');
    });

  ////////////////////////////////////////////////////////////////////////////

  function checkStepFields($step,index) {
    var fields_are_good = true;
    var $li_elems = $step.find('li:not(.gf-off-screen)');

    $li_elems.filter(':not(.field-group-2)')
      .removeClass('show_top_border')
      .filter(':not(:first)')
      .addClass('show_top_border');

    $li_elems.find('input').each(function() {
      var $elem = $(this);
      var val = $elem.val();

      if (val === '') {
        fields_are_good = false;

        return false;
      }
    });

    if (fields_are_good) {
      $li_elems.find('select').each(function() {
        var $elem = $(this);
        var val = separatedValue($elem.val());

        if ((val === '') || (val === '0')) {
          fields_are_good = false;

          return false;
        }
      });
    }

    if (fields_are_good) {
      if (index == 1){
        if ($step.find('.add-filter').length){
           $('#offers-for-woocommerce-add-to-cart-wrap').hide();
        } else {
          $('#offers-for-woocommerce-add-to-cart-wrap').show();
        }
      }
      $step.addClass('ready_step');
    } else {
      $step.removeClass('ready_step');
    }

    if ($step.find('.button-add_to_cart').length) {
      if (fields_are_good) {
        $add_to_cart_button.removeAttr('disabled');
         $('#product-add-to-cart .add-btn a').removeAttr('disabled');
        $('#product-add-to-cart .add-btn a').addClass('btn_ready');
        $('#customize-order .check').show();
      } else {
        $add_to_cart_button.attr('disabled', 'disabled');
        $('#product-add-to-cart .add-btn a').attr('disabled','disabled');
      }
    }

    updateProductTotalsPositions();
  }

  $('.field-group-1, .field-group-2, .field-group-3, .field-group-4')
    .find('input[type="text"]')
    .bind('change input keyup', function(event) {
      updateForm();
    })
    .end()
    .find('select')
    .bind('change', function(event) {
      updateForm();
    });

  $('.field-group-3').find('select').change(function() {
    updateLogic();
  });

  ////////////////////////////////////////////////////////////////////////////

  var $section_buttons = $('.gsection-buttons');
  var $section_header = $('.gfield.gsection');

  function switchStep(id) {

    var $target = $('.gsection.field-group-' + id);

    // devlog('switchStep(' + id + ')');

    if ($target.length) {
      $('.gf_will_expand.gf_expanded').slideUp({
        'duration': 400,
        'progress': function() {
          updateProductTotalsPositions();
        },
        'complete': function() {
          $(this).removeClass('gf_expanded');
          $(this).prev().removeClass('active');
          $target.next('.gf_will_expand').slideDown({
            'duration': 400,
            'progress': function() {
              updateProductTotalsPositions();
            },
            'complete': function() {
              $(this).addClass('gf_expanded');
              $(this).prev().addClass('active');
            }
          });
        }
      });

      setTimeout(function() {
        $.scrollTo($target, 400);
      }, 400);
    }
  }

  $section_buttons.find('.button-back').click(function(event) {
    var $btn = $(this);
    var $gs_btns = $btn.closest('.gsection-buttons');

    if ($gs_btns.is('.gsection-buttons-2')) {
      switchStep(1);
    } else if ($gs_btns.is('.gsection-buttons-3')) {
      switchStep(2);
    }

    return false;
  });

  $section_buttons.find('.button-next').click(function(event) {
    var $btn = $(this);
    var $gs_btns = $btn.closest('.gsection-buttons');

    if ($gs_btns.is('.gsection-buttons-1')) {
      switchStep(2);
    } else if ($gs_btns.is('.gsection-buttons-2')) {
      switchStep(3);
    }

    return false;
  });

  // $section_buttons.find('.button-add_to_cart').click(function(event) {
  //   $product_form.submit();

  //   return false;
  // });

  $('#product-add-to-cart').find('.button-add_to_cart').click(function(event) {
    $product_form.submit();

    return false;
  });

  // $section_header.click(function(event) {
  //   var $btn = $(this);

  //  if ( $btn.is('.field-group-1') && $btn.not('.grp-size') ) {
  //     switchStep(1);
  //     switchStep(3);
  //   }


  //   return false;
  // });

  ////////////////////////////////////////////////////////////////////////////

  var $group_3_elems = $('.field-group-3');

  $group_3_elems.on('click', '.simple_select a', function(event) {
    var $a = $(this);
    var $select = $a.closest('.simple_select');

    $select.find('.selected').removeClass('selected');
    $a.addClass('selected');
    $select.prev('.simple_select_inv')
      .find('select')
      .val($a.data('val'))
      .trigger('change');

    return false;
  });

  $group_3_elems.filter('.gf_simple_select')
    .find('select')
    .each(function() {
      var $select = $(this).wrap('<div class="simple_select_inv">');
      var $select_wrap = $select.parent();
      var $simple_select = $('<div class="simple_select">');

      $simple_select.insertAfter($select_wrap);

      var val = $select.val();

      $select.find('option').each(function() {
        var $option = $(this);
        var option_text = $option.text();
        var option_val = $option.val();
        var option_sval = separatedValue(val);

        if ((option_sval === '') || (option_sval === '0')) {
          return;
        }

        var $a = $('<a>');
        $a.attr('href', '#');
        $a.attr('data-val', option_val);
        $a.text(option_text);

        if (val === option_val) {
          $a.addClass('selected');
        }

        $simple_select.append($a);
      });

      $simple_select.append('<div class="clear">');
    });

  $group_3_elems.on('click', '.radio_image a', function(event) {
    var $a = $(this);
    var $select = $a.closest('.radio_image');

    $select.find('.selected').removeClass('selected');
    $a.addClass('selected');
    $select.prev('.radio_image_inv')
      .find('select')
      .val($a.data('val'))
      .trigger('change');

    return false;
  });

  $group_3_elems.filter('.gf_radio_image').find('select')
    .each(function() {
      var $select = $(this).wrap('<div class="radio_image_inv">');
      var $select_wrap = $select.parent();
      var $radio_image = $('<div class="radio_image">');

      var fgn = $(this).parent().parent().prev().text(); // Get field group name
      fg = fgn.replace('Select ', '');

      $radio_image.insertAfter($select_wrap);

      var noicon = false; // no icon/image option
      if ($(this).parent().parent().parent().hasClass('no-icon')) {
        noicon = true;
      }

      var hasimage = false; // use gf image instead of icons
      if ($(this).parent().parent().parent().hasClass('has-image')) {
        hasimage = true;
      }

      if ( ($(this).parent().parent().parent().find('.gfield_label').text().indexOf('Colour') > 0) || ($(this).parent().parent().parent().find('.gfield_label').text().indexOf('Color') > 0) || ($(this).parent().parent().parent().find('.gfield_label').text().indexOf('colour') > 0) || ($(this).parent().parent().parent().find('.gfield_label').text().indexOf('color') > 0) ) {
        $(this).parent().parent().parent().addClass('has-color');
      }

      var val = $select.val();

      $select.find('option').each(function() {
        var $option = $(this);
        var option_text = $option.text();
        var option_val = $option.val();
        var option_sval = separatedValue(option_val);

        if ((option_sval === '0') || (option_sval === '')) {
          return;
        }

        var $a = $('<a>');
        $a.attr('href', '#');
        $a.attr('data-val', option_val);

        var $icon_class = fg + ' ' + option_val;
        $icon_class = $icon_class.toLowerCase(); // Combine fg name and option name and convent to pretty url

        if (val === option_val) {
          $a.addClass('selected');
        }

        if (noicon == false) {

          var $option_image = $('<span class="option_image">');
          $a.append($option_image);

          var icon_url = $option.data('icon_url');
          var img_url = '';

          if (icon_url && (icon_url !== '')) {
            img_url = icon_url;
          } else {
            var bg_css = $option.css('background-image');

            if (bg_css && (bg_css !== 'none')) {
              img_url = bg_css.slice(4, -1).replace(/['"]/g, '');
            }
          }

          if (img_url && (img_url !== '')) {
            // if (hasimage == true){
              var $img = $('<img>');
              $img.attr('src', img_url);
              $img.attr('alt', option_text);
            // } else {
            //   var $img = $('<i>');
            //   $img.addClass('bc-icon '+ $icon_class );
            // }
           
            $option_image.append($img);
          }

        }

        var $option_label = $('<span class="option_label">');
        var $option_price = $('<span class="option_price">');
        $option_label.text(option_text);
        $a.append($option_label);

        var option_val2 = '';
        var option_fval = '';
        if ( option_val.indexOf('|') > 0 ) {
          option_val2 = option_val.split('|');
          option_fval = option_val2[1];
        
          if ( option_fval != '0' || option_fval != ''  ) { 
             option_fval = option_fval.replace(/[^0-9\.]/g, '');

            if ($.isNumeric(option_fval) && option_fval != 0){
              $option_price.text('+ $' + option_fval);
              $a.append($option_price);
            }
            if (option_text == "Chain"){
               $option_price.text('Included');
              $a.append($option_price);
            }
          }
        }

        var $option_radio = $('<span class="option_radio">');
        $a.append($option_radio);

        $radio_image.append($a);
      });

      $radio_image.append('<div class="clear">');
    });

  $group_3_elems.filter(':not(.gf_simple_select):not(.gf_radio_image)')
    .find('option')
    .each(function() {
      var $option = $(this);
      var icon_url = $option.data('icon_url');
      var img_url = '';

      if (icon_url && (icon_url !== '')) {
        img_url = icon_url;
      } else {
        var bg_css = $option.css('background-image');

        if (bg_css && (bg_css !== 'none')) {
          img_url = bg_css.slice(4, -1).replace(/['"]/g, '');
        }
      }

      if (img_url && (img_url !== '')) {
        var $img = $('<img>');
        $img.attr('src', img_url);
        $img.attr('alt', option_text);

        $option_image.append($img);
      }
    });

  ////////////////////////////////////////////////////////////////////////////

  // Quantity:
  $('.quantity .q_plus').click(function() {
    var $qf = $(this).parents('.quantity').find('input.qty');

    $qf.val(parseInt($qf.val()) + parseInt($qf.attr('step')));
    $qf.parents('form').find('[name="update_cart"]').click();

    return false;
  });

  $('.quantity .q_minus').click(function() {
    var $qf = $(this).parents('.quantity').find('input.qty');
    var old_val = $qf.val();

    if (parseInt($qf.val()) - parseInt($qf.attr('step')) > 0) {
      $qf.val(parseInt($qf.val()) - parseInt($qf.attr('step')));
    } else {
      $qf.val(1);
    }

    if ($qf.val() !== old_val) {
      $qf.parents('form').find('[name="update_cart"]').click();
    }

    return false;
  });

  $('.edit .edit_product').click(function() {
    $('#cart-content-loader').addClass('active');

    var $this = $(this);

    $this.parents('form')
      .find('[name="edit_cart_item"]').val($this.attr('rel'));

    $this.parents('form').submit();

    return false;
  });

  $('.edit_cart_product_selector').change(function() {
    var $this = $(this);
    var $extended_cart_form = $('#extended_cart_form');

    $extended_cart_form
      .find('[name="edit_cart_item"]')
      .val($product_form.find('[name="edit_cart_item"]').val());

    $extended_cart_form
      .find('[name="edit_cart_new_product"]')
      .val($this.val());

    $extended_cart_form.submit();
  });

  // Cart:
  $('body').on('click', '#update_cart_custom', function() {
    $('#cart-content-loader').addClass('active');
    $('#cart-custom-product-container')
      .parent()
      .find('form')
      .find('[name="update_cart"]').click();

    return false;
  });

  if (DEBUG_EXPAND_STEPS) {
    if ($gform_variation.length) {
      setTimeout(function() {
        $('.gf_will_expand').addClass('gf_expanded');

        updateProductTotalsPositions();
      }, 500);
    }
  }
  //Plantation Online Midrail Enter measurement
  jQuery('.Plantation-m .radio_image').prepend('<p style="margin-bottom: 5px;">Note: over 1400mm high must have midrail</p>');
  jQuery('.Plantation-m .radio_image_inv select').change(function(){
    if (jQuery(this).val() == 'Enter Measurement'){
      jQuery('.Plantation-m .radio_image').append('<input type="text" class="m-enter-m" placeholder="e.g 1200mm" />');
      jQuery(document).on('keypress', 'input.m-enter-m', function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
          return false;
        }
      });
      jQuery(document).on('blur', 'input.m-enter-m', function(e){
        jQuery('.Plantation-m .radio_image_inv select option:selected').addClass('enter-m');
        jQuery('.Plantation-m .radio_image_inv select option:selected').val(jQuery(this).val() + 'mm');
      });
    } else {
      if (jQuery('input.m-enter-m').length != 0) {
        jQuery('.Plantation-m .radio_image_inv select option.enter-m').val('Enter Measurement');
        jQuery('.Plantation-m .radio_image_inv select option.enter-m').removeClass('enter-m');
        jQuery('input.m-enter-m').remove();
      }
    }
  });
  // Plantation Online Show/Hide None Option
  jQuery(document).on('blur', '.bc-height-field-pnb input', function(){
    if(jQuery(this).val() > 1400){
      jQuery('.Plantation-m .radio_image a[data-val="None"]').hide();
    } else {
      jQuery('.Plantation-m .radio_image a[data-val="None"]').show();
    }
  });

  // New Fabric text
  jQuery('#picker_pa_fabric .swatch-filter a:contains("Jersey")').parent().append('<span style="position: absolute;top: -4px;font-size: 12px;background: red;color: #fff;line-height: 1;padding: 2px 5px;font-weight: bold;right: 0;">NEW!</span>');
  jQuery('#picker_pa_fabric .swatch-filter a:contains("Jersey")').parent().css('position','relative');
  var menu = $('#menu-mobile').slideMenu({
      position: 'left'
  });

  // cart border height
  jQuery('.woocommerce-cart .cart_item').each(function(){
    var cart_box_height = jQuery(this).height() - 50;
    jQuery(this).find('.pqty-wrapper').height(cart_box_height);
  });
});
// COUNTDOWN TIMER

// Set the date we're counting down to
var countDownDate = new Date("August 30, 2018 00:00:00").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get todays date and time
  var now = new Date().getTime();

  // Find the distance between now an the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Display the result in the element with id="demo"
  document.getElementById("timer").innerHTML = days + "d " + hours + "h "
  + minutes + "m " + seconds + "s ";

  // If the count down is finished, write some text 
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("timer").innerHTML = "EXPIRED";
  }
}, 1000);


// LOGIN PAGE REGISTER EFFECT
// jQuery(document).on('click','#reg-toggle', (function(e) {
//   jQuery('#login-check').slideUp();
//   jQuery('#customer_reg').slideDown();
//   e.preventDefault();
// }));
// jQuery(document).on('click','#back-toggle', (function(e) {
//   jQuery('#customer_reg').slideUp();
//   jQuery('#login-check').slideDown();
//   e.preventDefault();
// }));
// ;(function($){
//     'use strict';

//     var defaults = {
// topOffset: 400, //px - offset to switch of fixed position
// hideDuration: 300, //ms
// stickyClass: 'is-fixed'
// };

// $.fn.stickyPanel = function(options){
// if(this.length == 0) return this; // returns the current jQuery object

// var self = this,
// settings,
// isFixed = false, //state of panel
// stickyClass,
// animation = {
// normal: self.css('animationDuration'), //show duration
// reverse: '', //hide duration
// getStyle: function (type) {
//   return {
//     animationDirection: type,
//     animationDuration: this[type]
//   };
// }
// };

// // Init carousel
// function init(){
//   settings = $.extend({}, defaults, options);
//   animation.reverse = settings.hideDuration + 'ms';
//   stickyClass = settings.stickyClass;
//   $(window).on('scroll', onScroll).trigger('scroll');
// }

// // On scroll
// function onScroll() {
//   if ( window.pageYOffset > settings.topOffset){
//     if (!isFixed){
//       isFixed = true;
//       self.addClass(stickyClass)
//       .css(animation.getStyle('normal'));
//     }
//   } else {
//     if (isFixed){
//       isFixed = false;

//       self.removeClass(stickyClass)
//       .each(function (index, e) {
//     // restart animation
//     // https://css-tricks.com/restart-css-animation/
//     void e.offsetWidth;
// })
//       .addClass(stickyClass)
//       .css(animation.getStyle('reverse'));

//       setTimeout(function () {
//         self.removeClass(stickyClass);
//       }, settings.hideDuration);
//     }
//   }
// }

// init();

// return this;
// }
// })(jQuery);

// //run
// jQuery(function () {
//     jQuery('#header-notice').stickyPanel();
// }); 