jQuery( function( $ ) {
    var cartwrap = $( '.woocommerce-cart #main' );

    $( document ).on( 'change', 'select.qty', function() {

        var item_hash = $( this ).attr( 'name' ).replace(/cart\[([\w]+)\]\[qty\]/g, "$1");
        var item_quantity = $( this ).val();
        var currentVal = parseFloat(item_quantity);
        var ship_in = $('input[name="ship_insurance"]:checked').val();
        var ext_war = $('input[name="ext_warranty"]:checked').val();

        function qty_cart() {
    
            $.ajax({
                type: 'POST',
                url: cart_qty_ajax.ajax_url,
                data: {
                    action: 'qty_cart',
                    hash: item_hash,
                    quantity: currentVal,
                    ship_insurance : ship_in,
                    ext_warranty : ext_war
                },
                beforeSend: function(data){
                    $('#wrapper-container').css('opacity','0.3');
                    $('body').prepend('<div class="ajax-wrap" style="position: fixed; width: 100%; height: 100%; background: transparent; z-index: 9999; overflow:hidden;"><div style="position: fixed;margin: auto;left: 50%;bottom: 50%;" class="ajax-loader"></div></div>'); 
                },
                success: function(data) {
                    $('#wrapper-container').css('opacity','1');
                    cartwrap.html(data);
                    $('body').find('.ajax-wrap').remove();
                }
            });  

        }
        
        qty_cart();

    });

    function add_fee_ship(val){
        var ext = $('input[name=ext_warranty]:checked').val();
         $.ajax({
            type: 'POST',
            url: cart_qty_ajax.ajax_url,
            data: {
                action: 'add_fee',
                ship_insurance : val,
                ext_warranty : ext
            },
            beforeSend: function(data){
                $('#wrapper-container').css('opacity','0.3');
                 $('body').prepend('<div class="ajax-wrap" style="position: fixed; width: 100%; height: 100%; background: transparent; z-index: 9999; overflow:hidden;"><div style="position: fixed;margin: auto;left: 50%;bottom: 50%;" class="ajax-loader"></div></div>'); 
            },
            success: function(data) {
                $('#wrapper-container').css('opacity','1');
                cartwrap.html(data);
                $('body').find('.ajax-wrap').remove();
            }
        });  
    }

     function add_fee_ext(val){
        var ship = $('input[name=ship_insurance]:checked').val();
         $.ajax({
            type: 'POST',
            url: cart_qty_ajax.ajax_url,
            data: {
                action: 'add_fee',
                ship_insurance : ship,
                ext_warranty : val
            },
            beforeSend: function(data){
                $('#wrapper-container').css('opacity','0.3');
                $('body').prepend('<div class="ajax-wrap" style="position: fixed; width: 100%; height: 100%; background: transparent; z-index: 9999; overflow:hidden;"><div style="position: fixed;margin: auto;left: 50%;bottom: 50%;" class="ajax-loader"></div></div>');
            },
            success: function(data) {
                $('#wrapper-container').css('opacity','1');
                cartwrap.html(data);
                $('body').find('.ajax-wrap').remove();
            }
        });  
    }

    $( document ).on( 'change', 'input[name=ship_insurance]', function() {
        add_fee_ship($(this).val());
    });

    $( document ).on( 'change', 'input[name=ext_warranty]', function() {
        add_fee_ext($(this).val());
    });

});