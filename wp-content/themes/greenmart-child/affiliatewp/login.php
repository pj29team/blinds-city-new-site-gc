<hr>
<?php
global $affwp_login_redirect;
affiliate_wp()->login->print_errors();
?>
<div class="row affiliate-login">
<h3><?php _e( 'Log into your account', 'affiliate-wp' ); ?></h3>
<form id="affwp-login-form" class="affwp-form" action="" method="post">
	<?php
	/**
	 * Fires at the top of the affiliate login form template
	 */
	do_action( 'affwp_affiliate_login_form_top' );
	?>
	<div class="row">
		<?php
		/**
		 * Fires immediately prior to the affiliate login form template fields.
		 */
		do_action( 'affwp_login_fields_before' );
		?>	
		<div class="col-sm-6">
			<div class="form-group">
				<label for="affwp-login-user-login"><?php _e( 'Username', 'affiliate-wp' ); ?></label>
				<input id="affwp-login-user-login" class="required" type="text" name="affwp_user_login" title="<?php esc_attr_e( 'Username', 'affiliate-wp' ); ?>" />
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<label for="affwp-login-user-pass"><?php _e( 'Password', 'affiliate-wp' ); ?></label>
				<input id="affwp-login-user-pass" class="password required" type="password" name="affwp_user_pass" />
				<div class="row">
					<div class="col-xs-6">
						<div class="checkbox">
							<label class="affwp-user-remember" for="affwp-user-remember">
								<input id="affwp-user-remember" type="checkbox" name="affwp_user_remember" value="1" /> <?php _e( 'Remember Me', 'affiliate-wp' ); ?>
							</label>
						</div>
					</div>
					<div class="col-xs-6">
						<div class="checkbox lpwd-wrap">
							<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>" class="lost-pwd"><?php _e( 'Lost your password?', 'affiliate-wp' ); ?></a>
						</div>
					</div>
				</div>
			</div>
			<br />
			<div class="btn-wrap">
				<input type="hidden" name="affwp_redirect" value="<?php echo esc_url( $affwp_login_redirect ); ?>"/>
				<input type="hidden" name="affwp_login_nonce" value="<?php echo wp_create_nonce( 'affwp-login-nonce' ); ?>" />
				<input type="hidden" name="affwp_action" value="user_login" />
				<input type="submit" class="button btn btn-danger" value="<?php esc_attr_e( 'Log In', 'affiliate-wp' ); ?>" />
			</div>
			<?php
			/**
			 * Fires immediately after the affiliate login form template fields.
			 */
			do_action( 'affwp_login_fields_after' );
			?>
		</div>
	</div>
	<?php
	/**
	 * Fires at the bottom of the affiliate login form template (inside the form element).
	 */
	do_action( 'affwp_affiliate_login_form_bottom' );
	?>
</form>
</div>