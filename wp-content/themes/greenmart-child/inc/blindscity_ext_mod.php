<?php

//add_filter('show_admin_bar', '__return_false');

remove_action('woocommerce_single_variation', 'woocommerce_single_variation_add_to_cart_button', 20);

////////////////////////////////////////////////////////////////////////////////

function bs_print_buttons($args, $return = false) {
	$args = wp_parse_args($args, array(
		'buttons' => array(
			'back' => array(
				'label' => 'Back',
				'class' => 'button-back'
			),
			'next' => array(
				'label' => 'Next',
				'class' => 'button-next'
			)
		),
		'modify' => '',
		'class'  => ''
	));

	$class = 'gfield gsection-buttons';

	if ($args['class']) {
		if (is_array($args['class'])) {
			$class .= ' ' . implode(' ', $args['class']);
		} else {
			$class .= ' ' . $args['class'];
		}
	}

	$html = '<li class="' . $class . '">'
	. '<div class="section-buttons">';
	
	$buttons = $args['buttons'];
	
	if ($args['modify']) {
		$buttons = wp_parse_args($args['modify'], $buttons);
	}
	
	foreach ($buttons as $button_id => &$button) {
		if (!$button) {
			continue;
		}
		
		if (isset($button['href'])) {
			$href = $button['href'];
		} else {
			$href = '#';
		}
		
		$class = '';
		
		if (isset($button['class'])) {
			if (is_array($button['class'])) {
				$class .= ($class ? ' ' : '') . implode(' ', $button['class']);
			} else {
				$class .= ($class ? ' ' : '') . $button['class'];
			}
		}
		
		$html .= '<a href="' . esc_attr($href) . '"'
		. ' class="' . esc_attr($class) . '">'
		. esc_html($button['label']) . '</a>';
	}
	
	$html .= '<div class="clear"></div>'
	. '</div>'
	. '</li>';

	if ($return) {
		return $html;
	}

	echo $html;
}

function bs_gform_field_container_fitler(
	$field_container,
	$field,
	$form,
	$css_class,
	$style,
	$field_content
) {
	static $_cache;

	if ($_cache === null) {
		$_cache = array();
	}

	/* ?><pre><?php var_dump($form); ?></pre><?php */

	if (!isset($_cache[$form['id']])) {
		$_cache[$form['id']] = array(
			'opened'    => false,
			'iterator'  => 0,
			'btns_cntr' => 1,
			'inner_fc'  => 0
		);
	}

	$cache      =& $_cache[$form['id']];
	$fields_num = sizeof($form['fields']);

	$cache['iterator']++;

	$field_container = str_replace(
		'{FIELD_CONTENT}',
		'{FIELD_CONTENT}<div class="clear"></div>',
		$field_container
	);

	if ($field->type === 'section') {
		if ($cache['opened']) {
			$cache['btns_cntr']++;
			$prefix = bs_print_buttons(
				array(
					'class' => array(
						'gsection-buttons-' . $cache['btns_cntr']
					)
				),
				true
			);
			$field_container = $prefix . '</ul></li>' . $field_container;
		}
		$field_container .= '<li class="gf_will_expand">'
		. '<ul class="gf_inner_form">';
		$cache['opened'] = true;
		$cache['inner_fc'] = -1;
	}
  
	if ($cache['opened']) {
		if (strpos($css_class, 'gf-off-screen') === false) {
			$cache['inner_fc']++;
			if ($cache['inner_fc'] >= 2) {
				$field_container = str_replace(
					$css_class,
					$css_class . ' gfield-hidden',
					$field_container
				);
			}
		}
		if ($field->id === $form['fields'][$fields_num - 1]->id) {
			$cache['btns_cntr']++;
			$prefix = bs_print_buttons(
				array(
					'class' => array(
						'gsection-buttons-' . $cache['btns_cntr']
					),
					'modify' => array(
						'next'        => '',
						'add_to_cart' => array(
							'label' => 'Add to cart',
							'class' => 'button-add_to_cart'
						)
					)
				),
				true
			);
			$field_container .= $prefix . '</ul>'
			. '</li>';
			$cache['opened'] = false;
		}
	}
	return $field_container;
}

if (!is_admin()) {
	add_filter('gform_field_container', 'bs_gform_field_container_fitler', 10, 6);
}

////////////////////////////////////////////////////////////////////////////////
function bs_gform_field_content_filter(
	$field_content,
	$field,
	$value,
	$lead_id,
	$form_id
) {
	if ($field->type === 'section') {
		if (preg_match(
			'/^(Step (\d+?)) (.+?)$/ui',
			$field->label,
			$matches
		)) {
			$step_str = $matches[1];
			$text_str = $matches[3];
			
			$field_content = preg_replace(
				'/' . preg_quote($step_str) . '/ui',
				'<span class="step-label">' . $step_str . '</span>',
				$field_content
			);
			$field_content = preg_replace(
				'/' . preg_quote($text_str) . '/ui',
				'<span class="text-label">' . $text_str . '</span>',
				$field_content
			);
		}
	} else {
		$field_content .= '<div class="gfield-status-icon"></div>';
	}
	return $field_content;
}

if (!is_admin()) {
	add_action('gform_field_content', 'bs_gform_field_content_filter', 10, 5);
}

////////////////////////////////////////////////////////////////////////////////

function bs_reset_variations_link($reset_variations_link) {
	return '';
}

add_filter(
	'woocommerce_reset_variations_link',
	'bs_reset_variations_link'
);

////////////////////////////////////////////////////////////////////////////////

function bs_ajax_variation_threshold_filter($limit, $product) {
	return $limit + 1000;
}

add_filter(
	'woocommerce_ajax_variation_threshold',
	'bs_ajax_variation_threshold_filter',
	10,
	2
);

////////////////////////////////////////////////////////////////////////////////

function bs_product_object_filter($the_product) {
	//db_write(print_r($the_product, true), true, 'PRODUCT');
	
	return $the_product;
}

add_filter(
	'woocommerce_product_object',
	'bs_product_object_filter'
);

////////////////////////////////////////////////////////////////////////////////

//function bs_calculate_totals($cart) {
//	//db_write(print_r(array(
//	//	'$cart' => $cart
//	//), true), true, 'CALCULATE_TOTALS');
//	
//	$total_quantities = 0;
//	
//	foreach ($cart->cart_contents as $item_key => &$item) {
//		if (isset($item['is_ready_made']) && $item['is_ready_made']) {
//			if (isset($item['quantities'])) {
//				$total_quantities += array_sum($item['quantities']);
//			}
//		}
//	}
//	
//	unset($item);
//	
//	if ($total_quantities > 0) {
//		$shipping_total = 15;
//		
//		if ($total_quantities > 1) {
//			$shipping_total += ($total_quantities - 1) * 5;
//		}
//		
//		$cart->shipping_total = $shipping_total;
//	}
//}
//
//add_action('woocommerce_calculate_totals', 'bs_calculate_totals');
