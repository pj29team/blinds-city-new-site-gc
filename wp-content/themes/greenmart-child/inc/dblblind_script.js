	jQuery(document).ready(function($) {
	var addBtn = jQuery('#db-blind-form a.button-add_to_cart');
	var blockOut = jQuery('#db-blind-form #picker_pa_filter .select-option.swatch-wrapper[data-value="blockout"]');
	var lightFilter = jQuery('#db-blind-form #picker_pa_filter .select-option.swatch-wrapper[data-value="light-filter"]');
	var sunscreen = jQuery('#db-blind-form #picker_pa_filter .select-option.swatch-wrapper[data-value="sunscreen"]');
	// var nextBtn2 = jQuery('#db-blind-form .gsection-buttons-2 a.button-next');
	var dblStep1 = jQuery('#db-blind-form .gsection.field-group-1');
	var dblStep2 = jQuery('#db-blind-form .gsection.field-group-2');
	var dblStep3 = jQuery('#db-blind-form .gsection.field-group-3');
	var dblsize = jQuery('#db-blind-form .gf_will_expand.grp-size');
	var cartForm = jQuery('#db-blind-form');
	jQuery('#db-blind-form .gfield.field-group-1 .text-label').text('Colour & Fabric: Sunscreen');
	jQuery('#db-blind-form .gfield.field-group-1s .text-label').text('Size: Sunscreen');
	var f1_oldprice = 0;
	blockOut.hide();
	lightFilter.hide();	
	// nextBtn2.hide();
	// nextBtn2.next().remove();
	dblStep1.next().next().find('.gf_inner_form').append('<li class="add-filter" style="text-align:center;"><a href="#" class="button button-add_new_filter" style="margin-top: 20px;max-width:180px;">Add New Filter</a><div class="clear"></div><li>');
	dblStep3.hide();
	// dblStep3.next().hide();
	jQuery(document).on('click', '.button-add_new_filter', function(e){
		e.preventDefault();			
		var rm_in;
		var rm_wd;
		var rm_ht;
		var rm_price = jQuery('#product-add-to-cart .formattedTotalPrice.ginput_total').text();
		var fb_grp = jQuery('#db-blind-form .gfield.field-group-2 .ginput_container select option:selected').text();
		if($('#db-blind-form').length) {
            var form_selector = $('#db-blind-form .gfield.field-group-2 .ginput_container input');
		} else if($('.variations_form.cart').length) {
            var form_selector = $('.variations_form.cart .gfield.field-group-2 .ginput_container input');
		}


		$(form_selector).each(function(index){
			if ( index == '0') {
				jQuery(this).addClass('') ;
				rm_in = jQuery(this).val();
			}
			if ( index == '1') {
				rm_wd = jQuery(this).val();
			}
			if ( index == '2') {
				rm_ht = jQuery(this).val();
			}
		});
		var dataForm = {
			filter 		: 	jQuery('#db-blind-form #pa_filter option:selected').text(),
			fabric 		: 	jQuery('#db-blind-form #pa_fabric option:selected').text(),
			roller_color: 	jQuery('#db-blind-form #pa_roller-blinds-colour option:selected').text(),
			fb_gr 		: 	fb_grp,
			rm_ins 		: 	rm_in,
			width 		: 	rm_wd,
			height 		: 	rm_ht,
			price		: 	rm_price.replace(/[^0-9]/g, '')
		}
		if( (rm_in.length > 80) || (rm_wd < 300 || rm_wd > 3000) || (rm_ht < 400 || rm_ht > 3300) ) {
			dblsize.find('f1-error').remove();
			dblsize.append('<p class="f1-error gfield_description" style="color: #ff0000;margin:0; width:100%;">Please check fields if it matches the condition.</p>');
		} else {
			jQuery('.f1-error').remove();
			jQuery.ajax({
				url: dbl_scr.ajax_url,
				type: 'post',
				data: {
					action: 'dbl_filter_func',
					values: dataForm
				},
				beforeSend: function(){
					jQuery('#db-blind-form')
						.prepend('<img src="'+dbl_scr.theme_url+'/images/ajax-loader.gif" class="dbl-loader" style="left:45%;position:absolute;top:45%;z-index:2;" />')
						.prepend('<div class="dbl-loader-cover" style="background: #fff;position: absolute;width: 100%;height: 100%;z-index: 1;opacity: 0.5;"></div>');
				},
				complete: function(){
					jQuery('#db-blind-form .gfield.field-group-1s .step-label').text('3');
					jQuery('#db-blind-form .gfield.field-group-1s .text-label').text('Size: Blockout');
					jQuery('#db-blind-form .gfield.field-group-1 .step-label').text('4');
					jQuery('#db-blind-form .gfield.field-group-1 .text-label').text('Colour & Fabric: Blockout');
					jQuery('#db-blind-form .gfield.field-group-3 .step-label').text('5');
					jQuery('#db-blind-form .gfield.field-group-1').trigger('click');
					jQuery('#db-blind-form #pa_filter').val('');
					jQuery('#db-blind-form #pa_fabric').val('');
					jQuery('#db-blind-form #pa_roller-blinds-colour').val('');
					jQuery('#db-blind-form #picker_pa_filter .select-option.swatch-wrapper.selected')
						.trigger('click')
						.css('display', 'none');
					blockOut.show();
					jQuery('img.dbl-loader').remove();		
					jQuery('.dbl-loader-cover').remove();
					jQuery('li.add-filter').remove();
				},
				success: function(response){
					console.log(response);
					jQuery('#db-blind-form input[name="dbl_filter_1"]').val(response[0].filter);
					jQuery('#db-blind-form input[name="dbl_filter_1_fabric"]').val(response[0].fabric);
					jQuery('#db-blind-form input[name="dbl_filter_1_color"]').val(response[0].roller_color);
					jQuery('#db-blind-form input[name="dbl_filter_1_group"]').val(response[0].fb_gr);
					jQuery('#db-blind-form input[name="dbl_filter_1_rmins"]').val(response[0].rm_ins);
					jQuery('#db-blind-form input[name="dbl_filter_1_width"]').val(response[0].width);
					jQuery('#db-blind-form input[name="dbl_filter_1_height"]').val(response[0].height);
					jQuery('#db-blind-form input[name="dbl_filter_1_price"]').val(response[0].price);
					f1_oldprice = response[0].price;
				},
				dataType: 'json'
			});
		}
	});
	jQuery('#db-blind-form .gf_inner_form .gfield.prod-qty input').keyup(function(){
		var f1_newprice = f1_oldprice * jQuery(this).val();
		jQuery('#db-blind-form input[name="dbl_filter_1_price"]').val(f1_newprice);
	});
});