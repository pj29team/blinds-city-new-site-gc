<?php

/* Template Name: Checkout Page */

get_header();
?>

<style media="screen" type="text/css">
.cart-col .cart_totals, .cart-col .checkout-button, .cart-col .wc-forward, .cart-col .shop_table.cart .actions .pull-right { display: none!important; }
</style>

<div id="checkout-container">
	<div id="cart-sum" class="cart-col collapse col-lg-6 col-sm-12 pull-right">
        <h3 class="sum-title">Cart Review</h3>
        <div class="w-cart">
            <?php echo do_shortcode('[woocommerce_cart]');?>
        </div>
        <div class="order-review">
            <?php include( 'woocommerce/checkout/review-order.php' ); ?>
        </div>
	</div>
<!--	<div class="cart-toggle-arrow hidden-lg hidden-md" align="center" style="margin-top: 20px;">
        <a class="tog-cart" href="#cart-sum" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="cart-sum"><i class="fa fa-chevron-down" style="font-size: 20px;"></i></a>
    </div>-->
    <div class="checkout-col col-lg-6 col-sm-12 pull-left">
        <h3 class="c-title">Checkout</h3>
        <h3 class="or-title">Order Received</h3>
        <?php echo do_shortcode('[woocommerce_checkout]');?>
    </div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('.shop_table_responsive.cart tr.cart_item').each(function(index, value){
			var w = jQuery(this).find('dd.variation-Width').text();
			var h = jQuery(this).find('dd.variation-Height').text();
			var q = jQuery(this).find('select.qty').val();
			var num = index + 1;
			jQuery('.pqty-wrapper').hide();
			if (w != '' || h != '') jQuery(this).find('dl.variation').prepend('<p class="item-wh">'+w+' x '+h+'</p>');
			jQuery(this).find('td.product-quantity').append('<p class="item-qty">Qty<br>'+q+'</p>');
			jQuery(this).prepend('<p class="item-num">'+ num +'</p>');
		});
		jQuery('a.tog-cart').click(function(){
			if ( jQuery('.cart-total-price i').hasClass('fa-chevron-down') ){
				jQuery('.cart-total-price i').removeClass('fa-chevron-down');
				jQuery('.cart-total-price i').addClass('fa-chevron-up');
				jQuery('.cart-toggle-arrow i').removeClass('fa-chevron-down');
				jQuery('.cart-toggle-arrow i').addClass('fa-chevron-up');
			} else {
				jQuery('.cart-total-price i').removeClass('fa-chevron-up');
				jQuery('.cart-total-price i').addClass('fa-chevron-down');
				jQuery('.cart-toggle-arrow i').removeClass('fa-chevron-up');
				jQuery('.cart-toggle-arrow i').addClass('fa-chevron-down');
			}
		});

	});
</script>
<?php get_footer(); ?>





