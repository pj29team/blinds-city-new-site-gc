jQuery(document).ready(function ($) {
	var $samples_page = $('.samples-page');
	
	var sp_loadColours = function ($sp, prod_id, filters) {
		var variations = p_variations[prod_id];
		
		var attribute_terms = p_attribute_terms[prod_id];
		var attribute_keys;
		
		var colour_key;
		var colour_attribute;
		var colour_terms;
		
		var samples_html = '';
		
		var prod_in_cart = typeof p_items_in_cart[prod_id] !== 'undefined';
		
		for (var i = 0, j = variations.length; i < j; i++) {
			var variation = variations[i];
			var attributes = variation.attributes;
			var found = false;
			
			for (var filter in filters) {
				var filter_value = filters[filter];
				
				if (attributes['attribute_' + filter] !== filter_value) {
					found = false;
					break;
				}
				
				if (found === false) {
					found = true;
				}
			}
			
			if (found === false) {
				continue;
			}
			
			if (!attribute_keys) {
				attribute_keys = Object.keys(attributes);
				
				colour_key = attribute_keys[attribute_keys.length - 1];
				colour_attribute = colour_key.substr(10);
				colour_terms = attribute_terms[colour_attribute];
			}
			
			//devlog('colour_key: ' + colour_key);
			//devlog('colour_attribute: ' + colour_attribute);
			//devlog('colour_terms: ', colour_terms);
			
			var colour_name = attributes[colour_key];
			
			var found_colour_term = null;
			
			for (var k = 0, m = colour_terms.length; k < m; k++) {
				var colour_term = colour_terms[k];
				
				if (colour_term.slug === colour_name) {
					found_colour_term = colour_term;
					break;
				}
			}
			
			if (found_colour_term === null) {
				continue;
			}
			
			var variation_id = variation.variation_id;
			
			var description = found_colour_term.description.split(' - ');
			
			var sample_class = 'sample';

			console.log(variation);

			var image_src = variation.image.src.split('.');
			
			if (prod_in_cart
				&& (p_items_in_cart[prod_id].indexOf(variation_id) != -1)) {
				sample_class += ' sample-added';
			}
			
			samples_html += '<div class="' + sample_class + '"' +
			' data-variation_id="' + variation_id + '"' +
			' data-attribute="' + colour_attribute + '"' +
			' data-value="' + colour_name + '"' +
			'>' +
				'<div class="sample-box">' +
					'<div class="picture">' +
						'<img src="' + image_src[0] + '.' + image_src[1] + '.' + image_src[2] + '-300x300.' + image_src[3] + '" alt="">' +
					'</div>' +
					'<div class="title">' +
						found_colour_term.name +
					'</div>' +
					'<div class="subtitle">' +
						description[description.length > 1 ? 1 : 0] +
					'</div>' +
					'<div class="buttons">' +
						'<button class="btn-select">Order Sample</button>' +
						'<button class="btn-remove">Remove</button>' +
					'</div>' +
					'<div class="ajax-loader-wrap"><div class="ajax-loader"></div></div>' +
				'</div>' +
			'</div>';
		}
		
		if (samples_html !== '') {
			$sp.$samples_b.html(samples_html).fadeIn();
		} else {
			$sp.$samples_b.fadeOut();
		}
	};
	
	var sp_addFilter = function ($sp, name, label, values) {
		var options_html = '<option value="">Choose ' + label + '</option>';
		var options_filter = '';
		
		for (var value_id in values) {
			var value = values[value_id];

			if (value.name == "") break;
			
			options_html += '<option value="' + value.slug + '">' +
			value.name +
			'</option>';

			if ( label == 'Filter') {
				var lochname = '';
				if (window.location.hostname == 'localhost') {
					lochname = '/blindswp';
				}
				options_filter += '<div class="filter_sel" data-filter="'+ value.slug +'"><img src="'+ lochname +'/wp-content/themes/cleansale/samples/images/img-'+ value.slug +'.jpg" /><span>'+ value.name +'</span></div>';
			}

		}
		var dis_n = ''; 
		// if (label == 'Fabric') dis_n = 'style="display:none;"'; 
		var filter_b_html = '<div class="filter" '+ dis_n +' >' +
			'<div class="filter__label">' +
				'Filter by ' + label +
			'</div>' +
			'<div class="filter__box">' +
				// '<div class="filter-wrap">' + 
				// options_filter + 
				// '</div>' +
				'<select name="' + name + '">' +
					options_html +
				'</select>' +
			'</div>' +
		'</div>';
		
		var $filter_b = $(filter_b_html);
		
		$sp.$filters_b.append($filter_b);
	};
	
	$.each($samples_page, function (sp_key, sp) {
		var $sp = $(sp);
		
		$sp.$filters_b = $sp.find('.samples-page__filters:first');
		$sp.$samples_b = $sp.find('.samples-page__samples:first');
		
		var c_prod_id = 0;
		
		$sp.$filters_b.on('change', 'select', function (evt) {
			var $s_box = $(evt.currentTarget);

			var $f_box = $s_box.closest('.filter');
			console.log($f_box);
			
			var s_box_val = $s_box.val();
			
			var f_box_id = $f_box.index();
			
			if (f_box_id == 0) {
				c_prod_id = s_box_val;
			}
			
			$sp.$filters_b.find('.filter:gt(' + f_box_id + ')').remove();
			
			if (c_prod_id == '') {
				$sp.$samples_b.fadeOut();
				return;
			}
			
			var p_attr_keys = Object.keys(p_attributes[c_prod_id]);
			var p_attr_name = p_attr_keys[f_box_id];
			
			var c_filters = {};
			
			var $filters = $sp.$filters_b.find('.filter:gt(0)').find('select');

			
			$filters.each(function (filter_key, filter) {
				var $filter = $(filter);
				var filter_val = $filter.val();
				
				if (filter_val !== '') {
					c_filters[$filter.attr('name')] = filter_val;
				}
			});
			
			sp_loadColours($sp, c_prod_id, c_filters);
			
			if ((!p_attr_name) 
				|| (s_box_val === '')
				|| (f_box_id >= (p_attr_keys.length - 1))) {
				return;
			}
			
			var f_label  = p_attribute_labels[p_attr_name];
			var f_values = {};
			
			var terms = p_attribute_terms[c_prod_id][p_attr_name];
			
			for (var term_id in terms) {
				var term = terms[term_id];
				
				f_values[term.term_id] = {
					name: term.name,
					slug: term.slug
				};
			}
			
			sp_addFilter($sp, p_attr_name, f_label, f_values);
		}).find('.filter:first').find('select').val('').trigger('change');

		$sp.$filters_b.on('click', '.filter_sel', function (evt) {
			var blind = $(this).data('filter');
			$(this).parent().find('.active').removeClass('active');
			$(this).addClass('active').parent().next('select').val(blind).trigger('change').hide();
		});
		
		$sp.$samples_b.on('click', '.btn-select', function (evt) {
			evt.stopPropagation();
			
			var $btn = $(evt.currentTarget);
			var $sample = $btn.closest('.sample');
			var prod_id = c_prod_id;
			var variation_id = parseInt($sample.data('variation_id'));
			var swatch_attr = $sample.data('attribute');
			var swatch_val = $sample.data('value');
			
			$.ajax({
				url: php_array.admin_ajax,
				type: 'POST',
				data: {
					action: 'bc_add_sample',
					prod_id: c_prod_id,
					var_id: variation_id,
					swatch_attr: swatch_attr,
					swatch_val: swatch_val
				},
				beforeSend: function(xhr, settings) {
					$sample.addClass('ajax-loading');
				},
				complete: function(xhr, textstatus) {
					$sample.removeClass('ajax-loading');
				},
				success: function(data) {
					if (typeof p_items_in_cart[prod_id] === 'undefined') {
						p_items_in_cart[prod_id] = [];
					}
					
					var items = p_items_in_cart[prod_id];
					
					items.push(variation_id);
					
					$sample.addClass('sample-added');
					$('#cart').removeClass('open');
					$('#cart .dropdown-menu').html(data.fragments['div.widget_shopping_cart_content']);
					$('#cart .mini-cart-items').remove();
					$('#cart span.cart-icon').append(data.fragments['#cart .mini-cart-items']);
					$('html, body').animate({
						scrollTop: $("#cart").offset().top
					}, 2000);
					$('#cart').addClass('open');
				},
				error: function() {
					alert('Error: Try again later.');
				}
			});
			
			return false;
		});
		
		$sp.$samples_b.on('click', '.btn-remove', function (evt) {
			evt.stopPropagation();
			
			var $btn = $(evt.currentTarget);
			var $sample = $btn.closest('.sample');
			var prod_id = c_prod_id;
			var variation_id = parseInt($sample.data('variation_id'));
			
			$.ajax({
				url: php_array.admin_ajax,
				type: 'POST',
				data: {
					action: 'bc_remove_sample',
					prod_id: c_prod_id,
					var_id: variation_id
				},
				beforeSend: function(xhr, settings) {
					$sample.addClass('ajax-loading');
				},
				complete: function(xhr, textstatus) {
					$sample.removeClass('ajax-loading');
				},
				success: function(data) {
					if (typeof p_items_in_cart[prod_id] !== 'undefined') {
						var items = p_items_in_cart[prod_id];
						var index = items.indexOf(variation_id);
						
						items.splice(index, 1);
					}
					
					$sample.removeClass('sample-added');
					$('#cart').removeClass('open');
					$('#cart .dropdown-menu').html(data.fragments['div.widget_shopping_cart_content']);
					$('#cart .mini-cart-items').remove();
					$('#cart span.cart-icon').append(data.fragments['#cart .mini-cart-items']);
					$('html, body').animate({
						scrollTop: $("#cart").offset().top
					}, 2000);
					$('#cart').addClass('open');
				},
				error: function() {
					alert('Error: Try again later.');
				}
			});
			
			return false;
		});
	});
});