<?php

global $product, $post;

$_product = $product;
$link = get_permalink($post->ID);
$args = array('postid' => $post->ID, 'width' => 340, 'hide_href' => true, 'exclude_video' => false, 'imglink' => false, 'imgnocontainer' => true, 'resizer' => '660auto');
// $video = get_obox_media($args);
$image_option = get_option("ocmx_product_image");

$cart_item_id = (isset($_POST['edit_cart_item']) && $_POST['edit_cart_item']) ? $_POST['edit_cart_item'] : 0;
$cart_item_id = (isset($_POST['edit_cart_item']) && $_POST['edit_cart_item']) ? $_POST['edit_cart_item'] : 0;

//echo "cart_item_id: ".$cart_item_id."<br/>";

$item = WC()->cart->get_cart_item($cart_item_id);

if ($item) {
    $gravity_form_id = $item["_gravity_form_data"]["id"];
    $values = $item["_gravity_form_lead"];
    $variation_data = $item["variation"];
    $formated_price_total = sprintf(get_woocommerce_price_format(), get_woocommerce_currency_symbol(), $item["line_total"]);
}

//echo "gravity_form_id: $gravity_form_id<br/>";
//echo "<pre style='white-space: pre-wrap;'>";print_r($variation_data);echo "</pre>";
//echo "<pre style='white-space: pre-wrap;'>";print_r($values);echo "</pre>";
//echo "<pre style='white-space: pre-wrap;'>";print_r($item);echo "</pre>";

?>
<div id="product-<?php the_ID(); ?>" <?php post_class($styles); ?>>
    <div class="row">
<?php
    /**
     * woocommerce_before_single_product hook
     *
     * @hooked wc_print_notices - 10
     */

     do_action( 'woocommerce_before_single_product' );

     if ( post_password_required() ) {
        echo get_the_password_form();
        return;
     }
?>
</div>


<div class="row">
    <div class="image-mains">
        <?php
            /**
             * woocommerce_before_single_product_summary hook
             *
             * @hooked woocommerce_show_product_sale_flash - 10
             * @hooked woocommerce_show_product_images - 20
             */
            do_action( 'woocommerce_before_single_product_summary' );
        ?>
    </div>
    <div class="information">
    <div class="summary entry-summary <?php if( $product->is_type( 'simple' ) ) echo 'simple' ;?>">

        <?php
            /**
             * woocommerce_single_product_summary hook
             *
             * @hooked woocommerce_template_single_title - 5
             * @hooked woocommerce_template_single_rating - 10
             * @hooked woocommerce_template_single_price - 10
             * @hooked woocommerce_template_single_excerpt - 20
             * @hooked woocommerce_template_single_add_to_cart - 30
             * @hooked woocommerce_template_single_meta - 40
             * @hooked woocommerce_template_single_sharing - 50
             */
            do_action( 'woocommerce_single_product_summary' );

        ?>
        <div style="clear:both;"></div>

        <div class="zippay <?php if( $product->is_type( 'simple' ) ) echo 'simple' ;?>">
            <div class="bc-icon zip-icon"></div>
            <div class="zippay-desc">
                <p>Shop now, pay later with 12 months interest free</p>
                <a href="#">Click to know how</a>
            </div>
        </div>
        <?php if( $product->is_type( 'variable' ) ) : ?>
        <div class="custom-order-button">
            <a class="button">
                <i class="bc-icon cart-icon white"></i>
                <span>CUSTOMIZE &amp; ORDER</span>
            </a>
        </div>

        <?php endif; ?>

    </div><!-- .summary -->
    </div>
</div>

    <meta itemprop="url" content="<?php the_permalink(); ?>" />
<?php if( $product->is_type( 'variable' ) ) : ?>
<div class="product-info-tabs">
    <div class="row">
        <ul>
            <li class="info-1"><a href="#" class="tab-overview">Overview <i class="bc-icon angle-down-icon"></i></a></li>
            <li class="info-2"><a href="#" class="tab-specifications">Specifications <i class="bc-icon angle-down-icon"></i></a></li>
            <li class="info-3"><a href="#" class="tab-warranty">Warranty <i class="bc-icon angle-down-icon"></i></a></li>
        </ul>
    </div>
</div>
<div id="product-content">
    <?php echo do_shortcode(get_the_content()); ?>
</div>
<?php endif; ?>


</div><!-- #product-<?php the_ID(); ?> -->
<!-- Modal -->
<!-- <div class="modal fade" id="product-content" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body">
        
      </div>
    </div><!-- /.modal-content
  </div> -->
<?php if( $product->is_type( 'variable' ) ) : ?>
<script type="text/javascript">
    jQuery(document).ready(function($){
        // $('.product-info-tabs li a').click(function(){
        $('#product-content').hide();
        // });
        $('.product-info-tabs li a.tab-overview').click(function(e){
            $(this).parent().parent().find('li a').removeClass('active');
            $('#product-content .wpb_accordion_section').each(function(index){
                if (index == 0) $(this).find('h3').trigger('click');
            });
            e.preventDefault();
            $('#product-content').slideDown('400');
            $(this).addClass('active');
        });
        $('.product-info-tabs li a.tab-specifications').click(function(e){
            $(this).parent().parent().find('li a').removeClass('active');
            $('#product-content .wpb_accordion_section').each(function(index){
                if (index == 1) $(this).find('h3').trigger('click');
            });
            e.preventDefault();
            $('#product-content').slideDown('400');
            $(this).addClass('active');
        });
        $('.product-info-tabs li a.tab-warranty').click(function(e){
            $(this).parent().parent().find('li a').removeClass('active');
            $('#product-content .wpb_accordion_section').each(function(index){
                if (index == 2) $(this).find('h3').trigger('click');
            });
            e.preventDefault();
            $('#product-content').slideDown('400');
            $(this).addClass('active');
        });
    });
</script>
<?php endif; ?>

<div class="woo-after-single-product-summary <?php if( $product->is_type( 'simple' ) ) echo 'simple'; ?>">
        <div class="container">
            <div class="row">
                <?php if( $product->is_type( 'variable' ) ) : ?>
                <?php 
                    $manweek = rwmb_meta( 'bcmeta_manufacture_weeks' );
                    $del = rwmb_meta( 'bcmeta_delivery' );
                    $manwar = rwmb_meta( 'bcmeta_manufacture_warranty' );
                ?>

                <div class="product-more-info">
                    <div class="get-samples">
                        <i class="bc-icon sample-icon"></i>
                        <span>Want to see this fabric before you buy it? Get your <a href="<?php get_bloginfo('url')?>/samples" class="button">FREE SAMPLE</a></span>
                    </div>
                    <div class="more-info">
                        <ul>
                            <li>
                                <i class="bc-icon factory-icon"></i>
                                <p>Manufacture</p>
                                <p><?php echo (empty($manweek)) ? '2-3 weeks' : $manweek;?></p>
                            </li>
                            <li class="info-mid">
                                <i class="bc-icon manufacture-icon"></i>
                                <p>Leaves warehouse in</p>
                                <p>Delivery: <?php echo (empty($del)) ? '3 weeks' : $del;?></p>
                            </li>
                            <li>
                                <i class="bc-icon warranty-icon"></i>
                                <p><?php echo (empty($manvar)) ? '5 years' : $manwar;?> Manufacturer's</p>
                                <p>Warranty</p>
                            </li>
                        </ul>
                    </div>
                </div>
            <?php endif; ?>
                <div class="woo-after-single-content col-xs-12 <?php echo esc_attr($sidebar_configs['main']['class']); ?>">
                    <?php if( $product->is_type( 'variable' ) ) : ?>
                    <h3>Customise Your Order</h3>
                    <h5>Begin by selecting your options</h5>
                    <?php endif; ?>
                    <?php 
                        /**
                         * woocommerce_after_single_product_summary hook
                         *
                         * @hooked woocommerce_output_product_data_tabs - 10
                         * @hooked woocommerce_upsell_display - 15
                         * @hooked woocommerce_output_related_products - 20
                         */
                        do_action( 'woocommerce_after_single_product_summary' ); 
                    ?>

                </div>



            </div>
        </div>
    </div>


<script type="text/javascript">
	var edit_cart_config = {
		'form_id' : '<?php echo $gravity_form_id; ?>',
		'values' : <?php echo json_encode($values); ?>,
		'variation_data' : <?php echo JSON_encode($variation_data); ?>,
		'formated_price_total' : '<?php echo $formated_price_total; ?>'
	};
	
	jQuery(document).ready(function ($) {
		$('.single_add_to_cart_button').html('Save');
		$('<input>', {
			'type'  : 'hidden',
			'name'  : 'edit_cart_item',
			'value' : '<?php echo $cart_item_id; ?>'
		}).appendTo('.variations_form');
	});
</script>
