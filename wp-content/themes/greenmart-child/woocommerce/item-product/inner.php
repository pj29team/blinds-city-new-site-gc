<?php 
global $product;

$rating				= wc_get_rating_html( $product->get_average_rating());


?>
<div class="product-block grid" data-product-id="<?php echo esc_attr($product->get_id()); ?>">
	<div class="product-content">
		<div class="block-inner">
			<figure class="image">
				<?php woocommerce_show_product_loop_sale_flash(); ?>
				<a title="<?php the_title(); ?>" href="<?php echo the_permalink(); ?>" class="product-image">
					<?php
						/**
						* woocommerce_before_shop_loop_item_title hook
						*
						* @hooked woocommerce_show_product_loop_sale_flash - 10
						* @hooked woocommerce_template_loop_product_thumbnail - 10
						*/
						remove_action('woocommerce_before_shop_loop_item_title','woocommerce_show_product_loop_sale_flash', 10);
						do_action( 'woocommerce_before_shop_loop_item_title' );
					?>
				</a>
				
			</figure>

			

		</div>
		<div class="caption">
			<div class="meta">
				<div class="infor">
					<div class="name-subtitle">
						<h3 class="name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

						<?php do_action( 'greenmart_after_title_tbay_subtitle'); ?>
						
					</div>
					
					<?php
						/**
						* woocommerce_after_shop_loop_item_title hook
						*
						* @hooked woocommerce_template_loop_rating - 5
						* @hooked woocommerce_template_loop_price - 10
						*/
						
						do_action( 'woocommerce_after_shop_loop_item_title');

					?>
					
				</div>
			</div>  
			<div class="groups-button clearfix">
				<?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
			</div>		
		</div>
    </div>
</div>
