<?php
/**
 * Variable product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/variable.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.5.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product, $product_fields, $post;
$product_fields = get_fields();

if (isset($product_fields['is_ready_made_product']) && $product_fields['is_ready_made_product']): // If Ready Made
echo '<link rel="stylesheet" type="text/css" href="'.get_stylesheet_directory_uri().'/css/ready_made.css">';
// Begin ready-made
echo '<div class="ready-made"',
' data-product_id="', absint($product->id), '"',
'>';

$scripts = array();

if ($product_fields['product_rows']) {
	$rows_product =& $product_fields['product_rows'];
	
	$scripts[] = 'var product_rows = ' . json_encode($rows_product);
	
	foreach ($rows_product as $row_product) {
		if ($row_product['use_display_expression']) {
			$hide = true;
		} else {
			$hide = false;
		}
		
		echo '<div class="products', ($hide ? ' ready-hidden' : ''), '"',
		' id="row_product_', $row_product['id'], '"',
		'>';
		
		foreach ($row_product['products'] as $_product) {
			echo '<div class="product" id="product_', $_product['id'], '">';
			
			if (isset($_product['image']['sizes']['shop_single'])) {
				echo '<div class="product-image">',
				'<img src="', $_product['image']['sizes']['shop_single'], '" alt="">',
				'</div>';
			}
			
			$words = preg_split('/(\s+)/u', $_product['name']);
			$title = '<span class="high">' . $words[0] . '</span>';
			
			if (sizeof($words) > 1) {
				$title .= ' ' . implode(' ', array_slice($words, 1));
			}
			
			echo '<div class="product-wrap">',
			'<h1 class="product-title">', $title, '</h1>',
			'<a href="#" class="select-btn">Select</a>',
			'</div>',
			'<div class="borders"></div>';
			
			echo '</div>'; // End product
		}
		
		echo '</div>'; // End product rows
	}
	
	//echo '<div class="product-tip">Select your product</div>';
	
	echo '<div class="product-more ready-hidden">',
	'<div class="product-description"></div>',
	'<div class="product-rating">',
	'<img src="', get_template_directory_uri(), '/images/ready_made/rating.png"',
	' alt="5 stars">',
	'</div>',
	'</div>';
	
	echo '<div class="product-info ready-hidden">',
	'<div class="product-info-price">$0.00</div>',
	'<div class="product-info-advantages"></div>',
	'</div>';
}

if ($product_fields['colour_rows']) {
	$rows_colour =& $product_fields['colour_rows'];
	
	$scripts[] = 'var colour_rows = ' . json_encode($rows_colour);
	
	foreach ($rows_colour as $row_colour) {
		echo '<div class="product-row product-colour ready-hidden"',
		' id="row_colour_', esc_attr($row_colour['id']), '"',
		'>',
		'<div class="product-row-title">',
		esc_html($row_colour['name']),
		'</div>',
		'<div class="colours">';
		
		foreach ($row_colour['colours'] as $colour) {
			echo '<a href="#" class="colour"',
			' id="colour_', esc_attr($colour['id']), '">',
			'<span class="image">';
			
			if (isset($colour['image']['sizes'])) {
				echo '<img src="', $colour['image']['sizes']['80x80'], '" alt="">';
			}
			
			echo '</span>';
			
			if ($colour['name']) {
				echo '<span class="name">',
				esc_html($colour['name']),
				'</span>';
			}
			
			echo '</a>';
		}
		
		echo '</div>',
		'<div class="product-status-icon"></div>',
		'</div>';
	}
}

if ($product_fields['size_rows']) {
	$rows_size =& $product_fields['size_rows'];
	
	$scripts[] = 'var size_rows = ' . json_encode($rows_size);
	
	foreach ($rows_size as $row_size) {
		$size_id = 'row_size_' . esc_attr($row_size['id']);
		
		echo '<div class="product-row product-size ready-hidden"',
		' id="', $size_id, '"',
		'>';
		
		echo '<div class="product-columns">'; // Begin columns
		
		echo '<div class="product-column column-size">', // Begin Size
		'<div class="product-row-title">',
		esc_html($row_size['name']),
		'</div>';
		
		echo '<select name="', $size_id, '" class="select sizes">';
		
		if ($row_size['placeholder']) {
			echo '<option value="">',
			esc_html($row_size['placeholder']),
			'</option>';
		}
		
		foreach ($row_size['sizes'] as $size) {
			echo '<option value="', esc_attr($size['name']), '"',
			' id="size_', esc_attr($size['id']), '"',
			'>',
			esc_html($size['name']),
			'</option>';
		}
		
		echo '</select>';
		
		echo '<div class="require-custom">',
		'<a href="#">Require Custom Size Blinds?</a>',
		'</div>';
		
		echo '</div>'; // End Size
		
		$qnt_id = 'qnt_row_size_' . esc_attr($row_size['id']);
		
		echo '<div class="product-column column-quantity">', // Begin Quantity
		'<div class="product-row-title">Quantity</div>';
		
		echo '<a href="#" class="quantity-btn btn-minus"',
		' data-field="#', $qnt_id, '">-</a>',
		'<input type="number" name="', $qnt_id, '"',
		' class="input"',
		' id="', $qnt_id, '"',
		' value="1" min="1" max="99"',
		'>',
		'<a href="#" class="quantity-btn btn-plus"',
		' data-field="#', $qnt_id, '">+</a>';
		
		echo '</div>'; // End Quantity
		
		echo '</div>'; // End columns
		
		echo '<div class="product-status-icon"></div>',
		'</div>';
	}
}

if ($product_fields['extra_rows']) {
	$rows_extra =& $product_fields['extra_rows'];
	
	$scripts[] = 'var extra_rows = ' . json_encode($rows_extra);
	
	foreach ($rows_extra as $row_extra) {
		echo '<div class="product-row product-extras ready-hidden"',
		' id="row_extra_', esc_html($row_extra['id']), '"',
		'>';
		
		echo '<div class="product-row-title">',
		esc_html($row_extra['name']),
		'</div>';
		
		foreach ($row_extra['extras'] as $extra) {
			echo '<div class="product-columns">'; // Begin columns
			
			echo '<div class="product-column column-extras">',
			'<div class="extras">',
			'<a href="#" class="extra"',
			' id="extra_', $extra['id'], '"',
			'>',
			'<span class="extra-name">', esc_html($extra['name']), '</span>',
			'<span class="extra-image">';
			
			if (isset($extra['image']['sizes']['106x106'])) {
				echo '<img src="', $extra['image']['sizes']['106x106'], '" alt="">';
			}
			
			echo '</span>',
			'<span class="extra-radio"></span>',
			'<span class="extra-price">$', esc_html($extra['price']), '</span>',
			'</a>',
			'</div>',
			'</div>'; // End Extras
			
			$qnt_id = 'qnt_extra_' . esc_attr($extra['id']);
			
			echo '<div class="product-column column-quantity">', // Begin Quantity
			'<div class="product-row-title">Quantity</div>';
			
			echo '<a href="#" class="quantity-btn btn-minus"',
			' data-field="#', $qnt_id, '">-</a>',
			'<input type="number" name="', $qnt_id, '"',
			' class="input"',
			' id="', $qnt_id, '"',
			' value="1" min="1" max="99"',
			'>',
			'<a href="#" class="quantity-btn btn-plus"',
			' data-field="#', $qnt_id, '">+</a>';
			
			echo '</div>'; // End Quantity
			
			echo '</div>'; // End columns
		}
		
		echo '</div>';
	}
}

echo '<div class="ready-made-totals ready-hidden">',
'<div class="total-price">',
'<div class="total-price-caption">Total Price</div>',
'<div class="total-price-value">$0.00</div>',
'</div>',
'<div class="plus-delivery-charges">',
'+ Delivery charges',
'</div>',
'<div class="add-to-cart">',
'<a href="#" class="add-to-cart-btn">Add to cart</a>',
'</div>',
'</div>';

echo '<div class="ajax-frame"></div></div>'; // End ready-made

echo '<script type="text/javascript">',
implode(';' . PHP_EOL, $scripts),
'</script>';

echo '<script type="text/javascript"',
' src="', get_stylesheet_directory_uri(), '/scripts/ready_made.js"></script>';

else: // End If Not Ready Made
	
	
	if(has_term('fixed-product','product_cat',$post)){
	




			// Availability
			$availability      = $product->get_availability();
			$availability_html = empty( $availability['availability'] ) ? '' : '<p class="stock ' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</p>';

			//echo apply_filters( 'woocommerce_stock_html', $availability_html, $availability['availability'], $product );
		?>

		<?php if ( $product->is_in_stock() ) : ?>

			<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>


			<!-- 
			<form class="cart" method="post" enctype='multipart/form-data'>
			 	<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

			 	<?php
			 		if ( ! $product->is_sold_individually() ) {
			 			echo '<span class="quantity-label">Quantity:</span>';
			 			woocommerce_quantity_input( array(
			 				'min_value'   => apply_filters( 'woocommerce_quantity_input_min', 1, $product ),
			 				'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->backorders_allowed() ? '' : $product->get_stock_quantity(), $product ),
			 				'input_value' => ( isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : 1 )
			 			) );
			 		}

			 		
			 	?>

			 	<input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->id ); ?>" />


			 	<button type="submit" data-quantity="1" data-product_id="<?php echo $product->id; ?>"
				    class="button alt ajax_add_to_cart add_to_cart_button product_type_simple">
				    ADD TO CART
				</button>

				<?php do_action( 'woocommerce_after_add_to_cart_button' );

				?>

				
			</form>

			-->

			<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>



		<?php endif; 

	}else{
		echo "not fixed product";
	}


endif;

?>
