<?php
/**
 * Variable product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/variable.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.5.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product, $product_fields;
$product_fields = get_fields();

if (isset($product_fields['is_ready_made_product']) && $product_fields['is_ready_made_product']): // If Ready Made
echo '<link rel="stylesheet" type="text/css" href="'.get_stylesheet_directory_uri().'/css/ready_made.css">';
// Begin ready-made
echo '<div class="ready-made"',
' data-product_id="', absint($product->id), '"',
'>';

$scripts = array();

if ($product_fields['product_rows']) {
	$rows_product =& $product_fields['product_rows'];
	
	$scripts[] = 'var product_rows = ' . json_encode($rows_product);
	
	foreach ($rows_product as $row_product) {
		if ($row_product['use_display_expression']) {
			$hide = true;
		} else {
			$hide = false;
		}
		
		echo '<div class="products', ($hide ? ' ready-hidden' : ''), '"',
		' id="row_product_', $row_product['id'], '"',
		'>';
		
		foreach ($row_product['products'] as $_product) {
			echo '<div class="product" id="product_', $_product['id'], '">';
			
			if ($_product['image']) {
				echo '<div class="product-image">',
				'<img src="', $_product['image']['sizes']['shop_single'], '" alt="">',
				'</div>';
			}
			
			$words = preg_split('/(\s+)/u', $_product['name']);
			$title = '<span class="high">' . $words[0] . '</span>';
			
			if (sizeof($words) > 1) {
				$title .= ' ' . implode(' ', array_slice($words, 1));
			}
			
			echo '<div class="product-wrap">',
			'<h1 class="product-title">', $title, '</h1>',
			'<a href="#" class="select-btn">Select</a>',
			'</div>',
			'<div class="borders"></div>';
			
			echo '</div>'; // End product
		}
		
		echo '</div>'; // End product rows
	}
	
	echo '<div class="product-tip">Select your product</div>';
	
	echo '<div class="product-more ready-hidden">',
	'<div class="product-description"></div>',
	'<div class="product-rating">',
	'<img src="', get_template_directory_uri(), '/images/ready_made/rating.png"',
	' alt="5 stars">',
	'</div>',
	'</div>';
	
	echo '<div class="product-info ready-hidden">',
	'<div class="product-info-price">$0.00</div>',
	'<div class="product-info-advantages">',
	'Leaves warehouse in 2 days',
	'</div>',
	'</div>';
}

if ($product_fields['colour_rows']) {
	$rows_colour =& $product_fields['colour_rows'];
	
	$scripts[] = 'var colour_rows = ' . json_encode($rows_colour);
	
	foreach ($rows_colour as $row_colour) {
		echo '<div class="product-row product-colour ready-hidden"',
		' id="row_colour_', esc_attr($row_colour['id']), '"',
		'>',
		'<div class="product-row-title">Colour</div>',
		'<div class="colours">';
		
		foreach ($row_colour['colours'] as $colour) {
			echo '<a href="#" class="colour"',
			' id="colour_', esc_attr($colour['id']), '">',
			'<img src="', $colour['image']['sizes']['80x80'], '" alt="">',
			'</a>';
		}
		
		echo '</div>',
		'<div class="product-status-icon"></div>',
		'</div>';
	}
}

if ($product_fields['size_rows']) {
	$rows_size =& $product_fields['size_rows'];
	
	$scripts[] = 'var size_rows = ' . json_encode($rows_size);
	
	foreach ($rows_size as $row_size) {
		$size_id = 'row_size_' . esc_attr($row_size['id']);
		
		echo '<div class="product-row product-size ready-hidden"',
		' id="', $size_id, '"',
		'>';
		
		echo '<div class="product-columns">'; // Begin columns
		
		echo '<div class="product-column column-size">', // Begin Size
		'<div class="product-row-title">',
		esc_html($row_size['name']),
		'</div>';
		
		echo '<select name="', $size_id, '" class="select sizes">';
		
		if ($row_size['placeholder']) {
			echo '<option value="">',
			esc_html($row_size['placeholder']),
			'</option>';
		}
		
		foreach ($row_size['sizes'] as $size) {
			echo '<option value="', esc_attr($size['name']), '"',
			' id="size_', esc_attr($size['id']), '"',
			'>',
			esc_html($size['name']),
			'</option>';
		}
		
		echo '</select>';
		
		echo '<div class="require-custom">',
		'<a href="#">Require Custom Size Blinds?</a>',
		'</div>';
		
		echo '</div>'; // End Size
		
		$qnt_id = 'qnt_row_size_' . esc_attr($row_size['id']);
		
		echo '<div class="product-column column-quantity">', // Begin Quantity
		'<div class="product-row-title">Quantity</div>';
		
		echo '<a href="#" class="quantity-btn btn-minus"',
		' data-field="#', $qnt_id, '">-</a>',
		'<input type="number" name="', $qnt_id, '"',
		' class="input"',
		' id="', $qnt_id, '"',
		' value="1" min="1" max="99"',
		'>',
		'<a href="#" class="quantity-btn btn-plus"',
		' data-field="#', $qnt_id, '">+</a>';
		
		echo '</div>'; // End Quantity
		
		echo '</div>'; // End columns
		
		echo '<div class="product-status-icon"></div>',
		'</div>';
	}
}

if ($product_fields['extra_rows']) {
	$rows_extra =& $product_fields['extra_rows'];
	
	$scripts[] = 'var extra_rows = ' . json_encode($rows_extra);
	
	foreach ($rows_extra as $row_extra) {
		echo '<div class="product-row product-extras ready-hidden"',
		' id="row_extra_', esc_html($row_extra['id']), '"',
		'>';
		
		echo '<div class="product-row-title">',
		esc_html($row_extra['name']),
		'</div>';
		
		foreach ($row_extra['extras'] as $extra) {
			echo '<div class="product-columns">'; // Begin columns
			
			echo '<div class="product-column column-extras">',
			'<div class="extras">',
			'<a href="#" class="extra"',
			' id="extra_', $extra['id'], '"',
			'>',
			'<span class="extra-name">', esc_html($extra['name']), '</span>',
			'<span class="extra-image">',
			'<img src="', $extra['image']['sizes']['106x106'], '" alt="">',
			'</span>',
			'<span class="extra-radio"></span>',
			'<span class="extra-price">$', esc_html($extra['price']), '</span>',
			'</a>',
			'</div>',
			'</div>'; // End Extras
			
			$qnt_id = 'qnt_extra_' . esc_attr($extra['id']);
			
			echo '<div class="product-column column-quantity">', // Begin Quantity
			'<div class="product-row-title">Quantity</div>';
			
			echo '<a href="#" class="quantity-btn btn-minus"',
			' data-field="#', $qnt_id, '">-</a>',
			'<input type="number" name="', $qnt_id, '"',
			' class="input"',
			' id="', $qnt_id, '"',
			' value="1" min="1" max="99"',
			'>',
			'<a href="#" class="quantity-btn btn-plus"',
			' data-field="#', $qnt_id, '">+</a>';
			
			echo '</div>'; // End Quantity
			
			echo '</div>'; // End columns
		}
		
		echo '</div>';
	}
}

echo '<div class="ready-made-totals ready-hidden">',
'<div class="total-price">',
'<div class="total-price-caption">Total Price</div>',
'<div class="total-price-value">$0.00</div>',
'</div>',
'<div class="add-to-cart">',
'<a href="#" class="add-to-cart-btn">Add to cart</a>',
'</div>',
'</div>';

echo '<div class="ajax-frame"></div></div>'; // End ready-made

echo '<script type="text/javascript">',
implode(';' . PHP_EOL, $scripts),
'</script>';

echo '<script type="text/javascript"',
' src="', get_stylesheet_directory_uri(), '/scripts/ready_made.js"></script>';

else: // Else If Not Ready Made

$attribute_keys = array_keys( $attributes );

function bs_single_add_to_cart_text($text, $_product) {
	global $product;
	
	if ($product->ID === $_product->ID) {
		$text = 'Checkout';
	}
	return $text;
}

add_filter('woocommerce_product_single_add_to_cart_text', 'bs_single_add_to_cart_text', 10, 2);

do_action( 'woocommerce_before_add_to_cart_form' ); ?>

<form <?php if( has_term('double-roller-blinds','product_cat')) echo 'id=db-blind-form';?> class="variations_form cart" method="post" enctype='multipart/form-data' data-product_id="<?php echo absint( $product->id ); ?>" data-product_variations="<?php echo htmlspecialchars( json_encode( $available_variations ) ) ?>">
	<?php do_action( 'woocommerce_before_variations_form' ); ?>

	 <?php if( has_term('double-roller-blinds','product_cat')) { //* Edits. Used to get the data for the 1st filter.?>
	 		<input type="hidden" name="dbl_filter_1" value="">
	 		<input type="hidden" name="dbl_filter_1_fabric" value="">
	 		<input type="hidden" name="dbl_filter_1_color" value="">
	 		<input type="hidden" name="dbl_filter_1_group" value="">
	 		<input type="hidden" name="dbl_filter_1_rmins" value="">
	 		<input type="hidden" name="dbl_filter_1_width" value="">
	 		<input type="hidden" name="dbl_filter_1_height" value="">
	 		<input type="hidden" name="dbl_filter_1_price" value="">
	 		<input type="hidden" name="dbl_filter_2_price" value="">
	 <?php } ?>

	<?php if ( empty( $available_variations ) && false !== $available_variations ) : ?>
		<p class="stock out-of-stock"><?php _e( 'This product is currently out of stock and unavailable.', 'woocommerce' ); ?></p>
	<?php else : ?>
		<div class="gform_wrapper">
			<ul class="gform_fields left_label form_sublabel_below description_below">
				<li class="gfield field-group-1s grp-size gsection active"><h2 class="gsection_title"><span class="step-label">1</span> <span class="text-label">Size</span><i class="bc-icon angle-down-icon"></i></h2></li>
				<li class="gf_will_expand gf_expanded grp-size">
					<ul class="gf_inner_form">
						<li class="gfield field-group-1 field_sublabel_below field_description_below">
								<label class="section-desc">Enter Room Name and Dimension for each window</label>
								<div class="ginput_container rooms">
									<div class="room room-1">
										<div class="left-input">
											<input id="room-name-1" class="room-name" type="text" name="room-name-1" required>
											<label for="room-name-1">Room Name</label>
										</div>
										<div class="mid-input rwidth">
											<input id="room-width-1" class="room-width" type="text" name="room-width-1" required>
											<label for="room-width-1">Width <span>mm</span></label>
											<span class="min-max"></span>
										</div>
										<div class="right-input rheight">
											<input id="room-height-1" class="room-height" type="text" name="room-height-1" required>
											<label for="room-height-1">Height <span>mm</span></label>
											<span class="min-max"></span>
										</div>
										<div class="check">
											<i class="bc-icon check-icon green"></i>
										</div>
									</div>
								</div>
								<div class="clear"></div>
								<div class="add-room" style="display:none;">
									<a class="button" href="#">
										ADD ANOTHER ROOM
										<i class="glyphicon glyphicon-plus"></i>
									</a>
								</div>
								<?php 
								// bs_print_buttons(array(
								// 	'class' => 'gsection-buttons-1',
								// 	'modify' => array(
								// 		'back' => ''
								// 	)
								//);?>

								<script type="text/javascript">
									function convertToSlug(Text){
									    return Text
									        .toLowerCase()
									        .replace(/ /g,'-')
									        .replace(/[^\w-]+/g,'')
									        ;
									}
									jQuery(document).ready(function($){
										var pid = parseInt($('.variations_form').data('product_id'), 10);
											var is_shutter = false;
											if (pid == 258 || pid == 1952 || pid == 1932 || pid == 2351 || pid == 21721 || pid == 21740 || pid == 21771 || pid == 21790 || pid == 21809 || pid == 21828) { 
												is_shutter = true;
											} /* Plantation Shutters, sydney shutters and express shutters */
										var r = 1;
										$('li.gf-off-screen.field-group-2').next().addClass('bc-room-name');
										$(".custom-order-button a").click(function() {
										    $('html, body').animate({
										        scrollTop: $(".woo-after-single-content").offset().top
										    }, 2000);
										});
										$(document).on('click','.add-room a', function(e){
											r++;
										     // $('.ginput_container.rooms .room').each(function(i){
										     // 	if (i == r) {
										     // 		$(this).removeClass('gfield-hidden');
										     // 	}
										     // });
										     $('.ginput_container.rooms').append('<div class="room room-'+ r +'"><div class="left-input"><input id="room-name-'+ r +'" class="room-name" type="text" name="room-name-'+ r +'" required><label for="room-name-'+ r +'">Room Name</label></div><div class="mid-input"><input id="room-width-'+ r +'" class="room-width" type="text" name="room-width-'+ r +'" required><label for="room-width-'+ r +'">Width <span>mm</span></label></div><div class="right-input"><input id="room-height-'+ r +'" class="room-height" type="text" name="room-height-'+ r +'" required><label for="room-height-'+ r +'">Height <span>mm</span></label></div><div class="check"><img src="<?php echo get_stylesheet_directory_uri();?>/images/gfield-status-icon.png"></div></div>')
										     // if (r >= 10) $(this).parent().hide();
											e.preventDefault();
										});
										$('.gsection.field-group-3').next('.gf_will_expand').addClass('gf_expanded');
										$('.gsection.field-group-3 .step-label').text('3');
										$('.gsection.field-group-3 .text-label').text('Customise & Personalise');
										$('.rwidth .min-max').text($('.bc-width-field .gfield_description').text());
										$('.rheight .min-max').text($('.bc-height-field .gfield_description').text());
										var w_range = [];
										var h_range = [];
										$('.bc-width-field .instruction strong').each(function(index){
											w_range.push(parseInt($(this).text()));
										});
										$('.bc-height-field .instruction strong').each(function(index){
											h_range.push(parseInt($(this).text()));
										});
										$('.swatch-control .select-option.swatch-wrapper').each(function(){
											var shade_text = $(this).find('a').attr('title');
											$(this).append('<span class="shade-title">'+ shade_text +'</span>');
										});
										$(document).on('click', '#picker_pa_filter a, #picker_pa_material a', function(e){
											var filter_name = $(this).attr('title');
											$('.label-text.fabric').text('Select '+ filter_name +' Fabric Range');
										});
										// $('.gfield.field-group-3 .gfield_label').each(function(){
										// 	var label = $(this).text().toLowerCase();
										// 	if (label != 'quantity') $(this).append('<span class="label-link">Learn More about '+ label +'</span>');
										// }); removed for later use
										$('.gfield.field-group-3 .gfield_label').each(function(){
											var label = $(this).text();
											$(this).text('Select ' + label);
										});
										$("#bcroom-heading-1 a").addClass('collapsed');
										$('.rooms .room').each(function(){
											$(this).find('input').on('change input', function(){
												var $in = $(this);
												if ( $in.is('.room-name') ) { 
													$('.bc-room-name input').val($in.val()).trigger('keyup');
													$('.panel-group.fg2 .title-rmname').text($in.val());
												}
												if ( $in.is('.room-width') ) { 
													$('.panel-group.fg2 .title-rmwidth').text($in.val());
													if (($in.val() < w_range[0]) || ($in.val() > w_range[1]) ){
														$in.attr('style','border-color: red !important');
														$('.rwidth .min-max').css('color','red');
													} else {
														$('.bc-width-field input').val($in.val()).trigger('keyup');
														if (is_shutter == true){
															$('#width_needed').val($in.val());
														}
														$(this).removeAttr('style');
														$('.rwidth .min-max').removeAttr('style');
													}
												}
												if ( $in.is('.room-height') ) {
													$('.panel-group.fg2 .title-rmheight').text($in.val());
													if (($in.val() < h_range[0]) || ($in.val() > h_range[1]) ){
														$in.attr('style','border-color: red !important');
														$('.rheight .min-max').css('color','red');
													} else {
														$('.bc-height-field input').val($in.val()).trigger('keyup');
														if (is_shutter == true){
															$('#length_needed').val($in.val());
														}
														$(this).removeAttr('style');
														$('.rheight .min-max').removeAttr('style');
													}
												}
												if ($(this).parent().parent().parent().parent().parent().hasClass('ready_step')){
													if ($("#bcroom-heading-1 a").hasClass('collapsed')){
														$("#bcroom-heading-1 a").trigger('click');
														setTimeout(function(){ 
															$('html, body').animate({
													        scrollTop: $("#customize-order").offset().top
													    	}, 2000);
														}, 2000);
													}
												}
											});
										});
									});
								</script>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="gform_wrapper">
			<ul id="customize-order" class="gform_fields left_label form_sublabel_below description_below">
				<li class="gfield field-group-1 gsection active"><h2 class="gsection_title"><span class="step-label">2</span> <span class="text-label">Customize &amp; Personalize</span><i class="bc-icon angle-down-icon"></i></h2></li>
				<li><label class="section-desc f2">Enter Room Name and Dimension for each window</label></li>
				<div class="panel-group fg2" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="bcroom-heading-1">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#bcroom-1" aria-expanded="true" aria-controls="collapseOne" class="accordion-toggle">
          <span class="title-rmname">Name of Room</span>
          <span class="title-msm">
	          <span class="title-rmwidth">width </span>mm
	          <span class="title-x">x</span>
	          <span class="title-rmheight">height </span>mm
          </span>
          <i class="bc-icon angle-down-icon"></i>
        </a>
      </h4>
    </div>
    <div id="bcroom-1" class="room-customize panel-collapse collapse" role="tabpanel" aria-labelledby="bcroom-heading-1">
      <div class="panel-body">
				<li class="gf_will_expand gf_expanded">
					<ul class="gf_inner_form">
						<?php
						
						$dd_found = false;
						$dd_child = null;
						$dd_attr  = null;
						
						//$vertical_blinds = isset($attributes['pa_vertical-blinds-colour']);
						
						/*
						if (!$vertical_blinds) {
							foreach ($attributes as $attribute => &$options) {
								if (!$dd_found) {
									if (
										($attribute === 'pa_fabric')
										|| ($attribute === 'pa_size')
									) {
										$dd_found = true;
										$dd_attr  = $attribute;
									}
									continue;
								}
								
								$attributes[$dd_attr]['dropdown'] = array(
									'attribute' => $attribute,
									'options'   => $options
								);
								
								$dd_found = false;
								$dd_child = $attribute;
							}
						}
						//OLD EXCEPTION FUNCTION FOR VERTICAL BLINDS
						*/

						foreach ($attributes as $attribute => &$options) { 
								if (!$dd_found) {
									if (
										($attribute === 'pa_fabric')
										|| ($attribute === 'pa_size')
									) {
										$dd_found = true;
										$dd_attr  = $attribute;
									}
									continue;
								}
								
								$attributes[$dd_attr]['dropdown'] = array(
									'attribute' => $attribute,
									'options'   => $options
								);
								
								$dd_found = false;
								$dd_child = $attribute;						
						}
						
						$filter_second = array();
						$filter_third  = array();
						
						$next_is_swatch_wrapper =
							isset($attributes['pa_twin-blinds-colour'])
							/*|| $vertical_blinds //OLD EXCEPTION FUNCTION FOR VERTICAL BLINDS*/
							|| isset($attributes['pa_venetian-colours'])
							|| isset($attributes['pa_honeycomb-colour'])
							|| isset($attributes['pa_plantation-colour']);
						
						foreach ($available_variations as $vk => &$variation) {
							$v_attrs =& $variation['attributes'];
							if (
								$dd_child
								&& isset(
									$v_attrs['attribute_pa_filter'],
									$v_attrs['attribute_pa_fabric'],
									$v_attrs['attribute_' . $dd_child]
								)
							) {
								$v_filter = $v_attrs['attribute_pa_filter'];
								$v_fabric = $v_attrs['attribute_pa_fabric'];
								$v_color  = $v_attrs['attribute_' . $dd_child];
								
								$filter_second[$v_filter][] = $v_fabric;
								$filter_third[$v_fabric][]  = $v_color;
							} elseif (isset($v_attrs['attribute_pa_twin-blinds-colour'])) {
								$v_filter = $v_attrs['attribute_pa_filter'];
								$v_colour = $v_attrs['attribute_pa_twin-blinds-colour'];
								
								$filter_second[$v_filter][] = $v_colour;
							} elseif (isset($v_attrs['attribute_pa_vertical-blinds-colour'])) {
								$v_fabric = $v_attrs['attribute_pa_fabric'];
								$v_color  = $v_attrs['attribute_pa_vertical-blinds-colour'];
								
								/* $filter_second[$v_fabric][] = $v_color; OLD EXCEPTION FUNCTION FOR VERTICAL BLINDS */
							} elseif (isset($v_attrs['attribute_pa_venetian-colours'])) {
								$v_material = $v_attrs['attribute_pa_material'];
								$v_size     = $v_attrs['attribute_pa_size'];
								$v_color    = $v_attrs['attribute_pa_venetian-colours'];
								
								$filter_second[$v_material][] = $v_size;
								$filter_third[$v_size][]      = $v_color;
							} elseif (isset($v_attrs['attribute_pa_honeycomb-colour'])) {
								$v_filter = $v_attrs['attribute_pa_filter'];
								$v_colour = $v_attrs['attribute_pa_honeycomb-colour'];
								
								$filter_second[$v_filter][] = $v_colour;
							} elseif (isset($v_attrs['attribute_pa_plantation-colour'])) {
								$v_material = $v_attrs['attribute_pa_material'];
								$v_color    = $v_attrs['attribute_pa_plantation-colour'];
								
								$filter_second[$v_material][] = $v_color;

							} elseif (isset($v_attrs['attribute_pa_roller-shutters-colour'])) {
								$v_material = $v_attrs['attribute_pa_material'];
								$v_size     = $v_attrs['attribute_pa_size'];
								$v_color    = $v_attrs['attribute_pa_roller-shutters-colour'];
								
								$filter_second[$v_material][] = $v_size;
								$filter_third[$v_size][]      = $v_color;
							}
						}
						
						if ($dd_child) {
							unset($attributes[$dd_child]);
						}
						
						$counter = 0;
						
						?>
						<?php foreach ( $attributes as $attribute => &$options ) : ?>
							<li class="gfield field-group-1 field_sublabel_below field_description_below<?php if (++$counter > 1) { echo ' gfield-hidden'; } ?>">
								<label class="gfield_label"><?php
								
								// // (isset($options['dropdown']) ? $options['dropdown']['attribute'] : $attribute
								// var_dump($attribute);

								if ($attribute == 'pa_filter' || $attribute == 'pa_material') {
									// echo '<span class="label-text">Select Blind Type</span><span class="label-link">Learn More about blind types</span>';
									echo '<span class="label-text">Select Blind Type</span>';
								}

								if ($attribute == 'pa_fabric') {
									echo '<span class="label-text fabric">Select Fabric Range</span>';
								}

								if (isset($options['dropdown'])) {
									// echo ' & ';
									
									// echo wc_attribute_label(
									// 	$options['dropdown']['attribute']
									// );
								} else {
									//echo wc_attribute_label($attribute);
								}
								
								?></label>
								<div class="ginput_container">
									<?php
									
									if (
										($attribute === 'pa_filter')
										|| ($attribute === 'pa_material')
										/*|| (
											($attribute === 'pa_fabric')
											&& $vertical_blinds
										)
										//OLD EXCEPTION FUNCTION FOR VERTICAL BLINDS
										*/
									) {
										if ($next_is_swatch_wrapper) {
											$next_elem = '.swatch-wrapper';
										} else {
											$next_elem = '.swatch-filter';
										}
										
										wc_dropdown_variation_attribute_options(
											array(
												'options' => $options,
												'attribute' => $attribute,
												'product' => $product,
												'filter_next' => $filter_second,
												'filter_next_elem' => $next_elem,
												'filter_next_look' => 'data-value',
												'show_status' => 1
											)
										);
									} elseif (
										($attribute === 'pa_fabric')
										|| ($attribute === 'pa_size')
									) {
										$dd =& $options['dropdown'];
										wc_dropdown_variation_attribute_options(
											array(
												'options' => $options,
												'attribute' => $attribute,
												'product' => $product,
												'filters' => $filter_third,
												'filter_for' => $dd['attribute']
											)
										);
										wc_dropdown_variation_attribute_options(
											array(
												'options' => $dd['options'],
												'attribute' => $dd['attribute'],
												'product' => $product,
												'filter_for' => $dd['attribute'],
												'visible_max' => 9
											)
										);
									} else {
										wc_dropdown_variation_attribute_options(
											array(
												'options' => $options,
												'attribute' => $attribute,
												'product' => $product,
												'show_status' => 1
											)
										);
									}
									
									?>
								</div>
								<div class="clear"></div>
							</li>
						<?php endforeach; ?>
						<?php
						
						// bs_print_buttons(array(
						// 	'class' => 'gsection-buttons-2',
						// 	// 'modify' => array(
						// 	// 	'back' => ''
						// 	// )
						// ));
						
						?>
					</ul>
		<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

		<div class="single_variation_wrap">
			<?php
				/**
				 * woocommerce_before_single_variation Hook.
				 */
				do_action( 'woocommerce_before_single_variation' );

				/**
				 * woocommerce_single_variation hook. Used to output the cart button and placeholder for variation data.
				 * @since 2.4.0
				 * @hooked woocommerce_single_variation - 10 Empty div for variation data.
				 * @hooked woocommerce_single_variation_add_to_cart_button - 20 Qty and cart button.
				 */
				do_action( 'woocommerce_single_variation' );

				/**
				 * woocommerce_after_single_variation Hook.
				 */
				do_action( 'woocommerce_after_single_variation' );
			?>
		</div>

		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
				</li>
      </div>
      <div class="check room-check-1" align="center">
		<i class="bc-icon check-icon green"></i>
	</div>
    </div>
  </div>
  </div>
			</ul>
		</div>


		<div id="add_sample-popup" style="display:none">
			<div class="popup-window">
				<a class="popup-close" href="#">X</a>
				<div class="popup-title">ACM - Brooklyn Boulder</div>
				<div class="popup-description">100% Polyester with White Backing<br>Textured Weave</div>
				<div class="popup-image"></div>
				<div class="popup-ajax-result"></div>
				<div class="popup-controls">
					<a href="#" class="popup-order-btn">Order sample</a>
					<a href="#" class="popup-select-btn">Select</a>
					<div class="clear"></div>
				</div>
				<div class="popup-ajax-loader"></div>
				<div class="clear"></div>
			</div>
		</div>
	<?php endif; ?>

	<?php do_action( 'woocommerce_after_variations_form' ); ?>
</form>
<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

<?php

endif; // End If Not Ready Made

?>
