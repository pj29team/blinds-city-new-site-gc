<?php
/**
 * Single product short description
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/short-description.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $product;

if ( ! $post->post_excerpt ) {
	return;
}
$manweek = rwmb_meta( 'bcmeta_manufacture_weeks' );
$del = rwmb_meta( 'bcmeta_delivery' );
$manwar = rwmb_meta( 'bcmeta_manufacture_warranty' );

?>
<div itemprop="description" class="short-description">
	<?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt ) ?>
</div>

<div class="social-share">
	<a class="icon-facebook" rel="nofollow" title="Share on Facebook"
		href="http://www.facebook.com/"
		onclick="popUp=window.open(
			'http://www.facebook.com/sharer.php?u=<?php echo get_permalink(); ?>',
			'popupwindow',
			'scrollbars=yes,width=800,height=400');
		popUp.focus();
		return false">
		<i class="bc-icon fb-share-icon"></i>
	</a>
	<a class="icon-pinterest" rel="nofollow" title="Save on Pinterest"
		href="http://www.pinterest.com/"
		onclick="popUp=window.open(
			'http://pinterest.com/pin/create/button/?url=<?php echo get_bloginfo('url'); ?>&amp;media=<?php echo get_the_post_thumbnail_url( $post->ID, 'full' );?>&amp;description=<?php echo get_the_title($post->ID);?>',
			'popupwindow',
			'scrollbars=yes,width=800,height=400');
		popUp.focus();
		return false">
		<i class="bc-icon pin-share-icon"></i>
	</a>
</div>

<?php if(has_term('fixed-product','product_cat',$post)) {?>

	<form class="cart" method="post" enctype='multipart/form-data'>

		<div class="fixed-product-price">
			<span><?php echo woocommerce_price($product->get_price_including_tax()); ?></span>
		</div>


	 	<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

	 	<div class="quantity-container">

		 	<?php
		 		if ( ! $product->is_sold_individually() ) {
		 			echo '<span class="quantity-label">Quantity:</span>';
		 			woocommerce_quantity_input( array(
		 				'min_value'   => apply_filters( 'woocommerce_quantity_input_min', 1, $product ),
		 				'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->backorders_allowed() ? '' : $product->get_stock_quantity(), $product ),
		 				'input_value' => ( isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : 1 )
		 			) );
		 		}

		 		
		 	?>

	 	</div>

	 	

	 	<input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->id ); ?>" />


		<div class="custom-order-button">
			<button class="button alt ajax_add_to_cart add_to_cart_button product_type_simple" type="submit" data-quantity="1" data-product_id="<?php echo $product->id; ?>">
				<i class="glyphicon glyphicon-shopping-cart"></i>
				<span>ADD TO CART</span>
			</button>
		</div>

		<?php do_action( 'woocommerce_after_add_to_cart_button' );

		?>

		<div class="zippay simple-zippay">
			<div class="bc-icon zip-icon white pull-left"></div>
			<div class="zippay-desc">
				<p>Shop now, pay later with 12 months interest free</p>
				<!-- <a href="#">Click to know how</a> -->
			</div>
		</div>
		
	</form>

	<div class="product-more-info simple">
		<div class="more-info">
			<ul>
				<li>
					<div class="bc-icon manufacture-icon green div-center"></div>
					<p>Manufacture : <?php echo (empty($manweek)) ? '2-3 weeks' : $manweek;?></p>
					<p>Delivery: <?php echo (empty($del)) ? '1-3 days' : $del;?></p>
				</li>
				<li class="border"></li>
				<li>
					<div class="bc-icon warranty-icon green div-center"></div>
					<p>include</p>
					<p><?php echo (empty($manvar)) ? '5 years' : $manwar;?> Warranty</p>				
				</li>
			</ul>
		</div>
	</div>
	
<?php } ?>


<?php /* sample push */ ?>
