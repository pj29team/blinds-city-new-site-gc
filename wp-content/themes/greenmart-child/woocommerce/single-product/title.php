<?php
/**
 * Single Product title
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/title.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author     WooThemes
 * @package    WooCommerce/Templates
 * @version    1.6.4
 */

global $product;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

// the_title( '<h1 class="product_title entry-title">', '</h1>' );
$prod_title = get_the_title();
$prod_type = '';
if( $product->is_type( 'variable' ) ) {
	if ( strpos($prod_title, 'DIY Custom') !== false ){
		if ( strpos($prod_title, 'Express') !== false ){
			$prod_type = 'DIY Express Custom';
		} else {
			$prod_type = 'DIY Custom';
		}

	} elseif ( strpos($prod_title, 'DIY') !== false ) {
		if ( (strpos($prod_title, 'Express') === false || strpos($prod_title, 'Custom') === false) && strpos($prod_title, 'Motorised') === false){
			$prod_type = 'DIY';
		} elseif ( strpos($prod_title, 'Express') !== false && strpos($prod_title, 'Custom') !== false && strpos($prod_title, '(') === false ) {
			$prod_type = 'DIY Express Custom';
		} elseif ( strpos($prod_title, '( DIY') !== false && strpos($prod_title, 'Express Custom') !== false ) {
			$prod_type = 'Express Custom';
		} elseif ( strpos($prod_title, 'Motorised') ) {
			$prod_type = 'DIY Motorised';
		}
	} elseif ( strpos($prod_title, 'Express Custom') !== false ) {
		if ( strpos($prod_title, 'DIY') === false  ) {
			$prod_type = 'Express Custom';
		} else {
			if ( strpos($prod_title, '( DIY') === false ) {
				$prod_type = 'DIY Express Custom';
			} else {
				$prod_type = 'Express Custom';
			}
		} 
	} elseif ( strpos($prod_title, 'Custom Made') !== false ) {
		if ( strpos($prod_title, 'DIY') === false ) {
			$prod_type = 'Custom Made';
		} else { 
			$prod_type = 'DIY Custom Made';
		}
	} elseif ( strpos($prod_title, 'Custom Printed') !== false ) {
		$prod_type = 'Custom Printed';
	} elseif ( strpos($prod_title, 'Fully Installed') !== false ) {
		if ( strpos($prod_title, '(') === false ) {
			$prod_type = 'Fully Installed';
		} else { 
			$prod_type = '';
		}
	} else {
		$prod_type = '';
	}
	$prod_title = explode($prod_type, $prod_title);
?>

	<?php if ( $prod_type != '') { ?>
	<h1 class="product_title entry-title"><?php echo $prod_type; ?><br><?php echo $prod_title[1]; ?></h1>
	<?php } else {
		the_title( '<h1 class="product_title entry-title">', '</h1>' );
	} ?>
<?php } else {
	the_title( '<h1 class="product_title entry-title">', '</h1>' );
}