<?php
/**
 * Checkout Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout
if ( ! $checkout->enable_signup && ! $checkout->enable_guest_checkout && ! is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', esc_html__( 'You must be logged in to checkout.', 'greenmart' ) );
	return;
}

// filter hook for include new pages inside the payment method
$get_checkout_url = apply_filters( 'woocommerce_get_checkout_url', wc_get_checkout_url() ); ?>

 <ul class="checkout-nav nav nav-pills" role="tablist">
 	<li role="presentation">
 		<a href="<?php bloginfo('url'); ?>/cart" >
 			<span class="check-list hidden-lg hidden-md"">1</span>
 			<span class="hidden-sm hidden-xs">Cart</span>
 			<span class="hidden-lg hidden-md">Confirm Cart</span>
 		</a>
 	</li>
 	<li class="nav-caret hidden-sm hidden-xs">></li>
    <li role="presentation" class="active">
    	<a href="#cust-info" aria-controls="cust-info" role="tab" data-toggle="tab">
    		<span class="check-list hidden-lg hidden-md"">2</span>
    		<span class="hidden-sm hidden-xs">Customer's Information</span>
    		<span class="hidden-lg hidden-md">Delivery</span>
    	</a>
    </li>
    <li class="nav-caret hidden-sm hidden-xs">></li>
    <li role="presentation">
    	<a href="#pay-method" aria-controls="pay-method" role="tab" data-toggle="tab">
    		<span class="check-list hidden-lg hidden-md"">3</span>
    		<span class="hidden-sm hidden-xs">Payment Method</span>
    		<span class="hidden-lg hidden-md">Payment</span>
    	</a>
    </li>
  </ul>

<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( $get_checkout_url ); ?>" enctype="multipart/form-data">

<div class="tab-content clearfix row">
	<div role="tabpanel" id="cust-info" class="tab-pane fade in active details-check col-md-12 col-lg-12 col-sm-12">
	<?php if ( sizeof( $checkout->checkout_fields ) > 0 ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div id="customer_details">
                <?php do_action( 'woocommerce_checkout_shipping' ); ?>
		</div>

		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

		<div class="cont-btn">
			<div class="pull-left hidden-sm hidden-xs">
				<a href="<?php bloginfo('url'); ?>/cart" ><&nbsp;&nbsp;&nbsp;Back to Cart</a>
			</div>
			<div class="back pull-left hidden-lg hidden-md">
				<a href="<?php bloginfo('url'); ?>/cart" ><&nbsp;&nbsp;&nbsp;BACK</a>
			</div>
			<div class="pull-right hidden-sm hidden-xs">
				<a href="#" class="button cont-payment">CONTINUE TO PAYMENT</a>
			</div>
			<div class="cont pull-right hidden-lg hidden-md">
				<a href="#" class="button cont-payment">CONTINUE&nbsp;&nbsp;&nbsp;></a>
			</div>
		</div>

	<?php endif; ?>
	</div>
	<div role="tabpanel" id="pay-method" class="tab-pane fade details-review col-md-12 col-lg-12 col-sm-12">
		<div class="order-review">
			<h3><?php esc_html_e( 'Shipping Address', 'greenmart' ); ?></h3>
            <p class="form-row " id="complete_shipping_address_field" data-priority="1">
                <input type="text" class="input-text " id="complete_shipping_address" value="test value" disabled="disabled"><a href="#cust-info" aria-controls="cust-info" role="tab" data-toggle="tab" class="edit-shipping">edit</a>
            </p>
			<h3 id="order_review_heading"><?php esc_html_e( 'Payment Method', 'greenmart' ); ?></h3>
            <p class="payment-reminder">All transaction are secure and encrypted.</p>
			<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

			<div id="order_review" class="woocommerce-checkout-review-order">
				<?php do_action( 'woocommerce_checkout_order_review' ); ?>
			</div>
            <h3><?php _e( 'Billing Address', 'woocommerce' ); ?></h3>
            <div id="billing_choose">
                <ul class="wc_payment_methods billing_ul methods">
                    <li class="wc_payment_method payment_method_bacs">
                        <label class="bill_choose_container">Same as shipping address
                            <input id="same_as_shipping" type="radio" class="input-radio choose_billing" name="choose_billing" value="same_as_shipping">
                            <span class="checkmark"></span>
                        </label>
                    </li>
                    <li class="wc_payment_method payment_method_bacs">
                        <label class="bill_choose_container">Use a different billing address
                            <input id="use_diff_billing" type="radio" class="input-radio choose_billing"  name="choose_billing" value="use_diff_billing">
                            <span class="checkmark"></span>
                        </label>
                    </li>
                </ul>
            </div>
            <div class="woocommerce_checkout_billing_wp hide">
            <?php do_action( 'woocommerce_checkout_billing' ); ?>
            </div>
			<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
		</div>

		<div class="cont-btn">
			<div class="pull-left hidden-sm hidden-xs">
				<a class="back-cust" href="#" ><&nbsp;&nbsp;&nbsp;Back to Customer's Information</a>
			</div>
			<div class="back pull-left hidden-lg hidden-md">
				<a class="back-cust" href="#" ><&nbsp;&nbsp;&nbsp;BACK</a>
			</div>
			<div class="pull-right hidden-sm hidden-xs">
				<a href="#" class="button place-your-order">PLACE YOUR ORDER</a>
			</div>
			<div class="cont pull-right hidden-lg hidden-md">
				<a href="#" class="button place-your-order">PLACE ORDER&nbsp;&nbsp;&nbsp;></a>
			</div>
		</div>

	</div>	

</div>

	

</form>

<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('body').trigger('update_checkout');
        jQuery(document).on('click', '.cont-payment', function(e){
            jQuery('.nav-pills li a[href="#pay-method"]').trigger('click');
            e.preventDefault();
            var fr = jQuery('form.woocommerce-checkout');
            if (fr.length){
                jQuery('html, body').animate({
                    scrollTop: fr.offset().top
                }, 1500);
            }
        });
        var shipping_address_1 = "";
        var shipping_address_2 = "";
        var shipping_city = "";
        var shipping_state = "";
        var shipping_postcode = "";

        jQuery(document).on('click', 'a[href="#pay-method"]', function(e){
            shipping_first_name = jQuery('#shipping_first_name').val();
            shipping_last_name = jQuery('#shipping_last_name').val();
            shipping_email = jQuery('#shipping_email').val();
            shipping_address_1 = jQuery('#shipping_address_1').val();
            shipping_address_2 = jQuery('#shipping_address_2').val();
            shipping_city = jQuery('#shipping_city').val();
            shipping_state = jQuery('#shipping_state').val();
            shipping_postcode = jQuery('#shipping_postcode').val();
            jQuery('#complete_shipping_address').val(shipping_address_1 + " " + shipping_address_2 + " " + shipping_city + " " + shipping_state + ", " + shipping_postcode);
        });
        jQuery(document).on('click', '.choose_billing', function(e){
            console.log(shipping_address_1);
            var _me = jQuery(this);
            var bf_wp = jQuery('.woocommerce-billing-fields');
            jQuery.each(bf_wp.find('input'), function (i,v) {
                if( jQuery(this).data('val') == "" || jQuery(this).data('val') == null ) {
                    jQuery(this).attr('data-val' , jQuery(this).val());
                }
           });
            var billing_wp = jQuery('.woocommerce_checkout_billing_wp');
            if(_me.val() == "use_diff_billing") {
                jQuery.each(bf_wp.find('input'), function (i,v) {
                    jQuery(this).val(jQuery(this).data('val'));
                });
                billing_wp.removeClass('hide');
            } else {
                jQuery('#billing_first_name').val(shipping_first_name);
                jQuery('#billing_last_name').val(shipping_last_name);
                jQuery('#billing_email').val(shipping_email);
                jQuery('#billing_address_1').val(shipping_address_1);
                jQuery('#billing_address_2').val(shipping_address_2);
                jQuery('#billing_city').val(shipping_city);
                jQuery('#billing_state').val(shipping_state);
                jQuery('#billing_postcode').val(shipping_postcode);
                billing_wp.addClass('hide');
            }
        });

        jQuery(document).on('click', '.place-your-order', function(e){
            jQuery('#place_order').trigger('click');
            e.preventDefault();
        });
        jQuery(document).on('click', '.back-cust', function(e){
            jQuery('.nav-pills li a[href="#cust-info"]').trigger('click');
            e.preventDefault();
        });
    });
</script>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
