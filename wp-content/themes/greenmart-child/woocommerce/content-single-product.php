<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$styles  = isset($_GET['style_single_product']) ? 'style-'.$_GET['style_single_product'] : 'style-'.greenmart_tbay_get_config('style_single_product','horizontal');

global $product, $post;
?>
<div id="product-<?php the_ID(); ?>" <?php post_class($styles); ?>>
	<div class="row">
<?php
	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked wc_print_notices - 10
	 */

	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>
</div>


<div class="row">
	<div class="image-mains">
		<?php
			/**
			 * woocommerce_before_single_product_summary hook
			 *
			 * @hooked woocommerce_show_product_sale_flash - 10
			 * @hooked woocommerce_show_product_images - 20
			 */
			do_action( 'woocommerce_before_single_product_summary' );
		?>
	</div>
	<div class="information">
	<div class="summary entry-summary <?php if( $product->is_type( 'simple' ) ) echo 'simple' ;?>">

		<?php
			/**
			 * woocommerce_single_product_summary hook
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 */
			do_action( 'woocommerce_single_product_summary' );

		?>
		<div style="clear:both;"></div>

		<div class="zippay <?php if( $product->is_type( 'simple' ) ) echo 'simple' ;?>">
			<div class="bc-icon zip-icon"></div>
			<div class="zippay-desc">
				<p>Shop now, pay later with 12 months interest free</p>
				<a href="#">Click to know how</a>
			</div>
		</div>
		<?php if( $product->is_type( 'variable' ) ) : ?>
		<div class="custom-order-button">
			<a class="button">
				<i class="bc-icon cart-icon white"></i>
				<span>CUSTOMIZE &amp; ORDER</span>
			</a>
		</div>

		<?php endif; ?>

	</div><!-- .summary -->
	</div>
</div>

	<meta itemprop="url" content="<?php the_permalink(); ?>" />
<?php if( $product->is_type( 'variable' ) ) : ?>
<div class="product-info-tabs">
	<div class="row">
		<ul>
			<li class="info-1"><a href="#" class="tab-overview">Overview <i class="bc-icon angle-down-icon"></i></a></li>
			<li class="info-2"><a href="#" class="tab-specifications">Specifications <i class="bc-icon angle-down-icon"></i></a></li>
			<li class="info-3"><a href="#" class="tab-warranty">Warranty <i class="bc-icon angle-down-icon"></i></a></li>
		</ul>
	</div>
</div>
<div id="product-content">
	<?php echo do_shortcode(get_the_content()); ?>
</div>
<?php endif; ?>


</div><!-- #product-<?php the_ID(); ?> -->
<!-- Modal -->
<!-- <div class="modal fade" id="product-content" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body">
        
      </div>
    </div><!-- /.modal-content
  </div> -->
<?php if( $product->is_type( 'variable' ) ) : ?>
<script type="text/javascript">
	function ows_click(classname,i){
		var clk = jQuery(".product-info-tabs li a." + classname +"");
		if (clk.hasClass('active')){
				clk.removeClass('active');
				jQuery('#product-content').slideUp('400');
			} else {
				clk.parent().parent().find('li a').removeClass('active');
				jQuery('#product-content .wpb_accordion_section').each(function(index){
					if (index == i) jQuery(this).find('h3').trigger('click');
				});
				jQuery('#product-content').slideDown('400');
				clk.addClass('active');
			}
	}
	jQuery(document).ready(function($){
		$('.product-info-tabs li a.tab-overview').click(function(e){
			ows_click('tab-overview', 0);
			e.preventDefault();
		});
		$('.product-info-tabs li a.tab-specifications').click(function(e){
			ows_click('tab-specifications', 1);
			e.preventDefault();
		});
		$('.product-info-tabs li a.tab-warranty').click(function(e){
			ows_click('tab-warranty', 2);
			e.preventDefault();
		});
		var atc_waypoint = new Waypoint({
		  element: document.getElementById('customize-order'),
		  handler: function() {
		    jQuery('#product-add-to-cart').show();
		  },
		  offset: 150
		})
		//mobile close
		var ows_sec = jQuery('#product-content .wpb_accordion_section');
		ows_sec.find('h3').removeClass('ui-accordion-header-active');
		ows_sec.find('h3').removeClass('ui-state-active');
		ows_sec.find('h3').next().removeClass('ui-accordion-content-active');
		ows_sec.find('h3').next().hide();
		ows_sec.find('h3 a').on('click', function (e){
			if ( jQuery(this).parent().hasClass('ui-accordion-header-active') ) {
				jQuery(this).parent().next().slideUp('400');
				jQuery(this).parent().removeClass('ui-accordion-header-active');
				jQuery(this).parent().removeClass('ui-state-active');
				jQuery(this).parent().next().removeClass('ui-accordion-content-active');
			} else {
				jQuery(this).parent().next().slideDown('400');
				jQuery(this).parent().addClass('ui-accordion-header-active');
				jQuery(this).parent().addClass('ui-state-active');
				jQuery(this).parent().next().addClass('ui-accordion-content-active');
			}
			e.preventDefault();
		});	
		var atc_hide = new Waypoint({
		  element: document.getElementById('content'),
		  handler: function() {
		    jQuery('#product-add-to-cart').hide();
		  }
		})
	});
</script>
<?php endif; ?>
<?php do_action( 'woocommerce_after_single_product' ); ?>
