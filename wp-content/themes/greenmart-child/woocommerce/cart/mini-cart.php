<?php
/**
 * Mini-cart
 *
 * Contains the markup for the mini-cart, used by the cart widget
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.2.0
 */

if(! defined('ABSPATH')) exit; // Exit if accessed directly

global $woocommerce;
?>

<?php do_action('woocommerce_before_mini_cart'); ?>
<div class="mini_cart_content">
	<div class="mini_cart_inner">
		<div class="mcart-border">
			<?php if(sizeof(WC()->cart->get_cart()) > 0) : ?>
				<ul class="cart_list product_list_widget <?php echo esc_attr($args['list_class']); ?>">
					<?php
					foreach(WC()->cart->get_cart() as $cart_item_key => $cart_item) {
						$_product     = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
						$product_id   = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);

						if($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key)) {

							$product_name      = apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key );
							$thumbnail     = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key);
							$product_price = apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key);

							?>
							<li id="mcitem-<?php echo esc_attr($cart_item_key); ?>">
								<div class="product-details">
									<?php echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
									    '<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s" data-cart_item_key="%s">x</a>',
									    esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
									    esc_html__( 'Remove this item', 'greenmart' ),
									    esc_attr( $product_id ),
									    esc_attr( $_product->get_sku() ),
									    esc_attr( $cart_item_key )
									), $cart_item_key ); 
									?>
									<?php 
									$prod_title = $product_name;
									$prod_type = '';
									if ( strpos($prod_title, 'DIY Custom') !== false ){
										if ( strpos($prod_title, 'Express') !== false ){
											$prod_type = 'DIY Express Custom';
										} else {
											$prod_type = 'DIY Custom';
										}

									} elseif ( strpos($prod_title, 'DIY') !== false ) {
										if ( (strpos($prod_title, 'Express') === false || strpos($prod_title, 'Custom') === false) && strpos($prod_title, 'Motorised') === false){
											$prod_type = 'DIY';
										} elseif ( strpos($prod_title, 'Express') !== false && strpos($prod_title, 'Custom') !== false && strpos($prod_title, '(') === false ) {
											$prod_type = 'DIY Express Custom';
										} elseif ( strpos($prod_title, '( DIY') !== false && strpos($prod_title, 'Express Custom') !== false ) {
											$prod_type = 'Express Custom';
										} elseif ( strpos($prod_title, 'Motorised') ) 	{
											$prod_type = 'DIY Motorised';
										}
									} elseif ( strpos($prod_title, 'Express Custom') !== false ) {
										if ( strpos($prod_title, 'DIY') === false  ) {
											$prod_type = 'Express Custom';
										} else {
											if ( strpos($prod_title, '( DIY') === false ) {
												$prod_type = 'DIY Express Custom';
											} else {
												$prod_type = 'Express Custom';
											}
										} 
									} elseif ( strpos($prod_title, 'Custom Made') !== false ) {
										if ( strpos($prod_title, 'DIY') === false ) {
											$prod_type = 'Custom Made';
										} else { 
											$prod_type = 'DIY Custom Made';
										}
									} elseif ( strpos($prod_title, 'Custom Printed') !== false ) {
										$prod_type = 'Custom Printed';
									} elseif ( strpos($prod_title, 'Fully Installed') !== false ) {
										if ( strpos($prod_title, '(') === false ) {
											$prod_type = 'Fully Installed';
										} else { 
											$prod_type = '';
										}
									} else {
										$prod_type = '';
									}
									$prod_title = explode($prod_type, $prod_title);
									?>
									<a class="product-name" href="<?php echo esc_url(get_permalink($product_id)); ?>">
										<span class="prod-type"><?php echo $prod_type;?></span>
										<span class="prod-name"><?php echo $prod_title[1];?></span>
									</a>
									
									<span class="quantity" style="display:none;">
										<?php esc_html_e('Qty', 'greenmart'); ?>: <?php echo apply_filters('woocommerce_widget_cart_item_quantity',  sprintf('%s', $cart_item['quantity']) , $cart_item, $cart_item_key); ?>
									</span>
									<div style="display:none;">
										<?php echo WC()->cart->get_item_data($cart_item); ?>
										<?php echo apply_filters('woocommerce_widget_cart_item_quantity',  sprintf('%s', $product_price) , $cart_item, $cart_item_key); ?>
									</div>
								</div>
							</li>
							<?php
						}
					}
					?>
				</ul><!-- end product list -->
				<script type="text/javascript">
					jQuery(document).ready(function(){
						jQuery('.mini_cart_inner ul li').each(function(index, value){
							var w = jQuery(this).find('dd.variation-Width').text();
							var h = jQuery(this).find('dd.variation-Height').text();
							if (w != '' || h != '') jQuery(this).find('span.prod-name').append('<span class="item-wh">'+w+' x '+h+'</span>');
						});
					});
				</script>
			<?php else: ?>
				<ul class="cart_empty <?php echo esc_attr($args['list_class']); ?>">
					<li><?php esc_html_e('You have no items in your shopping cart', 'greenmart'); ?></li>
				</ul>
			<?php endif; ?>

			<?php if(sizeof(WC()->cart->get_cart()) > 0) : ?>

				<p class="total">TOTAL: <?php echo WC()->cart->get_cart_subtotal(); ?></p>

				<?php do_action('woocommerce_widget_shopping_cart_before_buttons'); ?>

				<p class="buttons">
					<a href="<?php echo esc_url( wc_get_cart_url() ); ?>" class="button wc-forward view-cart"><?php esc_html_e('View Cart', 'greenmart'); ?></a>
					<a href="<?php echo esc_url( wc_get_checkout_url() );?>" class="button checkout wc-forward"><?php esc_html_e('Checkout', 'greenmart'); ?></a>	
				</p>

			<?php endif; ?>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<?php do_action('woocommerce_after_mini_cart'); ?>