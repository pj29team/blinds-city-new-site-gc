<?php
/**
 * Cart Page
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.3.8
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

wc_print_notices();

do_action( 'woocommerce_before_cart' );

$edit_cart_item = (isset($_POST['edit_cart_item']) && $_POST['edit_cart_item'])?$_POST['edit_cart_item']:0;
$edit_cart_new_product = (isset($_POST['edit_cart_new_product']) && $_POST['edit_cart_new_product'])?$_POST['edit_cart_new_product']:0;
?>

<div id="cart-content-loader"><div></div></div>

<?php if($edit_cart_item): ?>
<div class="singular-shop edit-cart-page" id="cart-custom-product-container">
	<?php
        $edit_prod_id = $edit_cart_new_product;
        if(!$edit_prod_id){
            foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                if ($edit_cart_item == $cart_item_key) {
                    $edit_prod_id = $cart_item['product_id'];
                    $selected_prod_key = $cart_item['key'];
                    break;
		}
            }
        }

        echo do_shortcode('[cart_custom_product id="'.$edit_prod_id.'"]');
	?>
</div>
<?php endif;?>

<?php
if($edit_cart_item) {echo "<div style='display:none;'>";} // hide cart if edit mode is on. BEGIN
?>
<?php if($edit_cart_item) : ?>

    <script type="text/javascript">
        function ows_click(classname,i){
            var clk = jQuery(".product-info-tabs li a." + classname +"");
            if (clk.hasClass('active')){
                clk.removeClass('active');
                jQuery('#product-content').slideUp('400');
            } else {
                clk.parent().parent().find('li a').removeClass('active');
                jQuery('#product-content .wpb_accordion_section').each(function(index){
                    if (index == i) jQuery(this).find('h3').trigger('click');
                });
                jQuery('#product-content').slideDown('400');
                clk.addClass('active');
            }
        }
        jQuery(document).ready(function($){
            var prod_wp = $('.shop_table.cart').find('tr[data-prod-id="<?=$selected_prod_key?>"]').find('.product-name');
            var prod_name = prod_wp.find('dd.variation-Roomofinstall').find('p').text();
            var width = prod_wp.find('dd.variation-Width').find('p').text().slice(0, -2);
            var height = prod_wp.find('dd.variation-Height').find('p').text().slice(0, -2);

            $('#room-name-1').val(prod_name);
            $('#room-name-1').trigger('change');

            $('#room-width-1').val($.trim(width));
            $('#room-width-1').trigger('change');


            $('#room-height-1').val($.trim(height));
            $('#room-height-1').trigger('change');

            /*script for blind type and fabric script*/

            //set the value of blind filter , material
            var pa_filter_val = prod_wp.find('dd.variation-Filter').find('p').text();
            var pa_select = $('#pa_filter');
            var pa_picker = $('#picker_pa_filter');
            var pa_select_option = $("#pa_filter option");
            if(pa_filter_val == "") {
                var pa_filter_val = prod_wp.find('dd.variation-Material').find('p').text();
                var pa_select = $('#pa_material');
                var pa_select_option = $("#pa_material option");
                var pa_picker = $('#picker_pa_material');
            }
            console.log('pa_filter_val = ' + pa_filter_val);

            if(pa_filter_val != "") {
                pa_select_option.filter(function() {
                    //may want to use $.trim in here
                    return $(this).text() == pa_filter_val;
                }).prop('selected', true);
                pa_filter_val_selected = pa_select.val();

                pa_picker.find('.select-option.swatch-wrapper[data-value="'+pa_filter_val_selected+'"]').addClass('selected');
                pa_picker.find('.select-option.swatch-wrapper[data-value="'+pa_filter_val_selected+'"]').addClass('done-select');
                pa_picker.find('.select-option.swatch-wrapper[data-value="'+pa_filter_val_selected+'"]').addClass('show');
                setTimeout(function(){
                    pa_select.trigger('change');
                }, 300);
            }

            var pa_fabric_val = prod_wp.find('dd.variation-Fabric').find('p').text();
            var pa_fabric_select = $("#pa_fabric");
            var pa_fabric_select_option = $("#pa_fabric option");
            var pa_fabric_picker = $("#picker_pa_fabric");

            if(pa_fabric_val == "") {
                var pa_fabric_val = prod_wp.find('dd.variation-Size').find('p').text();
                var pa_fabric_select = $("#pa_size");
                var pa_fabric_select_option = $("#pa_size option");
                var pa_fabric_picker = $("#picker_pa_size");
            }
            console.log('pa_fabric_val = ' + pa_fabric_val);

            if(pa_fabric_val != "") {
                pa_fabric_select_option.filter(function() {
                    //may want to use $.trim in here
                    return $(this).text() == pa_fabric_val;
                }).prop('selected', true);

                pa_fabric_picker.find('.swatch-filter').find('a').filter(function() {
                    if($(this).text() === pa_fabric_val) {
                        console.log('equal 1');
                        $(this).closest('.swatch-filter').addClass('selected');
                        $(this).closest('.swatch-filter').addClass('done-select');
                    }
                });
            }
            var colour_type = "";
            var color_select = "";
            var pa_rb_colour = prod_wp.find('dd.variation-RollerBlindsColour').find('p').text();
            colour_type = $("#pa_roller-blinds-colour option");
            color_select = $("#pa_roller-blinds-colour");
            color_picker = $("#picker_pa_roller-blinds-colour");
            if(pa_rb_colour == "") {
                //Awning Color
                var pa_rb_colour = prod_wp.find('dd.variation-AwningsColor').find('p').text();
                colour_type = $("#pa_awnings-color option");
                color_select = $("#pa_awnings-color");
                color_picker = $("#picker_pa_awnings-color");

                if(pa_rb_colour == "") {
                    //Vertical Blinds Color
                    var pa_rb_colour = prod_wp.find('dd.variation-VerticalBlindsColour').find('p').text();
                    colour_type = $("#pa_vertical-blinds-colour option");
                    color_select = $("#pa_vertical-blinds-colour");
                    color_picker = $("#picker_pa_vertical-blinds-colour");

                    if(pa_rb_colour == "") {
                        //Roman Blinds Color
                        var pa_rb_colour = prod_wp.find('dd.variation-RomanBlindsColour').find('p').text();
                        colour_type = $("#pa_roman-blinds-colour option");
                        color_select = $("#pa_roman-blinds-colour");
                        color_picker = $("#picker_pa_roman-blinds-colour");
                        if(pa_rb_colour == "") {
                            //Plantation Color
                            var pa_rb_colour = prod_wp.find('dd.variation-PlantationColour').find('p').text();
                            colour_type = $("#pa_plantation-colour option");
                            color_select = $("#pa_plantation-colour");
                            color_picker = $("#picker_pa_plantation-colour");

                            if(pa_rb_colour == "") {
                                //honey comb colour
                                var pa_rb_colour = prod_wp.find('dd.variation-HoneycombColour').find('p').text();
                                colour_type = $("#pa_honeycomb-colour option");
                                color_select = $("#pa_honeycomb-colour");
                                color_picker = $("#picker_pa_honeycomb-colour");

                                if(pa_rb_colour == "") {
                                    //venetian colour
                                    var pa_rb_colour = prod_wp.find('dd.variation-VenetianColours').find('p').text();
                                    colour_type = $("#pa_venetian-colours option");
                                    color_select = $("#pa_venetian-colours");
                                    color_picker = $("#picker_pa_venetian-colours");
                                }

                            }


                        }

                    }
                }

            }
            console.log(pa_rb_colour);
            if(pa_rb_colour != "") {
                var to_select = pa_rb_colour;
                //set selector
                colour_type.filter(function() {
                    //may want to use $.trim in here
                    return $(this).text() == to_select;
                }).prop('selected', true);
                var pa_rb_colour_val = color_select.val();

                console.log('check current');
                console.log(pa_rb_colour_val);
                console.log(color_picker.find('div.select-option.swatch-wrapper[data-value="'+pa_rb_colour_val+'"]').find('a').length);
                /*selecting blinds color*/

                var pa_rb_colour_interval = setInterval(function () {

                    var check_pa_rb_colour_val = color_picker.find('div.select-option.swatch-wrapper[data-value="'+pa_rb_colour_val+'"]').find('a').length;
                    if(check_pa_rb_colour_val) {
                        if(!color_picker.find('div.select-option.swatch-wrapper[data-value="'+pa_rb_colour_val+'"]').hasClass('selected')) {
                            color_picker.find('div.select-option.swatch-wrapper[data-value="'+pa_rb_colour_val+'"]').find('a').trigger('click');
                            $("#add_sample-popup").hide();
                            console.log('it triggered');
                            if(!$('#offers-for-woocommerce-add-to-cart-wrap').is(':visible')) {
//                                    $('#offers-for-woocommerce-add-to-cart-wrap').addClass('gf_expanded');
                                $('#offers-for-woocommerce-add-to-cart-wrap').addClass('show');
                            }
                            if($('.Honeycomb-mp').length) {
                                $('.Honeycomb-mp').find('.radio_image').find('a.selected').trigger('click');
                            }
                            if($('.Venetian-mp').length) {
                                $('.Venetian-mp').find('.radio_image').find('a.selected').trigger('click');
                            }

                            //check swatches if still invisible
                            if(!$('.select-option.swatch-wrapper.selected').is(':visible')) {
                                $.each(color_picker.find('.select-option.swatch-wrapper'),function () {
                                    $(this).removeClass('swatch-hidden');
                                });
                            }

                            //check if dynamic select doesnt effect
                            $.each($('.done-select'),function () {
                                if(!$(this).hasClass('selected')) {
                                    $(this).addClass('selected added-manualy');
                                }
                            });
                            //make sure all gfield is visible
                            $.each($('li.gfield'),function () {
                                $(this).removeClass('gfield-hidden');
                            });


                            $('.swatch-filter').show();



                        } else {
                            clearTimeout(pa_rb_colour_interval);
                        }

                    }

                }, 1000)

                /*end selecting blinds color*/

                $('.accordion-toggle').trigger('click');
            };
            /*script for blind type and fabric script*/
            $('.product-info-tabs li a.tab-overview').click(function(e){
                ows_click('tab-overview', 0);
                e.preventDefault();
            });
            $('.product-info-tabs li a.tab-specifications').click(function(e){
                ows_click('tab-specifications', 1);
                e.preventDefault();
            });
            $('.product-info-tabs li a.tab-warranty').click(function(e){
                ows_click('tab-warranty', 2);
                e.preventDefault();
            });
            var atc_waypoint = new Waypoint({
                element: document.getElementById('customize-order'),
                handler: function() {
                    console.log('here');
                    jQuery('#product-add-to-cart').show();
                },
                offset: 150
            })
            //mobile close
            var ows_sec = jQuery('#product-content .wpb_accordion_section');
            ows_sec.find('h3').removeClass('ui-accordion-header-active');
            ows_sec.find('h3').removeClass('ui-state-active');
            ows_sec.find('h3').next().removeClass('ui-accordion-content-active');
            ows_sec.find('h3').next().hide();
            ows_sec.find('h3 a').on('click', function (e){
                if ( jQuery(this).parent().hasClass('ui-accordion-header-active') ) {
                    jQuery(this).parent().next().slideUp('400');
                    jQuery(this).parent().removeClass('ui-accordion-header-active');
                    jQuery(this).parent().removeClass('ui-state-active');
                    jQuery(this).parent().next().removeClass('ui-accordion-content-active');
                } else {
                    jQuery(this).parent().next().slideDown('400');
                    jQuery(this).parent().addClass('ui-accordion-header-active');
                    jQuery(this).parent().addClass('ui-state-active');
                    jQuery(this).parent().next().addClass('ui-accordion-content-active');
                }
                e.preventDefault();
            });
            var atc_hide = new Waypoint({
                element: document.getElementById('cart-content-loader'),
                handler: function() {
                    jQuery('#product-add-to-cart').hide();
                }
            })
        });
    </script>
<?php endif; ?>
<div class="widget">

<form action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post" class="cart-form">
<input type="hidden" name="edit_cart_item" value=""/>
<input type="hidden" name="edit_cart_new_product" value=""/>
<?php do_action( 'woocommerce_before_cart_table' ); ?>

<div class="table-responsive">

<?php if (!is_checkout()): ?><h1 style="font-size: 30px;">Your Shopping Cart</h1><?php endif; ?>

<table class="shop_table shop_table_responsive cart">
	<thead>
		<tr>
			<?php if (!is_checkout()) ?><th class="product-number">&nbsp;</th>
			<th class="product-thumbnail"><?php esc_html_e( 'Product Details', 'greenmart' ); ?></th>
			<th class="product-name"><?php esc_html_e( 'Specification', 'greenmart' ); ?></th>
			<th class="product-quantity"><?php esc_html_e( 'Qty', 'greenmart' ); ?></th>
			<th class="product-subtotal"><?php esc_html_e( 'Price', 'greenmart' ); ?></th>
			<th class="product-remove">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<?php do_action( 'woocommerce_before_cart_contents' ); ?>

		<?php
		$count = 1;
		foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
			$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
			$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

			if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
				?>

				<?php if ( wp_is_mobile() ) { ?>

					<tr data-prod-id="<?= $cart_item['key'] ?>" class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item mobile', $cart_item, $cart_item_key ) ); ?>">
                        <?php
                        $has_link = false;
                        if ( ! $_product->is_visible() ) {
                            $prod_title = apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;';
                        } else {
                            $prod_title = apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s </a>', esc_url( $_product->get_permalink( $cart_item ) ), $_product->get_title() ), $cart_item, $cart_item_key );
                            $has_link = true;
                        }
                        $prod_type = '';
                        if ( strpos($prod_title, 'DIY Custom') !== false ){
							if ( strpos($prod_title, 'Express') !== false ){
								$prod_type = 'DIY Express Custom';
							} else {
								$prod_type = 'DIY Custom';
							}

						} elseif ( strpos($prod_title, 'DIY') !== false ) {
							if ( (strpos($prod_title, 'Express') === false || strpos($prod_title, 'Custom') === false) && strpos($prod_title, 'Motorised') === false){
								$prod_type = 'DIY';
							} elseif ( strpos($prod_title, 'Express') !== false && strpos($prod_title, 'Custom') !== false && strpos($prod_title, '(') === false ) {
								$prod_type = 'DIY Express Custom';
							} elseif ( strpos($prod_title, '( DIY') !== false && strpos($prod_title, 'Express Custom') !== false ) {
								$prod_type = 'Express Custom';
							} elseif ( strpos($prod_title, 'Motorised') ) 	{
								$prod_type = 'DIY Motorised';
							}
						} elseif ( strpos($prod_title, 'Express Custom') !== false ) {
							if ( strpos($prod_title, 'DIY') === false  ) {
								$prod_type = 'Express Custom';
							} else {
								if ( strpos($prod_title, '( DIY') === false ) {
									$prod_type = 'DIY Express Custom';
								} else {
									$prod_type = 'Express Custom';
								}
							} 
						} elseif ( strpos($prod_title, 'Custom Made') !== false ) {
							if ( strpos($prod_title, 'DIY') === false ) {
								$prod_type = 'Custom Made';
							} else { 
								$prod_type = 'DIY Custom Made';
							}
						} elseif ( strpos($prod_title, 'Custom Printed') !== false ) {
							$prod_type = 'Custom Printed';
						} elseif ( strpos($prod_title, 'Fully Installed') !== false ) {
							if ( strpos($prod_title, '(') === false ) {
								$prod_type = 'Fully Installed';
							} else { 
								$prod_type = '';
							}
						} else {
							$prod_type = '';
						}
						$prod_title = explode($prod_type, $prod_title);
                        ?>
                        <?php if (!is_checkout()){ ?>
                            <td class="product-info-mobile">
                                <?php
                                if ($has_link == true){
                                    echo '<a href="'.esc_url( $_product->get_permalink( $cart_item ) ).'"><span class="prod-type">'.$prod_type.'</span><br><span class="prod-title">'.$prod_title[1].'</span> </a>';
                                } else {
                                    echo $prod_type.'<br>'.$prod_title[1];
                                }

                                    // Meta data
                                    echo WC()->cart->get_item_data( $cart_item );

                                    // Backorder notification
                                    if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
                                        echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'greenmart' ) . '</p>';
                                    }
                                ?>
                                <?php
                                    echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
                                ?>
                            </td>

                            <td class="product-thumbnail-mobile">
                                <?php
                                    if ( $_product->is_sold_individually() ) {
                                        $product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
                                    } else {
                                        $product_quantity = woocommerce_quantity_input( array(
                                            'input_name'  => "cart[{$cart_item_key}][qty]",
                                            'input_value' => $cart_item['quantity'],
                                            'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
                                            'min_value'   => '0'
                                        ), $_product, false );
                                    }
                                ?>
                                <?php
                                    echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key );
                                ?>

                                <?php
                                    echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf( '<a href="%s" class="remove" title="%s"><i class="bc-icon del-icon"></i></a>', esc_url( WC()->cart->get_remove_url( $cart_item_key ) ), esc_html__( 'Remove this item', 'greenmart' ) ), $cart_item_key );
                                ?>
                                <div class="edit" style="margin-top: 15px;">
                                <a href="#" rel="<?php echo $cart_item_key; ?>" class="edit_product" ><i class="bc-icon edit-icon"></i></a>
                                </div>

                            </td>
                        <?php
                        } else { ?>
                            <td class="product-number" width="2%">
                                <?php echo "<p class='item-number'>" . $count . "</p>"; ?>
                            </td>
                            <td class="product-name">

                                <?php

                                    if ($has_link == true){
                                        echo '<a href="'.esc_url( $_product->get_permalink( $cart_item ) ).'"><span class="prod-type">'.$prod_type.'</span><br><span class="prod-title">'.$prod_title[1].'</span> </a>';
                                    } else {
                                        echo $prod_type.'<br>'.$prod_title[1];
                                    }
                                    echo WC()->cart->get_item_data( $cart_item );
                                ?>

                            </td>
                            <td class="product-quantity">
                                <div class="pqty-wrapper">
                                    <?php
                                    if ( $_product->is_sold_individually() ) {
                                        $product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
                                    } else {
                                        $product_quantity = woocommerce_quantity_input( array(
                                            'input_name'  => "cart[{$cart_item_key}][qty]",
                                            'input_value' => $cart_item['quantity'],
                                            'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
                                            'min_value'   => '0'
                                        ), $_product, false );
                                    }

                                    echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key );
                                    ?>
                                </div>
                            </td>


                            <td class="product-subtotal price">
                                <?php
                                echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
                                ?>
                            </td>
                        <?php
                        }
                        ?>
					</tr>

				<?php } else { ?>
				        <tr data-prod-id="<?= $cart_item['key'] ?>" class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">

						<?php if (!is_checkout()): ?>
							<td class="product-number">
								<?php echo $count; ?>
							</td>
						<?php endif;?>
						<td class="product-thumbnail">
							  <?php
                            $has_link = false;
                            if ( ! $_product->is_visible() ) {
                                $prod_title = apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;';
                            } else {
                                $prod_title = apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s </a>', esc_url( $_product->get_permalink( $cart_item ) ), $_product->get_title() ), $cart_item, $cart_item_key );
                                $has_link = true;
                            }
                            $prod_type = '';
                            if ( strpos($prod_title, 'DIY Custom') !== false ){
                                if ( strpos($prod_title, 'Express') !== false ){
                                    $prod_type = 'DIY Express Custom';
                                } else {
                                    $prod_type = 'DIY Custom';
                                }
                            } elseif ( strpos($prod_title, 'DIY') !== false ) {
                                if ( (strpos($prod_title, 'Express') === false || strpos($prod_title, 'Custom') === false) && strpos($prod_title, 'Motorised') === false){
                                    $prod_type = 'DIY';
                                } elseif ( strpos($prod_title, 'Express') !== false && strpos($prod_title, 'Custom') !== false ) {
									$prod_type = 'DIY Express Custom';
								} else {
									$prod_type = 'DIY Motorised';
								}
                            } elseif ( strpos($prod_title, 'Express Custom') !== false ) {
                                if ( strpos($prod_title, 'DIY') === false ) {
                                    $prod_type = 'Express Custom';
                                } else {
                                    $prod_type = 'DIY Express Custom';
                                }
                            } elseif ( strpos($prod_title, 'Custom Made') !== false ) {
                                if ( strpos($prod_title, 'DIY') === false ) {
                                    $prod_type = 'Custom Made';
                                } else {
                                    $prod_type = 'DIY Custom Made';
                                }
                            } elseif ( strpos($prod_title, 'Fully Installed') !== false ) {
                                if ( strpos($prod_title, '(') === false ) {
									$prod_type = 'Fully Installed';
								} else { 
									$prod_type = '';
								}
                            } elseif ( strpos($prod_title, 'Custom Printed') !== false ) {
                                $prod_type = 'Custom Printed';
                            } elseif ( strpos($prod_title, 'Sample') !== false ) {
                                $prod_type = 'Sample';
                            }
                            $prod_title = explode($prod_type, $prod_title);
                            if ($has_link == true){
                                echo '<a href="'.esc_url( $_product->get_permalink( $cart_item ) ).'"><span class="prod-type">'.$prod_type.'</span><br><span class="prod-title">'.$prod_title[1].'</span> </a>';
                            } else {
                                echo $prod_type.'<br>'.$prod_title[1];
                            }
                            ?>
						</td>

						<td class="product-name">

							<?php
                                if (is_checkout()){
                                    if ($has_link == true){
                                        echo '<a href="'.esc_url( $_product->get_permalink( $cart_item ) ).'"><span class="prod-type">'.$prod_type.'</span><br><span class="prod-title">'.$prod_title[1].'</span> </a>';
                                    } else {
                                        echo $prod_type.'<br>'.$prod_title[1];
                                    }
                                      echo WC()->cart->get_item_data( $cart_item );
                                } else {
                                    // Meta data
                                    echo WC()->cart->get_item_data( $cart_item );

                                    // Backorder notification
                                    if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
                                        echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'greenmart' ) . '</p>';
                                    }

                                }

							?>

						</td>

						<td class="product-quantity">
							<div class="pqty-wrapper">
							<?php
								if ( $_product->is_sold_individually() ) {
									$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
								} else {
									$product_quantity = woocommerce_quantity_input( array(
										'input_name'  => "cart[{$cart_item_key}][qty]",
										'input_value' => $cart_item['quantity'],
										'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
										'min_value'   => '0'
									), $_product, false );
								}

								echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key );
							?>
							</div>
						</td>


						<td class="product-subtotal price">
							<?php
								echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
							?>
						</td>

						<td class="product-remove">

							<?php
								echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf( '<a href="%s" class="remove" title="%s"><i class="bc-icon del-icon"></i></a>', esc_url( WC()->cart->get_remove_url( $cart_item_key ) ), esc_html__( 'Remove this item', 'greenmart' ) ), $cart_item_key );
							?>
							<div class="edit" style="margin-top: 15px;">
							<a href="#" rel="<?php echo $cart_item_key; ?>" class="edit_product" ><i class="bc-icon edit-icon"></i></a>
							</div>
						</td>

					</tr>
				<?php } ?>
			<?php
			}
			$count++;
		}

		do_action( 'woocommerce_cart_contents' );
		?>
		<tr>
			<td colspan="6" class="actions">
				<div class="clearfix">

					<?php if ( WC()->cart->coupons_enabled() && is_checkout() ) { ?>
						<div class="coupon pull-left">
							<input type="text" name="coupon_code" class="input-text " id="coupon_code" value="" placeholder="<?php esc_html_e( 'Coupon Code / Discount Code', 'greenmart' ); ?>" /> <input type="submit" class="button" name="apply_coupon" value="<?php esc_html_e( 'Apply Code', 'greenmart' ); ?>" />
							<?php do_action('woocommerce_cart_coupon'); ?>
						</div>
					<?php } ?>

					<div class="pull-right">
                        <input style="display: none;" type="submit" class="button" name="update_cart" value="<?php esc_html_e( 'Update Cart', 'greenmart' ); ?>" />
						<?php do_action( 'woocommerce_cart_actions' ); ?>
						<?php wp_nonce_field( 'woocommerce-cart' ); ?>
					</div>
				</div>
			</td>
		</tr>

		<?php do_action( 'woocommerce_after_cart_contents' ); ?>
	</tbody>
</table>

</div>

<?php do_action( 'woocommerce_after_cart_table' ); ?>

<?php if (!is_checkout()) :?>
<div class="extra-options" id="ext-op">
	<?php 
	$total = WC()->cart->cart_contents_total + WC()->cart->shipping_total;
	$yr1 = round($total * 0.15);
	$yr2 = round($total * 0.30);
	$yr3 = round($total * 0.45);
	if ($total <= 500) {
		$si = 20;
	} elseif ($total > 500 && $total <= 3000) {
		$si = 50;
	} else {
		$si = 89;
	}
	$ex_true = 0;
	$si_true = 'no';
	$ext_true_v = 0;
    foreach ( WC()->cart->get_fees() as $fee ) :
	    if ($fee->id == "extended-warranty"):
	        $ex_true = $fee->amount;
	        WC()->session->set( 'ext_w', $fee->amount);
	        if ( $ex_true == $yr1 ) {
	          $ext_true_v = '1';
	        } elseif ( $ex_true == $yr2 ){
	        	$ext_true_v = '2';
	        } elseif ( $ex_true == $yr3 ){
	        	$ext_true_v = '3';
	        }
	    endif;
	    if ($fee->id == "shipping-insurance"):
	        $si_true = $fee->amount;
	        WC()->session->set( 'ship_in', $fee->amount);
	    endif;
    endforeach;

    if ($si_true == 'no' && !empty(WC()->session->get('ship_in'))) {
    	WC()->session->set('ship_in',null);
    }
    if ($ext_true_v == '0' && !empty(WC()->session->get('ext_w'))) {
    	WC()->session->set('ext_w',null);
    }

	echo '
		<style>
			.ext-warranty input { position: relative; top: 2px; }
			.ext-warranty label { display: inline-block; margin: 10px 15px 0 5px; }
			#checkout-extra-fields .ship-insurance input { display: inline-block;position: relative; }
		</style>
	';
	echo '<div id="checkout-extra-fields">';
	echo '<div class="ship-insurance pull-left">';
	echo '<h3>Shipping Protection option</h3>';
	echo '<p>We will cover any loss or damage to your order that occurs during delivery</p>';
	woocommerce_form_field( 'ship_insurance', array(
	    'type'          => 'radio',
	    'class'         => array('ship_insurance form-row-wide'),

	    'placeholder'   => __(''),
	    'options'       => array(
	    					$si => 'Shipping Protection <span>$'.$si.'</span>',
	    					'no' => '<span class="no-ty">NO THANKS</span>',
	    				),
	    ), $si_true);
	echo '</div>';
	echo '<div class="ext-warranty pull-right">';
	echo '<h3>Extended Warranty</h3>';
	echo '<p>Purchase additional warranty for your blinds </p>';
	woocommerce_form_field( 'ext_warranty', array(
	    'type'          => 'radio',
	    'class'         => array('ext_warranty form-row-wide'),
	    'placeholder'   => __(''),
	    'options'       => array(
	    					'1' => '1 Year <span>$'.$yr1.'</span>',
	    					'2' => '2 Years <span>$'.$yr2.'</span>',
	    					'3' => '3 Years <span>$'.$yr3.'</span>',
	    					'0' => '<span class="no-ty">NO THANKS</span>',
	    				),
	    ), $ext_true_v );
	echo '</div>';
	echo '</div>';
	?>
</div>

<?php endif; ?>
</form>

</div>

<div class="cart-collaterals widget">

	<?php if ( WC()->cart->coupons_enabled() && !is_checkout() ) { ?>
		<div class="coupon pull-left">
			<input type="text" name="coupon_code" class="input-text " id="coupon_code" value="" placeholder="<?php esc_html_e( 'Coupon Code / Discount Code', 'greenmart' ); ?>" /> <input type="submit" class="button" name="apply_coupon" value="<?php esc_html_e( 'Apply Code', 'greenmart' ); ?>" />
			<?php do_action('woocommerce_cart_coupon'); ?>
		</div>
	<?php } ?>

	<?php do_action( 'woocommerce_cart_collaterals' ); ?>

</div>
<?php if (!is_checkout()): ?>
<div class="cart-buttons">
	<!-- <div class="continue-shopping">
		<a class="button cart-cont-shop" href="<?php //bloginfo('url');?>/shop">
			<?php// esc_html_e( 'CONTINUE SHOPPING', 'woocommerce' ); ?>
		</a>
	</div> -->
	<div class="proceed-to-checkout">
		 <a href="<?php echo esc_url( wc_get_checkout_url() );?>" class="button">
		 <?php esc_html_e( 'CHECKOUT', 'woocommerce' ); ?>
		 </a>
	</div>
</div>
<?php endif; ?>
<?php
if($edit_cart_item) {echo "</div>";} // hide cart if edit mode is on. END
?>

<?php do_action( 'woocommerce_after_cart' ); ?>
