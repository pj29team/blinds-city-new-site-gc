<?php
/**
 * Result Count
 *
 * Shows text: Showing x - x of x results.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/result-count.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $wp_query;

if ( ! woocommerce_products_will_display() ) {
	return;
}
?>
<div class="pull-right">
<p class="woocommerce-result-count">
	<span class="result-count">
	<?php
	$paged    = max( 1, $wp_query->get( 'paged' ) );
	$per_page = $wp_query->get( 'posts_per_page' );
	$total    = $wp_query->found_posts;
	$first    = ( $per_page * $paged ) - $per_page + 1;
	$last     = min( $total, $wp_query->get( 'posts_per_page' ) * $paged );

	if ( $total <= $per_page || -1 === $per_page ) {
		/* translators: %d: total results */
		printf( _n( 'Showing the single result', 'All %d', $total, 'woocommerce' ), $total );
	} else {
		/* translators: 1: first result 2: last result 3: total results */
		printf( _nx( 'Showing the single result', '%1$d of %2$d', $total, 'with first and last result', 'woocommerce' ), $last, $total );
	}
	?>
	</span>
	<span class="next-result"><?php
		previous_posts_link( '<i class="fa fa-angle-left" aria-hidden="true"></i>' );
		next_posts_link( '<i class="fa fa-angle-right" aria-hidden="true"></i>' );
	?></span>
</p>
</div>