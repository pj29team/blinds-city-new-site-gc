<?php
/**
 * Login Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
?>

<?php wc_print_notices(); ?>

<?php do_action( 'woocommerce_before_customer_login_form' ); ?>

<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

<div class="row" id="customer_login">

	<div id="login-form" class="col-md-6 col-sm-6 col-xs-12" <?php if ($_GET['action'] == 'register') echo 'style="display:none;"';?>>

<?php endif; ?>

		<h2><?php esc_html_e( 'Login to Blinds City', 'greenmart' ); ?></h2>
		<p class="login-note">Login to view invoices, order history and change your settings</p>
		<form method="post" class="login" role="form">

			<?php do_action( 'woocommerce_login_form_start' ); ?>

			<p class="form-group form-row form-row-wide">
				<input type="text" class="input-text form-control" name="username" id="username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" placeholder="Email Address" />
			</p>
			<p class="form-group form-row form-row-wide">
				<input class="input-text form-control" type="password" name="password" id="password" placeholder="Password" />
			</p>

			<?php do_action( 'woocommerce_login_form' ); ?>

			<p class="form-group form-row">
				<?php wp_nonce_field( 'woocommerce-login' ); ?>
				<p for="rememberme" class="inline">
					<input name="rememberme" type="checkbox" id="rememberme" value="forever" /> <?php esc_html_e( 'Remember me', 'greenmart' ); ?>
				</p>
				<input type="submit" class="button" name="login" value="<?php esc_html_e( 'Login', 'greenmart' ); ?>" />
			</p>
			<p class="form-group lost_password">
				<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php esc_html_e( 'Lost your password?', 'greenmart' ); ?></a>
			</p>

			<?php do_action( 'woocommerce_login_form_end' ); ?>

		</form>


<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

	</div>

	<div id="login-check" class="col-md-6 col-sm-6 col-xs-12 pull-right" <?php if ($_GET['action'] == 'register') echo 'style="display:none;"';?>>
		<div class="checklist">
			<ul>
				<li>Login to view your invoices</li>
				<li>View order history and order information</li>
				<li>Change your account setttings</li>
				<li>Track your order or ask your support</li>
			</ul>
		</div>
		<div class="reg-btn">
			<?php if ( wp_is_mobile() ) : ?>
				<a href="<?php bloginfo('url');?>/my-account/?action=register" id="reg-toggle" class="button trans-element">NOT A MEMBER? JOIN NOW</a>
			<?php else: ?>
				<a href="#" id="reg-toggle" class="button trans-element">NOT A MEMBER? JOIN NOW</a>
			<?php endif; ?>
		</div>
	</div>	

	<div id="customer_reg" class="col-md-6 col-sm-6 col-xs-12 pull-right" <?php if ($_GET['action'] == 'register') echo 'style="display:block !important;margin:auto;float:none!important;max-width:none;"';?>>

		<h2><?php esc_html_e( 'Register', 'greenmart' ); ?></h2>

		<form method="post" class="register widget" role="form">

			<?php do_action( 'woocommerce_register_form_start' ); ?>

			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

				<p class="form-group form-row form-row-wide">
					<input type="text" class="input-text form-control" name="username" id="reg_username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" placeholder="Username" />
				</p>

			<?php endif; ?>

			<p class="form-group form-row form-row-wide">
				<input type="email" class="input-text form-control" name="email" id="reg_email" value="<?php if ( ! empty( $_POST['email'] ) ) echo esc_attr( $_POST['email'] ); ?>" placeholder="Email Address" />
			</p>

			<p class="form-group form-row form-row-first">
				<input type="text" class="input-text" name="billing_first_name" id="reg_billing_first_name" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" placeholder="First Name" />
			</p>

			<p class="form-group form-row form-row-last">
				<input type="text" class="input-text" name="billing_last_name" id="reg_billing_last_name" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" placeholder="Last Name" />
			</p>


			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

				<p class="form-group form-row form-row-wide">
					<input type="password" class="input-text form-control" name="password" id="reg_password" placeholder="Password" />
				</p>

			<?php else: ?>
				
				<p>Password will be sent to the email above. Use valid email!</p>

			<?php endif; ?>

			<!-- Spam Trap -->
			<div style="<?php echo ( ( is_rtl() ) ? 'right' : 'left' ); ?>: -999em; position: absolute;"><label for="trap"><?php esc_html_e( 'Anti-spam', 'greenmart' ); ?></label><input type="text" name="email_2" id="trap" tabindex="-1" /></div>

			<?php do_action( 'woocommerce_register_form' ); ?>
			<?php do_action( 'register_form' ); ?>

			<p style="margin-top: 20px;text-align: center;"">By clicking REGISTER, you agree to the Blinds City <br><a href="<?php bloginfo('url');?>/terms-and-condition/"><u>Terms &amp; Conditions</u></a> and to receiving marketing communications from www.blindscity.com.au. <br>Remember, you can unsubscribe at any time.</p>

			<p class="form-group form-row">
				<?php wp_nonce_field( 'woocommerce-register' ); ?>
				<input type="submit" class="button" name="register" value="<?php esc_html_e( 'Register', 'greenmart' ); ?>" />
			</p>

			<?php do_action( 'woocommerce_register_form_end' ); ?>

		</form>
		<?php if ($_GET['action'] != 'register'): ?>

		<div class="reg-btn">
			<button id="back-toggle" class="trans-element">CANCEL</button>
		</div>

		<?php endif;?>

	</div>

</div>
<script type="text/javascript">
	jQuery(document).on('click','#reg-toggle', (function(e) {
	  jQuery('#login-check').slideUp();
	  jQuery('#customer_reg').slideDown();
	  e.preventDefault();
	}));
	jQuery(document).on('click','#back-toggle', (function(e) {
	  jQuery('#customer_reg').slideUp();
	  jQuery('#login-check').slideDown();
	  e.preventDefault();
	}));
</script>
<?php endif; ?>

<?php do_action( 'woocommerce_after_customer_login_form' ); ?>