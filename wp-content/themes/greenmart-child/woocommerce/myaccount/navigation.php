<?php
/**
 * My Account navigation
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/navigation.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_account_navigation' );
$billing_add = get_user_meta( get_current_user_id(), 'billing_address_1', true );
$shipping_add = get_user_meta( get_current_user_id(), 'shipping_address_1', true );
$current_user = wp_get_current_user();
$user_fname = $current_user->user_firstname;
$user_lname = $current_user->user_lastname;
?>

<nav class="woocommerce-MyAccount-navigation">
    <h4 class="nav-myaccount-tittle">My Account</h4>
	<ul>
		<?php foreach ( wc_get_account_menu_items() as $endpoint => $label ) : ?>
			<?php if ($label === 'Downloads' || $label === 'Logout') continue; ?>
            <?php
            $label_to_echo = "";
            $class_need_to_update = "all_good";
            if ( $label == 'Dashboard' ){
                $label_to_echo = 'My Account Dashboard';
            } else
                if ( $label == 'Orders'){
                    $label_to_echo = 'My Orders';
                } else
                    if ( $label == 'Addresses' ){
                        if ( empty($billing_add) || empty($shipping_add) ){
                            $label_to_echo = 'Update Your Billing and Shipping Address';
                            $class_need_to_update = "need_update";
                        } else {
                            $label_to_echo = esc_html( $label );
                            $class_need_to_update = "all_good";
                        }
                    } else
                        if ( $label == 'Account details'){
                            if ( empty($user_fname) || empty($user_lname) ) {
                                $label_to_echo = 'Update Your Account Details';
                                $class_need_to_update = "need_update";
                            } else {
                                $label_to_echo = esc_html( $label );
                            }
                        } else {
                            $label_to_echo = esc_html( $label );
                        }
            ?>
            </a>
			<li class="<?= $class_need_to_update; ?> <?php echo wc_get_account_menu_item_classes( $endpoint ) ; if ( ( $label == 'Addresses' ) && (empty($billing_add) || empty($shipping_add)) ) echo ' empty-field';?>">
                <div class="icon"></div>
				<a href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoint ) ); ?>">
                    <?= $label_to_echo ?>
			</li>
		<?php endforeach; ?>
		<br>
		<li class="contact-support <?php echo wc_get_account_menu_item_classes( $endpoint ) . ' is-active'; ?>">
			<a href="#">Contact our Support Team</a>
		</li>
		<li class="call-us <?php echo wc_get_account_menu_item_classes( $endpoint ) . ' is-active'; ?>">
			<a href="#">Call us on 1300 055 888</a>
		</li>
	</ul>
	<div class="act-coupon">
		<h4>$50 off Coupon Just For You</h4>
		<p>We created a secret discount just<br>
			for you. When you checkout next<br>
			time, just use the below code.
		</p>
		<div class="coupon-code">
			<span>#50bonus</span>
		</div>
	</div>
	<div class="signout-btn">
		<a id="signout" class="trans-element" href="<?php echo wc_logout_url( wc_get_page_permalink( 'myaccount' ) )?>">SIGN OUT</a>
	</div>
</nav>

<?php do_action( 'woocommerce_after_account_navigation' ); ?>
