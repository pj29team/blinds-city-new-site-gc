<?php
/**
 * Edit address form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-address.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.9
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$page_title = ( 'billing' === $load_address ) ? __( 'Billing address', 'woocommerce' ) : __( 'Shipping address', 'woocommerce' );

// get the user meta
$userMeta = get_user_meta(get_current_user_id());

// get the form fields
$countries = new WC_Countries();
$billing_fields = $countries->get_address_fields( '', 'billing_' );
$shipping_fields = $countries->get_address_fields( '', 'shipping_' );

do_action( 'woocommerce_before_edit_account_address_form' ); ?>
<?php if ( ! $load_address ) : ?>
<!-- billing form -->

    <form action="billing/" class="edit-account" method="post">
        <h2><?php echo apply_filters( 'woocommerce_my_account_edit_address_title',  __( 'Update Your Address', 'woocommerce' ) ); ?></h2>
        <p>You can update your shipping or billing address from here</p>
        <br>

        <h5><?php echo apply_filters( 'woocommerce_my_account_edit_address_title', __( 'Billing Address', 'woocommerce' ) ); ?></h5>

        <?php do_action( "woocommerce_before_edit_address_form_billing" ); ?>

        <?php foreach ( $billing_fields as $key => $field ) : ?>

            <?php woocommerce_form_field( $key, $field, $userMeta[$key][0] ); ?>

        <?php endforeach; ?>

        <?php do_action( "woocommerce_after_edit_address_form_billing" ); ?>


        <h5><?php echo apply_filters( 'woocommerce_my_account_edit_address_title',  __( 'Shipping Address', 'woocommerce' ) ); ?></h5>

        <?php do_action( "woocommerce_before_edit_address_form_shipping" ); ?>

        <?php foreach ( $shipping_fields as $key => $field ) : ?>

            <?php woocommerce_form_field( $key, $field, $userMeta[$key][0] ); ?>

        <?php endforeach; ?>

        <?php do_action( "woocommerce_after_edit_address_form__shipping" ); ?>




        <p>
            <input type="submit" class="button" name="save_address" value="<?php esc_attr_e( 'Save Address', 'woocommerce' ); ?>" />
            <?php wp_nonce_field( 'woocommerce-edit_address' ); ?>
            <input type="hidden" name="action" value="edit_address" />
        </p>

    </form>


<?php else : ?>

    <form method="post" class="edit-account">

        <h3><?php echo apply_filters( 'woocommerce_my_account_edit_address_title', $page_title, $load_address ); ?></h3>

        <div class="woocommerce-address-fields">
            <?php do_action( "woocommerce_before_edit_address_form_{$load_address}" ); ?>

            <div class="woocommerce-address-fields__field-wrapper">
                <?php
                foreach ( $address as $key => $field ) {
                    if ( isset( $field['country_field'], $address[ $field['country_field'] ] ) ) {
                        $field['country'] = wc_get_post_data_by_key( $field['country_field'], $address[ $field['country_field'] ]['value'] );
                    }
                    woocommerce_form_field( $key, $field, wc_get_post_data_by_key( $key, $field['value'] ) );
                }
                ?>
            </div>

            <?php do_action( "woocommerce_after_edit_address_form_{$load_address}" ); ?>

            <p>
                <input type="submit" class="button" name="save_address" value="<?php esc_attr_e( 'Save address', 'woocommerce' ); ?>" />
                <?php wp_nonce_field( 'woocommerce-edit_address' ); ?>
                <input type="hidden" name="action" value="edit_address" />
            </p>
        </div>

    </form>

<?php endif; ?>

<script type="text/javascript">
    jQuery(function() {
        setTimeout(function(){
            jQuery('#billing_city_field label').text('City');
            jQuery('#shipping_city_field label').text('City');
            var fin = jQuery('.woocommerce-MyAccount-content input');
            fin.each( function(){
                jQuery(this).removeAttr('placeholder');
                if (jQuery(this).val() != '') jQuery(this).closest('p').find('label').addClass('hasValue');
                jQuery(this).on('change focus keyup', function(){
                    jQuery(this).closest('p').find('label').addClass('hasValue');
                });
                jQuery(this).on('blur', function(){
                    if (jQuery(this).val() == '') jQuery(this).closest('p').find('label').removeClass('hasValue');
                });
            });
        }, 500);
    });

</script>
<?php do_action( 'woocommerce_after_edit_account_address_form' ); ?>
