<div id="header-notice" class="promobar">
	<div class="container">
		<span class="notice-txt">New site coming soon!</span>
		<span>Countdown: <span id="timer"></span></span>
		<a href="#" class="close" title="Close"><i class="bc-icon close-icon circle-x"></i></a>
	</div>
	<script type="text/javascript">
		jQuery('#header-notice .close').click(function(e){
			jQuery('#header-notice').slideUp();
			e.preventDefault();
		});
	</script>
</div>
<header id="tbay-header" class="site-header header-default header-v1 hidden-sm hidden-xs <?php echo (greenmart_tbay_get_config('keep_header') ? 'main-sticky-header' : ''); ?>" role="banner">
    <div class="header-main clearfix">
        <div class="container">
            <div class="header-inner">
                <div class="row">
					<!-- //LOGO -->
                    <div class="logo-in-theme col-md-2 text-left">
                        <?php get_template_part( 'page-templates/parts/logo' ); ?>
                    </div>
					
                    <!-- SEARCH -->
                    <div class="search col-md-5 hidden-sm hidden-xs">
                        <div class="pull-left call-number">
							<a href="tel:1300055888">
								<i class="bc-icon phone-icon"></i>
								<span class="nh">NEED<br>HELP</span>
								<span class="num">1300&nbsp;055&nbsp;888</span>
							</a>
						</div>
                    </div>
					
					<div class="pull-right col-md-5 text-right log-cart">
					
						<?php if ( greenmart_tbay_get_config('header_login') ) { ?>
							<?php get_template_part( 'page-templates/parts/topbar-account' ); ?>
						<?php } ?>
						
						<?php if ( !(defined('GREENMART_WOOCOMMERCE_CATALOG_MODE_ACTIVED') && GREENMART_WOOCOMMERCE_CATALOG_MODE_ACTIVED) && defined('GREENMART_WOOCOMMERCE_ACTIVED') && GREENMART_WOOCOMMERCE_ACTIVED ): ?>
							<div class="pull-right top-cart-wishlist">
								
								<!-- Cart -->
								<div class="pull-right top-cart hidden-xs">
									<?php get_template_part( 'woocommerce/cart/mini-cart-button' ); ?>
								</div>
							</div>
						<?php endif; ?>
						
					</div>
					
                </div>
            </div>
        </div>
    </div>
    <section id="tbay-mainmenu" class="tbay-mainmenu hidden-xs hidden-sm">
        <div class="container"> 
		
			<?php get_template_part( 'page-templates/parts/nav' ); ?>
			
			
        </div>      
    </section>
</header>
 <div class="bcfeat">
 	<div class="container">
 		<div>
	 		<p>
		 		<i class="bc-icon mf-icon-new green"></i>
		 		<span>Free Delivery Australia Wide</span>
	 		</p>
	 	</div>
	 	<div>
	 		<p>
		 		<i class="bc-icon wrt-icon-new green"></i>
		 		<span>5 Years Quality Warranty</span>
	 		</p>
	 	</div>
	 	<div>
	 		<p>
		 		<i class="bc-icon aus-icon-new green"></i>
		 		<span>Australia Owned &amp; Made</span>
	 		</p>
	 	</div>
	 	<!-- <div>
	 		<p>
		 		<i class="bc-icon manufacture-icon green"></i>
		 		<span>FREE DELIVERY<br>AUSTRALIAN WIDE</span>
	 		</p>
	 	</div>
	 	<div>
	 		<p>
		 		<i class="bc-icon tools-icon green"></i>
		 		<span>BUILD YOUR OWN<br>DIY CUSTOM BLINDS</span>
	 		</p>
	 	</div>
	 	<div>
	 		<p>
		 		<i class="bc-icon warranty-icon green"></i>
		 		<span>5 YEARS QUALITY<br>WARRANTY</span>
	 		</p>
	 	</div>
	 	<div>
	 		<p>
		 		<i class="bc-icon australia-icon green"></i>
		 		<span>PROUDLY OWNED &amp;<br>MADE IN AUSTRALIA</span>
	 		</p>
	 	</div>
	 	<div class="last-item">
	 		<p>
		 		<i class="bc-icon off80-icon green"></i>
		 		<span>MASSIVE SAVINGS<br>UP TO 80% OFF</span>
	 		</p>
	 	</div> -->
 	</div>
 </div>
 <?php if ( is_checkout() ): ?>
 <a class="tog-cart" href="#cart-sum" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="cart-sum">
 <div class="cart-toggle hidden-lg hidden-md">
 	<div class="container">
		<span class="order-sum-text pull-left"><i class="icofont icofont-shopping-cart"></i>Show Order Summary</span>
		<span class="cart-total-price pull-right"><i class="fa fa-chevron-down"></i> <?php wc_cart_totals_order_total_html(); ?></span>
	</div>
</div>
</a>	
<div style="clear:both;"></div>
<?php endif; ?>