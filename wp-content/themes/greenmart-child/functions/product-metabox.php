<?php
function bc_product_meta_box( $meta_boxes ) {
	$prefix = 'bcmeta_';

	$meta_boxes[] = array(
		'id' => 'bc-product-options',
		'title' => esc_html__( 'Blinds City Product Options', 'greenmart' ),
		'post_types' => array( 'product' ),
		'context' => 'advanced',
		'priority' => 'high',
		'autosave' => false,
		'fields' => array(
			array(
				'id' => $prefix . 'rating_star',
				'type' => 'image_advanced',
				'name' => esc_html__( 'Rating Star', 'greenmart' ),
				'max_file_uploads' => 1,

			),
			array(
				'id' => $prefix . 'manufacture_weeks',
				'type' => 'text',
				'name' => esc_html__( 'Manufacture', 'greenmart' ),
				'desc' => esc_html__( 'Enter Manufacture Duration', 'greenmart' ),
				'std' => '2-3 weeks',
			),
			array(
				'id' => $prefix . 'delivery',
				'type' => 'text',
				'name' => esc_html__( 'Leaves Warehouse in Delivery', 'greenmart' ),
				'desc' => esc_html__( 'Enter how many days/weeks the delivery will take effect (with days/weeks)', 'greenmart' ),
				'std' => '5 weeks',
			),
			array(
				'id' => $prefix . 'manufacture_warranty',
				'type' => 'text',
				'name' => esc_html__( 'Manufacturer Warranty', 'greenmart' ),
				'desc' => esc_html__( 'Enter warranty duration (with years/months)', 'greenmart' ),
				'std' => '5 years',
			),
			array(
				'id' => $prefix . 'is_sale',
				'type' => 'checkbox',
				'name' => esc_html__( 'Is product on sale?', 'greenmart' ),
			),
			array(
				'id' => $prefix . 'percent_discount',
				'type' => 'text',
				'name' => esc_html__( 'Percentage Discount', 'greenmart' ),
				'desc' => esc_html__( 'in % (dont include symblo). To show on shop/category pages', 'greenmart' ),
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'bc_product_meta_box' );