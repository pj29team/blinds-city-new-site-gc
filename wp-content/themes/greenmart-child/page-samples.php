<?php get_header();

$args = array(
	'post_type' => 'product',
	'posts_per_page' => -1,
	'product_cat' => 'custom-made'
);

$loop = new WP_Query($args);

$custom_products = array();

$p_attributes = array();
$p_attribute_labels = array();
$p_attribute_terms = array();
$p_variations = array();

while ($loop->have_posts()) {
    $loop->the_post();

    global $product;

    $custom_products[] = $product;

    if(!isset($_SESSION['p_attributes'])) {
        $attributes = $product->get_attributes();

        $attribute_terms = array();

        foreach ($attributes as $attribute) {
            $terms = get_the_terms($product->id, $attribute['name']);

            if (!isset($p_attribute_labels[$attribute['name']])) {
                $p_attribute_labels[$attribute['name']] = wc_attribute_label($attribute['name']);
            }

            if (is_array($terms)) {
                foreach ($terms as $term) {
                    $attribute_terms[$attribute['name']][] = $term;
                }
            }
        }

        $variations = $product->get_available_variations();

        $p_attributes[$product->id] = $attributes;
        $p_attribute_terms[$product->id] = $attribute_terms;
        $p_variations[$product->id] = $variations;

        //put the data in session
        $_SESSION['p_attributes'] = $p_attributes;
        $_SESSION['p_attribute_labels'] = $p_attribute_labels;
        $_SESSION['p_attribute_terms'] = $p_attribute_terms;
        $_SESSION['p_variations'] = $p_variations;
    } else {
        $p_attributes = $_SESSION['p_attributes'];
        $p_attribute_labels = $_SESSION['p_attribute_labels'];
        $p_attribute_terms = $_SESSION['p_attribute_terms'];
        $p_variations = $_SESSION['p_variations'];
    }


}



if(!isset($_SESSION['p_items_in_cart'])) {
    $p_items_in_cart = array();

    foreach ($woocommerce->cart->cart_contents as $item_key => $item) {
        $p_items_in_cart[$item['product_id']][] = $item['variation_id'];
    }
    $_SESSION['p_items_in_cart'] = $p_items_in_cart;
} else {
    $p_items_in_cart = $_SESSION['p_items_in_cart'];
}

echo '<script type="text/javascript">', PHP_EOL,
'var p_attributes = ', json_encode($p_attributes), ';', PHP_EOL,
'var p_attribute_labels = ', json_encode($p_attribute_labels), ';', PHP_EOL,
'var p_attribute_terms = ', json_encode($p_attribute_terms), ';', PHP_EOL,
'var p_variations = ', json_encode($p_variations), ';', PHP_EOL,
'var p_items_in_cart = ', json_encode($p_items_in_cart), ';', PHP_EOL,
'</script>';

?>
<ul class="double-cloumn clearfix">
	<div class="full-width" style="max-width: 1200px; margin: auto;">	
		<ul class="page-container" style="padding: 0;">
			<div class="samples-page" id="samples-page">
				<div class="samples-page__title">
					<h1><span class="light-font">Order your</span><br><span class="bold-font">Free Fabric Samples</span></h1>
				</div>
				<div class="samples-page__filters">
					<div class="filter">
						<div class="filter__label">
							Filter by Blind Type
						</div>
						<div class="filter__box">
							<?php if ($custom_products): ?>
							<select>
								<option value="">Choose Product</option>
								<?php
								
								foreach ($custom_products as $c_product) {
									$c_title = $c_product->post->post_title;
									
									if (strpos($c_title, 'Custom ') === 0) {
										$c_title = substr($c_title, 7);
									}
									
									echo '<option value="', $c_product->id, '">',
									esc_html($c_title),
									'</option>';
								}
								
								?>
							</select>
							<?php else: ?>
								Products not found.
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div class="samples-page__samples" style="display:none"></div>
				<div class="ajax-loader"></div>
			</div>
		</ul>
	</div>
</ul>
<?php get_footer(); ?>